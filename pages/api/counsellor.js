import axios from "axios";

export const getCounsellorDetails = (counsellorID) => {
    return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL+"counsellor/details/"+counsellorID);
}

export const getCounsellorCourses = (counsellorID) => {
    return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL+"counsellor/courses/"+counsellorID);
}

export const getCounsellorModesForACourse = (counsellorID, courseShortName) => {
    return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL+"counsellor/modes/"+counsellorID+"/"+courseShortName);
}

export const getCounsellorSpecializationsForModeCourse = (courseShortName, modeID) => {
    return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL+"counsellor/specializations/"+courseShortName+"/"+modeID);
}