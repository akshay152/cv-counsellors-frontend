import axiosJWT from '../../utils/jwtAxios';

export const submitReport = (customer_id, counsellor_id, reason, description) => {
    var formData = new FormData();
    formData.append("customer_id", customer_id);
    formData.append("counsellor_id", counsellor_id);
    formData.append("reason", reason);
    formData.append("description", description);

    return axiosJWT.post(process.env.NEXT_PUBLIC_BACKEND_URL+"misc/report/new", formData);
}