import axios from "axios";
import axiosJWT from "../../utils/jwtAxios";

export const getCustomerDetails = (customerID) => {
    return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL+"customer/details/"+customerID);
}

export const sendBusyCallDeclined = (message) => {
    return axiosJWT.post(process.env.NEXT_PUBLIC_BACKEND_URL+"consultation/busy/system/insert", message);
}