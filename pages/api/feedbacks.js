import axiosJWT from "../../utils/jwtAxios";

export const getCustomerFeedbacks = (user_id) => {
    return axiosJWT.get(process.env.NEXT_PUBLIC_BACKEND_URL+"customer/feedbacks/"+user_id);
}