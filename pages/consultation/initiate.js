import { Downgraded, useState } from '@hookstate/core';
import { useRouter } from 'next/router';
import React, { useEffect } from 'react'
import { Container } from 'react-bootstrap';
import InitiateConsultationWrapper from '../../components/user/consultations/initiate/InitiateConsultationWrapper';
import isAuthenticated from '../../utils/auth';
import { getUser } from '../../utils/common';
import store from '../../utils/store';

const InitiateConsultation = () => {

    const globalStore = useState(store);

    const router = useRouter();
    const mode_id = useState(null);
    const course_id = useState(null);
    const specialization_id = useState(null);

    const authentication = useState(null);

    useEffect(() => {
        authentication.set(isAuthenticated());

        if(!authentication.get()) {
            router.push("/");
        }
    }, [isAuthenticated]);

    useEffect(() => {
        globalStore.pathname.set("/consultation/initiate");
    }, []);

    useEffect(() => {
        mode_id.set(router.query.mode);
        course_id.set(router.query.course);
        specialization_id.set(router.query.specialization);
    }, [router]);

    useEffect(() => {
        if(mode_id.get() || course_id.get() || specialization_id.get()) {
            if(mode_id.get() === undefined || course_id.get() === undefined || specialization_id.get() === undefined) {
                router.replace("/");
            }
        }
    }, [mode_id.get(), course_id.get(), specialization_id.get()]);

    useEffect(() => {
        if(globalStore.socketRef.attach(Downgraded).get() && router.query.course && router.query.mode) {
            globalStore.socketRef.attach(Downgraded).get().current.emit("get-online-counsellors", getUser().id, router.query.course, router.query.mode);
        }
    }, [globalStore.socketRef.attach(Downgraded).get(), router]);

    return (
        <div>
            <Container>
                {authentication.get() && mode_id.get() && course_id.get() && specialization_id.get() ?
                <InitiateConsultationWrapper specializationID={specialization_id.get()} />
                :
                null
                }
            </Container>
        </div>
    )
}

export default InitiateConsultation;