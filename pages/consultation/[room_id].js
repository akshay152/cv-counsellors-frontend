import { Downgraded, useState } from "@hookstate/core";
import isAuthenticated from "../../utils/auth";
import { useRouter } from "next/router";
import { useEffect } from "react";
import VideoRoom from "../../components/chat/VideoRoom";
import AudioRoom from "../../components/chat/AudioRoom";
import store from "../../utils/store";
import AuthModal from "../../components/global/header/modals/AuthModal";
import CustomModal from "../../components/global/customs/CustomModal";

const AVChatRoom = () => {

    const globalStore = useState(store);

    const router = useRouter();
    const authentication = useState(null);

    const roomID = useState(null);
    const counsellorID = useState(null);
    const roomType = router.query.type;

    const modalContent = useState(null);
    const showModal = useState(false);

    function setupModal(key) {
        if(key === "auth") {
            modalContent.set(<AuthModal closeModal={() => showModal.set(false)} redirectURL={window.location} />);
            showModal.set(true);
        }
    }

    useEffect(() => {
        authentication.set(isAuthenticated());
        if(!authentication.get()) {
            setupModal("auth");
        }
    }, [isAuthenticated]);

    useEffect(() => {
        globalStore.pathname.set("/consultation/room-id");
    }, []);
    
    useEffect(() => {
        roomID.set(router.query.room_id);
        counsellorID.set(router.query.counsellor_id)
    }, [router]);

    return (
        <div style={{backgroundColor: "#202124"}} >
            <CustomModal content={modalContent} showModal={showModal} modalSize="md" staticBackdrop={false} />
            {authentication.get() && roomID.get() && counsellorID.get() ? roomType === "video" ? <VideoRoom roomID={roomID.get()} counsellorID={counsellorID.get()} /> : <AudioRoom roomID={roomID.get()} /> : null}
        </div>
    )
}

export default AVChatRoom;