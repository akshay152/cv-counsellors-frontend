import { useRouter } from 'next/router';
import { useEffect } from 'react';
import axios from 'axios';
import { useState } from '@hookstate/core';
import { setUserAuth } from '../../../utils/common';
import { Spinner } from 'react-bootstrap';

const Login = () => {

    const router = useRouter();

    const cv_id = useState(null);
    const dob = useState(null);
    const course = useState(null);
    const specialization = useState(null);

    useEffect(() => {
        if (router.query) {
            cv_id.set(router.query.cv_id);
            dob.set(router.query.dob);
            course.set(router.query.course);
            specialization.set(router.query.specialization);
        }
    }, [router.query]);

    function checkIfUserHasActivConsultation(prevRes) {
        axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "collegevidya/redirect/active-consultations?cv_id=" + cv_id.get() + "&specialization_id=" + prevRes.data.specialization_id)
            .then((response) => {
                if (response.data.length > 0) {
                    const name = response.data[0].name;
                    iziToast.question({
                        timeout: false,
                        close: false,
                        overlay: true,
                        displayMode: 'once',
                        closeOnEscape: false,
                        drag: false,
                        id: 'question',
                        zindex: 999,
                        title: 'Hey ' + name,
                        message: 'You already have active consultation(s) for this course. Would you still like to start a new one?',
                        position: 'center',
                        buttons: [
                            ['<button><b>Yes</b></button>', function (instance, toast) {
                                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                                window.location.href = `${process.env.NEXT_PUBLIC_BASE_URL}consultation/initiate?mode=1&course=${prevRes.data.course_id}&specialization=${prevRes.data.specialization_id}`;
                            }, true],
                            ['<button>No</button>', function (instance, toast) {
                                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                                window.location.href = `/user/consultations?consultation_id=`+response.data[0].active_consultations;
                            }],
                        ],
                    });
                }
                else {
                    window.location.href = `${process.env.NEXT_PUBLIC_BASE_URL}consultation/initiate?mode=1&course=${prevRes.data.course_id}&specialization=${prevRes.data.specialization_id}`;
                }
            })
    }

    useEffect(() => {
        if (cv_id.get() && dob.get() && course.get()) {
            axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "collegevidya/redirect/login?cv_id=" + cv_id.get())
                .then((response) => {
                    let formData = new FormData();
                    formData.append('mobile', response.data.mobile);
                    formData.append('password', dob.get());

                    axios.post(process.env.NEXT_PUBLIC_BACKEND_URL + "customer/login", formData)
                        .then((response) => {
                            setUserAuth(response.data.user, response.data.accessToken, response.data.refreshAccessToken);

                            axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "collegevidya/course-spec?course=" + course.get() + "&specialization=" + specialization.get())
                                .then((response) => {
                                    checkIfUserHasActivConsultation(response);
                                })
                                .catch((error) => {
                                    console.log(error);
                                    //window.location.href = "https://collegevidya.com";
                                })
                        })
                        .catch((error) => {
                            console.log(error);
                            //window.location.href = "https://collegevidya.com";
                        })
                })
                .catch((error) => {
                    console.log(error);
                    //window.location.href = "https://collegevidya.com";
                })
        }
        else {
            console.log(cv_id.get());
        }

    }, [cv_id.get()])

    return (
        <div className='d-flex align-items-center justify-content-center' style={{ height: "88vh", color: "#14bef0" }}>
            <Spinner size='lg' animation='border' />
        </div>
    )
}

export default Login;