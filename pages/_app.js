import '../styles/globals.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'izitoast/dist/css/iziToast.min.css';
import "primereact/resources/themes/lara-light-indigo/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css";
import "swiper/css/effect-cards";
import Header from '../components/global/header/Header';
import Footer from '../components/global/footer/Footer';
import { Modal, SSRProvider } from 'react-bootstrap';
import NextNProgress from "nextjs-progressbar";
import { useEffect, useRef } from 'react';
import io from 'socket.io-client';
import { getUser } from '../utils/common';
import store from '../utils/store';
import { getUserChats, readConsultation, scrollToBottom } from '../utils/chat';
import { Downgraded, useState } from '@hookstate/core';
import isAuthenticated from '../utils/auth';
import CallModalContent from '../components/chat/modals/CallModalContent';
import BusyCallModalContent from '../components/chat/modals/BusyCallModalContent';
import { useRouter } from 'next/router';

function MyApp({ Component, pageProps }) {

  const router = useRouter();

  const globalStore = useState(store);
  const socketRef = useRef();

  const modalContent = useState(null);
  const showModal = useState(false);
  const showBusyModal = useState(false);

  let callAudio = null;

  function stopCallAudio() {
    callAudio.pause();
    callAudio.currentTime = 0;
  }

  useEffect(() => {
    callAudio = new Audio("/audios/new-call-notification.mp3");
    callAudio.addEventListener('ended', function () {
      this.currentTime = 0;
      this.play();
    }, false);
    if (isAuthenticated()) {
      socketRef.current = io.connect(process.env.NEXT_PUBLIC_CHAT_SERVER_URL);
      socketRef.current.emit("new-chat-socket", getUser().id, "CUSTOMER");

      socketRef.current.on('connect', () => {
        globalStore.isActive.set(true);
        socketRef.current.emit("online-offline-status", getUser().id, "CUSTOMER", "ONLINE");
      });

      socketRef.current.on('disconnect', () => {
        globalStore.isActive.set(false);
        socketRef.current.emit("online-offline-status", getUser().id, "CUSTOMER", "OFFLINE");
      });

      globalStore.socketRef.set(socketRef);

      socketRef.current.on("incoming-message", (senderID, message, consultation_id) => {
        if (globalStore.pathname.get().includes("consultations")) {
          if (globalStore.activeConsultation.attach(Downgraded).get().counsellor_id === senderID) {
            globalStore.currentChatMessages.set([...globalStore.currentChatMessages.attach(Downgraded).get(), message]);
            readConsultation({ "customer_id": getUser().id, "consultation_id": globalStore.activeConsultation.attach(Downgraded).get().id }).then(() => {
              getUserChats().then((response) => {
                globalStore.currentChatList.set(response.data.description);
              });
            });
            scrollToBottom();
          }

          getUserChats().then((response) => {
            globalStore.currentChatList.set(response.data.description);
          });
        }
        else {

          var audio = new Audio("/audios/new-message-notification.mp3");
          audio.play();

          iziToast.info({
            title: "New message",
            message: "You have received a new message from a counsellor",
            position: "topRight",
            timeout: false,
            buttons: [
              ['<button><b>Take Me</b></button>', function (instance, toast) {
                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                router.push("/user/consultations?consultation_id=" + consultation_id);
              }, true]
            ]
          });
        }
      });

      socketRef.current.on("set-online-counsellors", (activeCounsellors) => {
        globalStore.activeCounsellors.set(activeCounsellors);
      });

      socketRef.current.on("set-availability", (availability) => {
        globalStore.currentCounsellorAvailability.set(availability);
        if (globalStore.pathname.get().includes("/consultation")) {
          globalStore.directLinkVideoCallAvailability.set(availability);
        }
      });

      socketRef.current.on("call-not-placed", () => {
        console.log("not placed");
      });

      socketRef.current.on("incoming-call", (requesterName, requesterImage, callType, callID, counsellorID) => {
        if (globalStore.isIdle.get()) {
          callAudio.play();
          setupModal("incoming-call", { name: requesterName, image: requesterImage, type: callType, room_id: callID, counsellor_id: counsellorID });
        }
        else {
          if (globalStore.activeConsultation.attach(Downgraded).get().counsellor_id !== counsellorID) {
            setupModal("busy-incoming-call", { name: requesterName, image: requesterImage, type: callType, room_id: callID, counsellor_id: counsellorID });
          }
        }
      });

      socketRef.current.on("destroy-call-notification", (callerName) => {
        stopCallAudio();
        iziToast.warning({
          title: "Missed call",
          message: "You have a missed call from " + callerName,
          position: "topRight",
          timeout: false
        })
        showModal.set(false);
        showBusyModal.set(false);
      });

      socketRef.current.on("destroy-call-notification-auto", (callerName) => {
        stopCallAudio();
        showModal.set(false);
        showBusyModal.set(false);
      });
    }

  }, [isAuthenticated]);

  function CallModal(props) {
    return (
      <Modal {...props}
        size="md"
        backdrop="static"
        centered>
        {modalContent.attach(Downgraded).get()}
      </Modal>
    )
  }

  function CustomCallModal(props) {
    return (
      <Modal {...props}
        size="md"
        backdrop="static"
        dialogClassName='busy-call-modal'
        centered={false}>
        {modalContent.attach(Downgraded).get()}
      </Modal>
    )
  }

  function setupModal(key, data) {
    if (key === "incoming-call") {
      modalContent.set(<CallModalContent stopCallAudio={stopCallAudio} closeModal={() => showModal.set(false)} data={data} />);
      showModal.set(true);
    }
    else if (key === "busy-incoming-call") {
      modalContent.set(<BusyCallModalContent closeModal={() => showBusyModal.set(false)} data={data} />);
      showBusyModal.set(true);
    }
  }

  return (
    <SSRProvider>
      <NextNProgress color='#14bef1' />
      <Header />
      <div style={{ marginTop: "100px" }}>
        <Component {...pageProps} />
      </div>
      <CustomCallModal show={showBusyModal.get()} onHide={() => showBusyModal.set(false)} />
      <CallModal show={showModal.get()} onHide={() => showModal.set(false)} />
      <Footer />
    </SSRProvider>
  )
}

export default MyApp;
