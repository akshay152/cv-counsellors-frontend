import { useEffect } from "react";
import { Container, Row, Col } from 'react-bootstrap';
import { Downgraded, useState } from "@hookstate/core";
import { useRouter } from 'next/router';
import ChatList from '../../components/user/consultations/ChatList';
import ConsultationWrapper from '../../components/user/consultations/ConsultationWrapper';
import isAuthenticated from "../../utils/auth";
import ChatWrapper from "../../components/user/consultations/ChatWrapper";
import ChatHeader from "../../components/user/consultations/ChatHeader";
import ChatBody from "../../components/user/consultations/ChatBody";
import ChatFooter from "../../components/user/consultations/ChatFooter";
import store from "../../utils/store";
import Image from "next/image";
import { getUser } from "../../utils/common";

const Consultations = () => {

    const globalStore = useState(store);

    const router = useRouter();
    const authentication = useState(null);

    useEffect(() => {
        authentication.set(isAuthenticated());

        if (!authentication.get()) {
            router.push("/");
        }
    }, [isAuthenticated]);

    useEffect(() => {
        globalStore.pathname.set("/user/consultations");
    }, []);

    useEffect(() => {
        if (globalStore.socketRef.attach(Downgraded).get()) {
            globalStore.socketRef.attach(Downgraded).get().current.emit("get-availablity", getUser().id, "CUSTOMER", globalStore.activeConsultation.attach(Downgraded).get().counsellor_id, "COUNSELLOR");
        }
    }, [globalStore.socketRef.attach(Downgraded).get(), globalStore.activeConsultation.attach(Downgraded).get()]);

    useEffect(() => {
        if (router && globalStore.currentChatList.attach(Downgraded).get().length > 0) {
            var consultationID = router.query.consultation_id;
            if (consultationID) {
                var currentConsultation = globalStore.currentChatList.attach(Downgraded).get().filter(chat => chat.id === parseInt(consultationID));
                if (currentConsultation.length > 0) {
                    globalStore.activeConsultation.set(currentConsultation[0]);
                }
                router.query.consultation_id = null;
            }
            window.history.replaceState(null, '', '/user/consultations')
        }
    }, [router, globalStore.currentChatList.attach(Downgraded).get()]);

    return (
        <Container className="consultation_main">
            {authentication.get() ?
                <ConsultationWrapper>
                    <Row>
                        <Col sm="4">
                            <ChatList />
                        </Col>
                        <Col sm="8">
                            {globalStore.activeConsultation.attach(Downgraded).get().id !== -1 ?
                                <ChatWrapper>
                                    <ChatHeader counsellorID={globalStore.activeConsultation.attach(Downgraded).get().counsellor_id} profLink={`/counsellor/${globalStore.activeConsultation.attach(Downgraded).get().slug}`} userImage={globalStore.activeConsultation.attach(Downgraded).get().image} userName={globalStore.activeConsultation.attach(Downgraded).get().name} videoRoomID={globalStore.activeConsultation.attach(Downgraded).get().video_room_id} audioRoomID={globalStore.activeConsultation.attach(Downgraded).get().audio_room_id} startTime={globalStore.activeConsultation.attach(Downgraded).get().start_time} />
                                    <ChatBody />
                                    <ChatFooter counsellorID={globalStore.activeConsultation.attach(Downgraded).get().counsellor_id} />
                                </ChatWrapper>
                                :
                                <ChatWrapper>
                                    <div className="d-flex align-items-center" style={{ height: "inherit", justifyContent: "center", flexDirection: "column" }}>
                                        <Image src="/images/chat.png" width={150} height={150} />
                                        <h4 className="mt-3 mb-2 fw-bold">Choose a consultation</h4>
                                        <h6>Select a chat to see your previous messages</h6>
                                    </div>
                                </ChatWrapper>
                            }
                        </Col>
                    </Row>
                </ConsultationWrapper>
                :
                null
            }
        </Container>
    )
}

export default Consultations;