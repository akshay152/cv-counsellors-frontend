import React, { useEffect } from 'react'
import { Container } from 'react-bootstrap';
import Image from 'next/image';
import Link from 'next/link';
import { useState } from '@hookstate/core';
import axios from 'axios';
import { useRouter } from 'next/router'

const VerifyEmail = () => {

    const router = useRouter();
    const valid = useState(true);

    useEffect(() => {
        if(router.isReady) {
            const email_address = router.query.email;
            const verification_token = router.query.verification_token;

            axios.get(process.env.NEXT_PUBLIC_BACKEND_URL+"customer/verify-email?email="+email_address+"&verification_token="+verification_token)
            .then(() => {
                valid.set(true);
                router.push("/user/profile");
            })
            .catch(() => {
                valid.set(false);
            })
        }
    }, [router.isReady]);

    return (
        <Container>
            {valid && !valid.get() ? 
            <div style={{height: "540px", display: "table"}}>
                <div style={{display: "table-cell", width: "45%", height: "100%", verticalAlign: "middle", textAlign: "center"}}>
                    <Image src="/images/no-results-found.png" height={204} width={312} />
                </div>
                <div style={{display: "table-cell", width: "55%", height: "100%", paddingTop: "80px", paddingRight: "80px", paddingLeft: "30px"}}>
                    <span style={{fontSize: "45px", fontWeight: "bold", lineHeight: "1.11", letterSpacing: "normal", color: "#414146"}}>
                        Link has expired
                    </span>
                    <br/>
                    <br/>
                    <br/>
                    Looks like the Verification Token is invalid or has already been used. If you need a fresh link, please 
                    <Link href="/user/profile">
                        <a style={{textDecoration: "none", color: "#14bef0"}}> click here</a>
                    </Link>
                    <br/>
                    <br/>
                    <br/>
                    <Link href="/user/profile">
                        <a style={{fontSize: "18px", fontWeight: "bold", lineHeight: "1.33", letterSpacing: "normal", color: "#28328c", textDecoration: "none"}}>Go to Homepage</a>
                    </Link>
                </div>
            </div>
        :
        <></>
        }
        </Container>
    )
}

export default VerifyEmail;