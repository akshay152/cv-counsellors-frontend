import { useState } from "@hookstate/core";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { Container } from "react-bootstrap";
import UserFeedbacks from "../../components/user/feedbacks/UserFeedbacks";
import isAuthenticated from "../../utils/auth";
import store from "../../utils/store";

const Feedbacks = () => {

    const router = useRouter();
    const authentication = useState(null);

    const globalStore = useState(store);

    useEffect(() => {
        authentication.set(isAuthenticated());
        if(!authentication.get()) {
            router.push("/");
        }
    }, [isAuthenticated]);

    useEffect(() => {
        globalStore.pathname.set("/user/feedbacks");
    }, []);
    
    return (
        <Container>
            {authentication.get() ? <UserFeedbacks /> : null}
        </Container>
    )
}

export default Feedbacks;