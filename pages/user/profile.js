import { useEffect } from "react";
import { Container } from "react-bootstrap";
import { useRouter } from 'next/router';
import { useState } from "@hookstate/core";
import UserProfile from "../../components/user/profile/UserProfile";
import isAuthenticated from "../../utils/auth";
import store from "../../utils/store";

const Profile = () => {

  const router = useRouter();
  const authentication = useState(null);

  const globalStore = useState(store);

  useEffect(() => {
    authentication.set(isAuthenticated());

    if(!authentication.get()) {
      router.push("/");
    }
  }, [isAuthenticated]);

  useEffect(() => {
    globalStore.pathname.set("/user/profile");
  }, []);

  return (
    <Container>
        {authentication.get() ? <UserProfile /> : null}
    </Container>
  )
}

export default Profile;