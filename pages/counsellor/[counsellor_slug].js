import { useRouter } from 'next/router';
import { useState } from '@hookstate/core';
import { Container } from 'react-bootstrap';
import CounsellorDetails from '../../components/counsellors/CounsellorDetails';
import { useEffect } from 'react';
import store from '../../utils/store';

const CounsellorProfile = () => {

    const globalStore = useState(store);

    const router = useRouter();
    const counsellor_slug = useState();

    useEffect(() => {
        counsellor_slug.set(router.query.counsellor_slug);
    }, [router]);

    useEffect(() => {
        globalStore.pathname.set("/counsellor/profile");
    }, []);

    return (
        <div>
            <Container>
                {counsellor_slug ?
                    <CounsellorDetails slug={counsellor_slug} />
                    :
                    null
                }
            </Container>
        </div>
    );
}

export default CounsellorProfile;