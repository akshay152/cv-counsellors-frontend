import { useState } from '@hookstate/core';
import { useEffect } from 'react';
import { Container } from 'react-bootstrap';
import CounsellorPage from "../components/counsellors/CounsellorPage";
import store from '../utils/store';

const Counsellors = () => {

    const globalStore = useState(store);

    useEffect(() => {
        globalStore.pathname.set("/counsellors");
    }, []);

    return (
        <Container>
            <CounsellorPage />
        </Container>
    );
}

export default Counsellors;