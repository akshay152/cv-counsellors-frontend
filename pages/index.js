import SearchBox from "../components/home/SearchBox/SearchBox";
import { Button, Container, Row } from 'react-bootstrap';
import { Downgraded, useState } from "@hookstate/core";
import { useEffect } from "react";
import getCountry from "../utils/getCountry";
import axios from "axios";
import Image from 'next/image';
import banner from '../public/images/main-banner.jpg'
import IndexCard from "../components/home/IndexCard/IndexCard";
import CourseSelect from "../components/global/CourseSelect/CourseSelect";
import TestimonialSlider from "../components/global/Testimonial/TestimonialSlider";
import TopCounsellors from "../components/home/TopCounsellors/TopCounsellors";
import store from "../utils/store";
import isAuthenticated from "../utils/auth";
import CustomModal from "../components/global/customs/CustomModal";
import AuthModal from "../components/global/header/modals/AuthModal";
import { useRouter } from "next/router";

export default function Home() {

  const globalStore = useState(store);

  const router = useRouter();
  const authentication = useState(null);

  const states = useState([]);
  const courses = useState([]);
  const isStateLoading = useState(true);
  const isCourseLoading = useState(true);

  const selectedState = useState("");
  const selectedCourse = useState("");

  const onStateChange = (value) => {
    selectedState.set(value);
  }

  const onCourseChange = (value) => {
    selectedCourse.set(value);
  }

  async function getStates() {
    var _country = await getCountry();

    axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "state?countryCode=" + _country.country_code)
      .then((response) => {
        states.set(response.data);
        isStateLoading.set(false);

        selectedState.set(response.data[0]);
      });
  }

  function getCourses() {
    axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "course")
      .then((response) => {
        courses.set(response.data);
        isCourseLoading.set(false);

        selectedCourse.set(response.data[0]);
      });
  }

  useEffect(() => {
    // getStates();
    // getCourses();
  }, []);

  useEffect(() => {
    globalStore.pathname.set("/");
  }, []);

  const modalContent = useState(null);
  const showModal = useState(false);

  function setupModal() {
    modalContent.set(<AuthModal redirectURL="/user/consultations" closeModal={() => showModal.set(false)} />);
    showModal.set(true);
  }

  useEffect(() => {
    authentication.set(isAuthenticated());

    if (!authentication.get()) {
      setupModal();
    }
    else {
      router.replace("/user/consultations")
    }
  }, [isAuthenticated]);

  const indexCards = [
    {
      id: 0,
      image: "/images/instant-video-consultation.png",
      title: "Instant Video Consultation",
      subtitle: "Connect within 60 secs",
      href: "/consult",
      background: "#AFCFED"
    },
    {
      id: 1,
      image: "/images/instant-video-consultation.png",
      title: "Certified Counsellors",
      subtitle: "Verified counsellors only",
      href: "/counsellors",
      background: "#98CBD6"
    },
    {
      id: 2,
      image: "/images/instant-video-consultation.png",
      title: "30+ Courses",
      subtitle: "Counselling for variety of courses",
      href: "/courses",
      background: "#CCD0DB"
    },
    {
      id: 3,
      image: "/images/instant-video-consultation.png",
      title: "Education Modes",
      subtitle: "Distance, Online & Abroad",
      href: "/courses",
      background: "#D5D8FC"
    },
    {
      id: 4,
      image: "/images/instant-video-consultation.png",
      title: "Trustworthy",
      subtitle: "We provide 100% unbiased counselling",
      href: "/consult",
      background: "#AFCFED"
    },
  ]

  return (
    <Container>
      {/* <div className="d-flex" style={{ padding: "20px 0px", justifyContent: "space-between" }}>
        <SearchBox default={selectedState.attach(Downgraded).get()} options={states.attach(Downgraded).get()} onChange={onStateChange} isLoading={isStateLoading.get()} placeholder="Search Location" />
        <SearchBox default={selectedCourse.attach(Downgraded).get()} options={courses.attach(Downgraded).get()} onChange={onCourseChange} isLoading={isCourseLoading.get()} placeholder="Search Course" />
        <div style={{ flexBasis: "45%" }} />
      </div>
      <div style={{ borderRadius: "15px", position: "relative", overflow: "hidden" }}>
        <Image src={banner} layout="responsive" />
        <h3 style={{ position: "absolute", top: "36px", left: "48px", color: "white", fontWeight: "700" }}>Skip the travel!</h3>
        <h2 style={{ position: "absolute", top: "70px", left: "47px", color: "white", fontWeight: "700" }}>Take Online Video Consultation</h2>
        <p style={{ position: "absolute", top: "120px", left: "48px", color: "white" }}>Private consultation + Audio call . Starts at just ₹199</p>
        <Button variant="light" className="shadow-none" style={{ position: "absolute", bottom: "36px", left: "48px" }}>Consult now</Button>
      </div>
      <Row className="mb-5" style={{ justifyContent: "space-between", marginTop: "4rem" }}>
        {indexCards.map(card =>
          <IndexCard key={card.id} card={card} />
        )}
      </Row>
      <div style={{ marginTop: "5rem" }}>
        <h4 className="fw-bold mb-0">We provide consultations for over 30 courses</h4>
        <p className="mt-1" style={{ fontSize: "14px" }}>Select education mode and specialization to quick start the consultation!</p>
        <CourseSelect />
      </div>
      <div style={{ marginTop: "5rem", marginBottom: "5rem" }}>
        <h4 className="fw-bold mb-0">Consult top counsellors for any education concern</h4>
        <p className="mt-1" style={{ fontSize: "14px" }}>2500+ mentors to give a realistic viewpoint about your career goals and enable you to build a successful career.</p>
        <TopCounsellors />
      </div>
      <div style={{ marginTop: "5rem", marginBottom: "5rem" }}>
        <h4 className="fw-bold text-center">What our users have to say</h4>
        <TestimonialSlider />
      </div> */}
      <CustomModal content={modalContent} showModal={showModal} modalSize="md" staticBackdrop={true} />
    </Container>
  );
}
