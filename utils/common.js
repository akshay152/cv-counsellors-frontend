import axios from "axios";
import cookie from "js-cookie";

export const getUser = () => {
    const userStr = cookie.get('user');

    if(userStr)
        return JSON.parse(userStr);
    else
        return null;
}

export const getAccessToken = () => {
    const accessTokenStr = cookie.get('accessToken');

    if(accessTokenStr)
        return accessTokenStr;
    else
        return null;
}

export const getRefreshAccessToken = () => {
    const refreshAccessTokenStr = cookie.get('refreshAccessToken');

    if(refreshAccessTokenStr)
        return refreshAccessTokenStr;
    else
        return null;
}

export const refreshAccessTokens = async () => {
    try {
        let formData = new FormData();
        formData.append("refreshAccessToken", getRefreshAccessToken())

        const res = await axios.post(process.env.NEXT_PUBLIC_BACKEND_URL+"customer/token/refresh", formData);
        cookie.set('accessToken', res.data.accessToken, { expires: 7 });
        cookie.set('refreshAccessToken', res.data.refreshAccessToken, { expires: 7 });

        return res.data;
    }
    catch(e) {
        cookie.remove('user');
        cookie.remove('accessToken');
        cookie.remove('refreshAccessToken');

        iziToast.error({
            title: "Session expired",
            message: "Your session has expired. Please login again.",
            timeout: 2000,
            onClosing: () => {
                window.location.href = '/';
            }
        });
    }
}

export const setUserAuth = (user, accessToken, refreshAccessToken) => {
    cookie.set('user', JSON.stringify(user), { expires: 7 });
    cookie.set('accessToken', accessToken, { expires: 7 });
    cookie.set('refreshAccessToken', refreshAccessToken, { expires: 7 });
}

export const removeUserAuth = (e) => {

    e.preventDefault();

    let formData = new FormData();
    formData.append("refreshAccessToken", getRefreshAccessToken());

    axios.post(process.env.NEXT_PUBLIC_BACKEND_URL+"customer/logout", formData)
    .then(() => {
        cookie.remove('user');
        cookie.remove('accessToken');
        cookie.remove('refreshAccessToken');

        iziToast.success({
            title: "Logout successful",
            message: "You have been successfully logged out",
            timeout: 2000,
            position: "topRight",
            onClosing: () => {
                window.location.href = "/";
            }
        });
    });
}