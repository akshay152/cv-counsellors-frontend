import { createState } from "@hookstate/core";

const store = createState({
    activeConsultation: {
        id: -1,
        unread: false,
        image: "",
        name: "",
        lastMessage: "",
        start_time: "",
        video_room_id: "",
        audio_room_id: "",
        lastMessageTime: "",
        counsellor_id: -1,
        finished: false
    },
    lowerLimit: 1,
    upperLimit: 100,
    currentChatList: [],
    currentChatMessages: [],
    currentChatAttachments: [],
    video_grid_height: "fit-content",
    socketRef: null,
    pathname: "/",
    activeCounsellors: [],
    currentCounsellorAvailability: false,
    directLinkVideoCallAvailability: null,
    isScreenSharing: false,
    isIdle: true,
    isActive: false,
    typing: false
});

export default store;