import axios from 'axios';

export default async function getCountry() {
    return await axios.get('https://ipapi.co/json/').then(res => res.data);
}