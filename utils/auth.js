const { getUser, getAccessToken, getRefreshAccessToken } = require("./common");

export default function isAuthenticated() {
    if(getUser() && getAccessToken() && getRefreshAccessToken()) {
        return true;
    }
    return false;
}