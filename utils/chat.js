import { getUser } from "./common";
import axiosJWT from "./jwtAxios";

export const scrollToBottom = () => {
    document.getElementById("dummy-div").scrollIntoView();
}

export const getUserChats = () => {
    return axiosJWT.get(process.env.NEXT_PUBLIC_BACKEND_URL+"consultation/"+getUser().id);
}

export const getChatMessages = (activeConsultationID, loadMessagesLimit) => {
    return axiosJWT.get(process.env.NEXT_PUBLIC_BACKEND_URL+"consultation/"+getUser().id+"/"+activeConsultationID+"?lower_limit=0&upper_limit="+loadMessagesLimit);
}

export const sendMessage = (message) => {
    return axiosJWT.post(process.env.NEXT_PUBLIC_BACKEND_URL+"consultation/message", message);
}

export const readConsultation = (consultation) => {
    return axiosJWT.put(process.env.NEXT_PUBLIC_BACKEND_URL+"consultation/read", consultation);
}

export const loadMoreChatMessages = (activeConsultationID, lowerLimit, upperLimit) => {
    return axiosJWT.get(process.env.NEXT_PUBLIC_BACKEND_URL+"consultation/"+getUser().id+"/"+activeConsultationID+"?lower_limit="+lowerLimit+"&upper_limit="+upperLimit);
}

export const sendMessagePingToSocket = (socketRef, receiverID, message, consultationID) => {
    socketRef.current.emit("new-message", getUser().id, receiverID, "COUNSELLOR", message, consultationID);
}

export const sendStatusPingToSocket = (socketRef, receiverID, consultationID, status) => {
    socketRef.current.emit("user-status", receiverID, "COUNSELLOR", consultationID, status);
}