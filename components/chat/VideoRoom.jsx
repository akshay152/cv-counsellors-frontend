import { useEffect, useLayoutEffect, useRef } from "react";
import Image from 'next/image';
import styles from './VideoRoom.module.css';
import { Downgraded, useState } from "@hookstate/core";
import UserVideo from "./VideoComponents/UserVideo";
import ScreenShare from "./VideoComponents/ScreenShare";
import ActionButtons from "./ActionButtons";
import store from '../../utils/store';
import { getUser } from "../../utils/common";
import { verifyConsultation } from "../../pages/api/consultation";
import { Modal } from "react-bootstrap";
import VideoModalContent from "./VideoComponents/modals/VideoModalContent";
import Peer from "simple-peer";
import PeerVideo from "./VideoComponents/PeerVideo";
import { getCounsellorDetails } from "../../pages/api/counsellor";
import { getCustomerDetails } from "../../pages/api/customer";
import FeedbackModalContent from "./VideoComponents/modals/FeedbackModalContent";
import { useRouter } from "next/router";

const VideoRoom = (props) => {

    const router = useRouter();

    const consultationVerified = useState(false);
    const consultationID = useState(null);

    const globalStore = useState(store);

    const modalContent = useState(null);
    const showModal = useState(false);
    const callStatus = useState("");

    const sPeers = useState([]);

    const mediaConstraints = {
        "audio": true,
        "video": {
            width: { min: 160, ideal: 320, max: 640 },
            height: { min: 120, ideal: 240, max: 480 },
        }
    };

    const userVideoRef = useRef(null);
    const screenShareRef = useRef(null);

    const userStream = useState(null);
    const peersRef = useRef([]);

    const customer = useState(null);
    const counsellor = useState(null);

    useEffect(() => {
        getCounsellorDetails(props.counsellorID).then((response) => counsellor.set(response.data[0]));
        getCustomerDetails(getUser().id).then((response) => customer.set(response.data[0]));
    }, []);

    useLayoutEffect(() => {
        document.getElementById("main-header").style.display = "none";
    }, []);

    useEffect(() => {
        if (!globalStore.isScreenSharing.get() && sPeers.attach(Downgraded).get().length === 0) {
            globalStore.video_grid_height.set("95%");
        }
        else {
            globalStore.video_grid_height.set("fit-content");
        }
    }, [sPeers.attach(Downgraded).get()]);

    useEffect(() => {

        var counsellorID = props.counsellorID;
        var customerID = getUser().id;
        var roomID = props.roomID;

        verifyConsultation(counsellorID, customerID, roomID, "video_room_id")
            .then((response) => {
                consultationVerified.set(true);
                consultationID.set(response.data.description[0].id);
            })
            .catch(() => {
                console.log("Meeting marked as complete or invalid link")
            })
    }, []);

    useEffect(() => {
        if (sPeers.attach(Downgraded).get().length > 0) {
            globalStore.socketRef.attach(Downgraded).get().current.emit("cancel-my-call-notification-auto", props.counsellorID, "COUNSELLOR", customer.attach(Downgraded).get().name);
            showModal.set(false);
        }
    }, [sPeers.attach(Downgraded).get()]);

    useEffect(() => {
        if (consultationVerified.get()) {

            if (!router.query.acceptor && globalStore.activeConsultation.attach(Downgraded).get().counsellor_id !== props.counsellorID && sPeers.attach(Downgraded).get().length === 0) {
                setupModal("video-call-modal");
            }

            if (globalStore.socketRef.attach(Downgraded).get()) {
                globalStore.socketRef.attach(Downgraded).get().current.emit("get-availablity", getUser().id, "CUSTOMER", parseInt(props.counsellorID), "COUNSELLOR");

                globalStore.socketRef.attach(Downgraded).get().current.on("call-status-update", (status) => {
                    if (status === "accepted") {
                        showModal.set(false);
                    }
                    else {
                        iziToast.error({
                            title: "Call declined",
                            message: "Your call has been declined",
                            position: "topRight",
                            timeout: 2000,
                            onClosing: () => {
                                window.location.href = "/user/consultations?consultation_id=" + consultationID.get();
                            }
                        });
                        showModal.set(false);
                    }
                });
            }

            if (globalStore.directLinkVideoCallAvailability.get()) {
                if (!router.query.acceptor) {
                    globalStore.socketRef.attach(Downgraded).get().current.emit("new-call", getUser().id, "CUSTOMER", customer.attach(Downgraded).get() ? customer.attach(Downgraded).get().name : '', customer.attach(Downgraded).get() ? customer.attach(Downgraded).get().profile_image : '/images/profile.png', "video", props.roomID, parseInt(props.counsellorID), "COUNSELLOR");
                }
                globalStore.isIdle.set(false);
                callStatus.set("Ringing...");
            }
            else if (globalStore.directLinkVideoCallAvailability.get() === false) {
                callStatus.set("Counsellor is unavailable at the moment");
            }
            else {
                callStatus.set("Connecting...");
            }
        }
    }, [consultationVerified.get(), globalStore.directLinkVideoCallAvailability.get(), callStatus.get()]);

    useEffect(() => {
        if (consultationVerified.get()) {
            navigator.mediaDevices.getUserMedia(mediaConstraints).then(stream => {
                userVideoRef.current.srcObject = stream;
                userStream.set(stream);

                globalStore.socketRef.attach(Downgraded).get().current.emit("join room", props.roomID);
                globalStore.socketRef.attach(Downgraded).get().current.on("all users", users => {
                    const peers = [];

                    users.forEach(userID => {
                        const peer = createPeer(userID, globalStore.socketRef.attach(Downgraded).get().current.id, stream);
                        peersRef.current.push({
                            peerID: userID,
                            peerName: counsellor.attach(Downgraded).get() ? counsellor.attach(Downgraded).get().name : '',
                            peerType: 'normal',
                            peer
                        });
                        peers.push({
                            peerID: userID,
                            peerName: counsellor.attach(Downgraded).get() ? counsellor.attach(Downgraded).get().name : '',
                            peerType: 'normal',
                            peer
                        });
                    });
                    sPeers.set(peers);
                });

                globalStore.socketRef.attach(Downgraded).get().current.on("user joined", payload => {
                    const peer = addPeer(payload.signal, payload.callerID, stream);
                    peersRef.current.push({
                        peerID: payload.callerID,
                        peerName: counsellor.attach(Downgraded).get() ? counsellor.attach(Downgraded).get().name : '',
                        peerType: 'normal',
                        peer
                    });

                    const peerObj = {
                        peerID: payload.callerID,
                        peerName: counsellor.attach(Downgraded).get() ? counsellor.attach(Downgraded).get().name : '',
                        peerType: 'normal',
                        peer,
                    }

                    sPeers.set(users => [...users, peerObj]);
                });

                globalStore.socketRef.attach(Downgraded).get().current.on("receiving returned signal", payload => {
                    const item = peersRef.current.find(p => p.peerID === payload.id);
                    item.peer.signal(payload.signal);
                });

                globalStore.socketRef.attach(Downgraded).get().current.on("user left", id => {
                    const peerObj = peersRef.current.find(p => p.peerID === id);
                    if (peerObj) {
                        peerObj.peer.destroy();
                    }
                    const peers = peersRef.current.filter(p => p.peerID !== id);
                    peersRef.current = peers;
                    sPeers.set(peers);
                });
            });
        }
    }, [consultationVerified.get()]);

    const startScreenShare = () => {
        navigator.mediaDevices.getDisplayMedia({ cursor: true, video: true }).then(stream2 => {

            screenShareRef.current.srcObject = stream2;
            const screenTrack = stream2.getVideoTracks()[0];

            sPeers.attach(Downgraded).get().forEach(peer => {
                peer.peer.replaceTrack(userStream.attach(Downgraded).get().getVideoTracks()[0], screenTrack, userStream.attach(Downgraded).get());
            });

            globalStore.isScreenSharing.set(true);
            screenTrack.onended = function () {
                endScreenShare();
            }
        });
    }

    const endScreenShare = () => {
        navigator.mediaDevices.getUserMedia(mediaConstraints).then(newStream => {
            sPeers.attach(Downgraded).get().forEach(peer => {
                peer.peer.replaceTrack(userStream.attach(Downgraded).get().getVideoTracks()[0], newStream.getVideoTracks()[0], userStream.attach(Downgraded).get());
            });
        });
        globalStore.isScreenSharing.set(false);
    }

    const handleCallEnd = () => {
        setupModal("feedback-modal");
    }

    function createPeer(userToSignal, callerID, stream) {
        const peer = new Peer({
            initiator: true,
            trickle: false,
            stream,
        });

        peer.on("signal", signal => {
            globalStore.socketRef.attach(Downgraded).get().current.emit("sending signal", { userToSignal, callerID, signal })
        });

        return peer;
    }

    function addPeer(incomingSignal, callerID, stream) {
        const peer = new Peer({
            initiator: false,
            config: {
                iceServers: [
                    {
                        urls: 'stun:stun.l.google.com:19302'
                    },
                    {
                        urls: 'turn:43.204.30.91:3478?transport=tcp',
                        username: "cv-admin",
                        credential: "cv-admin"
                    }
                ]
            },
            trickle: false,
            stream,
        });

        peer.on("signal", signal => {
            globalStore.socketRef.attach(Downgraded).get().current.emit("returning signal", { signal, callerID })
        });

        peer.signal(incomingSignal);
        return peer;
    }

    function VideoCallModal(props) {
        return (
            <Modal {...props}
                size="md"
                backdrop="static"
                centered>
                {modalContent.attach(Downgraded).get()}
            </Modal>
        );
    }

    function setupModal(key) {
        if (key === "video-call-modal") {
            modalContent.set(<VideoModalContent consultationID={consultationID.get()} closeModal={() => showModal.set(false)} counsellor={counsellor.attach(Downgraded).get()} customer={customer.attach(Downgraded).get()} callStatus={callStatus.get()} />);
            showModal.set(true);
        }

        if (key === "feedback-modal") {
            modalContent.set(<FeedbackModalContent consultationID={consultationID.get()} closeModal={() => showModal.set(false)} counsellor={counsellor.attach(Downgraded).get()} />);
            showModal.set(true);
        }
    }

    function getUniqueListBy(arr, key) {
        return [...new Map(arr.map(item => [item[key], item])).values()]
    }

    return (
        <div>
            <div className={styles.cv_logo}>
                <Image src="/images/logo.png" width="100px" height="45px" />
            </div>
            <div className={`${styles.video_room} ${globalStore.isScreenSharing.get() ? 'd-flex align-items-center' : ''} pt-3`}>
                <ScreenShare visible={globalStore.isScreenSharing.get() ? "block" : "none"} userName="You are presenting" videoRef={screenShareRef} />
                <div className={`${styles.video_flex} ${globalStore.isScreenSharing.get() ? styles.side_user_video : ''}`}>
                    <UserVideo userName={customer.attach(Downgraded).get() ? customer.attach(Downgraded).get().name : ''} videoRef={userVideoRef} />
                    {getUniqueListBy(sPeers.attach(Downgraded).get(), 'peerID').filter(peer => peer.peerType === 'normal').map(sPeer =>
                        <PeerVideo key={sPeer.peerID} peer={sPeer.peer} peerName={sPeer.peerName} />
                    )}
                </div>
            </div>
            {userStream.attach(Downgraded).get() ?
                <ActionButtons chatType="video" userStream={userStream.attach(Downgraded).get()} startScreenShare={startScreenShare} endScreenShare={endScreenShare} handleCallEnd={handleCallEnd} />
                :
                null
            }
            <VideoCallModal show={showModal.get()} onHide={() => showModal.set(false)} />
        </div>
    );
}

export default VideoRoom;