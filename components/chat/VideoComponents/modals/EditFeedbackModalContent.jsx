import { Button, Col, Form, FormControl, Modal, Row, Spinner } from "react-bootstrap";
import StarRatings from 'react-star-ratings';
import { useState } from "@hookstate/core";
import * as Yup from 'yup';
import { useFormik } from 'formik';
import { getUser } from "../../../../utils/common";
import axiosJWT from "../../../../utils/jwtAxios";

const EditFeedbackModalContent = (props) => {

    const submitting = useState(false);

    const feedbackHeading = [
        "",
        "What did you hate? 😞",
        "What was not upto the mark? 😐",
        "What was not upto the mark? 😐",
        "What did you like? 😄",
        "What did you love? 😍",
    ]

    function getRatingColor(rating) {
        if (rating < 3 && rating != 0) {
            return "#cb202d";
        }
        else if (rating < 4 && rating != 0) {
            return "#F5CB39";
        }
        else if (rating != 0) {
            return "#01a400";
        }
        else {
            return "#cbd3e3";
        }
    }

    function getRatingText(rating) {
        if (rating < 3 && rating != 0) {
            return "Poor";
        }
        else if (rating < 4 && rating != 0) {
            return "Average";
        }
        else if (rating != 0) {
            return "Excellent";
        }
        else {
            return "Not Rated";
        }
    }

    const validationSchema = Yup.object({
        title: Yup.string(),
        review: Yup.string()
    });

    const formik = useFormik({
        initialValues: {
            rating: props.feedback.rating,
            title: props.feedback.title,
            review: props.feedback.review,
        },
        enableReinitialize: true,
        validationSchema: validationSchema,
        onSubmit: (values) => {
            if (values.rating > 0) {
                submitting.set(true);

                var formData = new FormData();
                formData.append("customer_id", getUser().id);
                formData.append("feedback_id", props.feedback.id);
                formData.append("rating", values.rating);
                formData.append("title", values.title);
                formData.append("review", values.review);

                axiosJWT.put(process.env.NEXT_PUBLIC_BACKEND_URL + "counsellor/review/update", formData)
                    .then(() => {
                        submitting.set(false);

                        iziToast.success({
                            title: "Edited",
                            message: "Your review has been successfully edited.",
                            position: "topRight",
                            timeout: 2000
                        });

                        props.closeModal();
                        props.getFeedbacks();
                    })
                    .catch(() => {
                        submitting.set(false);

                        iziToast.error({
                            title: "Error",
                            message: "Oops! Something went wrong",
                            position: "topRight",
                            timeout: 2000
                        });
                    });
            }
            else {
                formik.setFieldError('rating', 'Please provide a rating');
            }
        }
    });

    return (
        <Modal.Body style={{ padding: "20px 15px" }}>
            <Form onSubmit={formik.handleSubmit}>
                <Row>
                    <Col>
                        <div>
                            <h5>Edit your feedback</h5>
                        </div>
                    </Col>
                </Row>
                <Row style={{ marginTop: "20px" }}>
                    <Col>
                        How satisfied are you with <b>{props.feedback.counsellor_name}</b>?
                    </Col>
                </Row>
                <Row style={{ marginTop: "20px" }}>
                    <Col>
                        <div className='mt-2 d-flex align-items-center justify-content-between'>
                            <StarRatings
                                rating={formik.values.rating}
                                starDimension="32px"
                                starSpacing="2px"
                                isSelectable={false}
                                changeRating={(rating) => formik.setFieldValue('rating', rating)}
                                starRatedColor={getRatingColor(formik.values.rating)}
                                starHoverColor={getRatingColor(formik.values.rating)} />

                            <div style={{ color: getRatingColor(formik.values.rating), fontSize: "14px" }}>
                                {getRatingText(formik.values.rating)}
                            </div>
                        </div>
                        <Form.Control.Feedback type="invalid">
                            {formik.errors.rating}
                        </Form.Control.Feedback>
                    </Col>
                </Row>
                {formik.values.rating > 0 ?
                    <>
                        <Row style={{ marginTop: "20px" }}>
                            <Col>
                                <Form.Label>{feedbackHeading[formik.values.rating]}</Form.Label>
                                <FormControl name='title'
                                    value={formik.values.title}
                                    onChange={formik.handleChange}
                                    isInvalid={!!formik.errors.title}
                                    className='shadow-none cv-input-field'
                                    placeholder='Type here...'
                                    type='text' />
                            </Col>
                        </Row>
                        <Row style={{ marginTop: "20px" }}>
                            <Col>
                                <Form.Label>Tell us about your experience with our Mentor</Form.Label>
                                <FormControl as="textarea" name='review'
                                    value={formik.values.review}
                                    onChange={formik.handleChange}
                                    isInvalid={!!formik.errors.review}
                                    className='shadow-none cv-input-field'
                                    placeholder='Start typing here...'
                                    rows={10} />
                            </Col>
                        </Row>
                    </>
                    :
                    null
                }
                <Button disabled={submitting.get()} variant="light" className="shadow-none" style={{ float: "left", marginTop: "10px" }} onClick={() => props.closeModal()}>Cancel</Button>
                {submitting.get() ?
                    <Button disabled className="cv-primary-btn shadow-none" style={{ float: "right", marginTop: "10px" }}><Spinner size="sm" animation="border" /></Button>
                    :
                    <Button type="submit" className="cv-primary-btn shadow-none" style={{ float: "right", marginTop: "10px" }}>Edit</Button>
                }
            </Form>
        </Modal.Body>
    )
}

export default EditFeedbackModalContent;