import Image from 'next/image';
import styles from './UserAudio.module.css';

const UserAudio = (props) => {
    return (
        <div className={styles.userVoice}>
            <div className={`${styles.userVoiceImage} ${styles.pulse}`}>
                <Image src={props.image} width={100} height={100} />
            </div>
            <h5 className={styles.userName}>{props.name}</h5>
        </div>
    );
}

export default UserAudio