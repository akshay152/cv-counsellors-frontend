import { useEffect, useLayoutEffect } from 'react';
import Image from 'next/image';
import { Row, Col, Container } from 'react-bootstrap';
import styles from './AudioRoom.module.css';
import UserAudio from './AudioComponents/UserAudio';
import ActionButtons from './ActionButtons';

const AudioRoom = () => {

  useLayoutEffect(() => {
    document.getElementById("main-header").style.display = "none";
  }, []);

  const userAudios = [
    {
      id: 0,
      name: "Akshay Sharma",
      image: "/images/testimonial-1.jpg"
    },
    {
      id: 1,
      name: "Counsellor",
      image: "/images/testimonial-2.jpg"
    }
  ]
  
  return (
    <div className={styles.audioChatBody}>
      <div className={styles.cv_logo}>
        <Image src="/images/logo.png" width="100px" height="45px" />
      </div>
      <Container style={{height: "100%"}}>
        <Row className="align-items-center justify-content-center" style={{height: "100%"}}>
          {userAudios.map(audio =>
              <Col key={audio.id}>
                <UserAudio name={audio.name} image={audio.image}  />
              </Col>
          )}
        </Row>
      </Container>
      <ActionButtons chatType="audio" />
    </div>
  );
}

export default AudioRoom;