import { Badge, Button, Col, Form, Row } from 'react-bootstrap';
import Image from 'next/image';
import { AiFillStar } from 'react-icons/ai';
import { FaChevronLeft, FaChevronRight } from 'react-icons/fa';
import Link from 'next/link';
import ReactPaginate from 'react-paginate';
import styles from './CounsellorPage.module.css';
import { Downgraded, useState } from '@hookstate/core';
import { useRouter } from 'next/router';
import { useEffect, useRef } from 'react';
import axios from 'axios';
import store from '../../utils/store';
import { getUser } from '../../utils/common';
import EmptyList from '../global/customs/EmptyList';

const CounsellorPage = () => {

    const router = useRouter();
    const globalStore = useState(store);

    const counsellors = useState([]);

    const courses = useState([]);
    const modes = useState([]);
    const specializations = useState([]);

    const coursesRef = useRef();
    const modesRef = useRef();
    const specializationsRef = useRef();

    const currentPage = useState(0);
    const counsellorsPerPage = useState(10);

    const pagesVisited = currentPage.get() * counsellorsPerPage.get();
    const pageCount = Math.ceil(counsellors.attach(Downgraded).get().length / counsellorsPerPage.get());

    const changePage = ({ selected }) => {
        currentPage.set(selected);
    };

    const course_id = useState(null);
    const courseShortName = useState(null);

    const mode_id = useState(null);
    const specialization_id = useState(null);

    const sortMethod = useState("relevance");
    const genders = useState([]);

    useEffect(() => {
        axios.get(process.env.NEXT_PUBLIC_BACKEND_URL+"course-consultation")
        .then((response) => {
            courses.set(response.data);
        });
    }, []);

    useEffect(() => {
        if(router.query.course) {
            course_id.set(parseInt(router.query.course));
        }
        if(router.query.mode) {
            mode_id.set(parseInt(router.query.mode));
        }
        if(router.query.specialization) {
            specialization_id.set(parseInt(router.query.specialization));
        }
    }, [router]);

    useEffect(() => {
        if(course_id.get() && course_id.get() !== '') {
            axios.get(process.env.NEXT_PUBLIC_BACKEND_URL+"course/details/"+course_id.get())
            .then((response) => {
                courseShortName.set(response.data[0].short_name);
                axios.get(process.env.NEXT_PUBLIC_BACKEND_URL+"mode/"+response.data[0].short_name)
                .then((response) => {
                    modes.set(response.data);
                });
            });
        }
        else {
            counsellors.set([]);
        }
    }, [course_id.get()]);

    useEffect(() => {
        if(mode_id.get() && course_id.get()) {

            let new_course_id = course_id.get();

            if(modes.attach(Downgraded).get().length > 0 ) {
                new_course_id = modes.attach(Downgraded).get().filter(mode => mode.id === parseInt(mode_id.get()))[0].course_id;
            }
            axios.get(process.env.NEXT_PUBLIC_BACKEND_URL+"specialization?mode_id="+mode_id.get()+"&course_id="+new_course_id)
            .then((response) => {
                specializations.set(response.data);
            });
        }
    },[mode_id.get()]);

    function fetchCounsellors() {
        if(courseShortName.get() && courseShortName.get() != '' && mode_id.get() && mode_id.get() != '' && specialization_id.get() && specialization_id.get() != '') {
            
            if(globalStore.socketRef.attach(Downgraded).get() && course_id.get() && mode_id.get()) {
                globalStore.socketRef.attach(Downgraded).get().current.emit("get-online-counsellors", getUser().id, course_id.get(), mode_id.get());
            }
            
            var formData = new FormData();
            formData.append("course_short_name", courseShortName.get());
            formData.append("mode_id", mode_id.get());
            formData.append("specialization_id", specialization_id.get());
            formData.append("genders", genders.attach(Downgraded).get());
            formData.append("sort_method", sortMethod.get());

            axios.post(process.env.NEXT_PUBLIC_BACKEND_URL+"counsellor/get-from-filter", formData)
            .then((response) => {
                counsellors.set(response.data);
            });
        }
        else {
            counsellors.set([]);
        }
    }

    function addToGenders(gender) {
        genders.set([...genders.attach(Downgraded).get(), gender]);
        fetchCounsellors();
    }

    function removeFromGenders(genderVal) {
        genders.set(genders.attach(Downgraded).get().filter(gender => gender !== genderVal));
        fetchCounsellors();
    }

    useEffect(() => {
        fetchCounsellors();
    }, [courseShortName.get(), mode_id.get(), specialization_id.get(), sortMethod.get()]);

    return (
        <div className={styles.wrapper}>
            <div style={{backgroundColor: "#0074d7", width: "100%", height: "100%", padding: "16px", borderRadius: "4px"}}>
                <Row className='d-flex' style={{justifyContent: "space-between"}}>
                    <Col className={`${styles.col}`}>
                        <Form.Label className={styles.filter_label}>Course*</Form.Label>
                        <Form.Select ref={coursesRef} className={styles.filter_select}
                        onChange={(e) => {
                            modesRef.current.value = "";
                            mode_id.set(null);
                            specializationsRef.current.value= "";
                            specialization_id.set(null);
                            
                            course_id.set(parseInt(e.target.value))
                        }}
                        >
                            <option value="" defaultValue={true}>Select an option</option>
                            {courses.attach(Downgraded).get().map(course =>
                                <option key={course.id} selected={courseShortName.get() === course.short_name} value={course.id}>{course.short_name}</option>
                            )}
                        </Form.Select>
                    </Col>
                    <Col className={`${styles.col}`}>
                        <Form.Label className={styles.filter_label}>Mode of Education*</Form.Label>
                        <Form.Select ref={modesRef} className={styles.filter_select}
                        onChange={(e) => {
                            specializationsRef.current.value= "";
                            specialization_id.set(null);

                            mode_id.set(parseInt(e.target.value));
                        }}
                        >
                            <option value="" defaultValue={true}>Select an option</option>
                            {modes.attach(Downgraded).get().map(mode =>
                                <option key={mode.id} selected={mode_id.get() === mode.id} value={mode.id}>{mode.name}</option>
                            )}
                        </Form.Select>
                    </Col>
                    <Col className={`${styles.col}`}>
                        <Form.Label className={styles.filter_label}>Specialization*</Form.Label>
                        <Form.Select ref={specializationsRef} className={styles.filter_select}
                        onChange={(e) => specialization_id.set(parseInt(e.target.value))}>
                            <option value="" defaultValue={true}>Select an option</option>
                            {specializations.attach(Downgraded).get().map(specialization =>
                                <option key={specialization.id} selected={specialization_id.get() === specialization.id} value={specialization.id}>{specialization.name}</option>
                            )}
                        </Form.Select>
                    </Col>
                </Row>
                <Row>
                <Col lg="4">
                        <Form.Label className={styles.filter_label}>Sort By</Form.Label>
                        <Form.Select onChange={(e) => sortMethod.set(e.target.value)} className={styles.filter_select}>
                            <option defaultValue={true} value="relevance">Relevance</option>
                            <option value="price-low">Price - Low to High</option>
                            <option value="price-high">Price - High to Low</option>
                            <option value="experience">Years of experience</option>
                            <option value="review">Recommendation</option>
                        </Form.Select>
                    </Col>
                    <Col lg="4" className="mt-3">
                        <div>
                        <Form.Label className={styles.filter_label}>Gender</Form.Label>
                        </div>
                        <div>
                        <Form.Check onChange={(e) => e.target.checked ? addToGenders(e.target.value) : removeFromGenders(e.target.value) } value={"male"} inline={true} className='filter_check' label="Male" />
                        <Form.Check onChange={(e) => e.target.checked ? addToGenders(e.target.value) : removeFromGenders(e.target.value) } value={"female"} inline={true} className='filter_check' label="Female" />
                        </div>
                    </Col>
              
                </Row>
            </div>
            <div style={{padding: "16px 25px"}}>
                {counsellors.attach(Downgraded).get().length > 0 ?
                counsellors.attach(Downgraded).get().slice(pagesVisited, pagesVisited + counsellorsPerPage.get()).map(counsellor => 
                    <Row key={counsellor.id} className={`${styles.counsellor_card} border-bottom mt-3 py-3`}>
                        <Col lg="3">
                            <div className={styles.counsellor_left}>
                                <div className={styles.counsellor_card_img}>
                                    {counsellor.image !== "" ?
                                    <Image className='rounded-circle' src={counsellor.image} width={125} height={125} />
                                    :
                                    <Image className='rounded-circle' src="/images/profile.png" width={125} height={125} />
                                    }
                                    {counsellor.verified ? <img className={styles.verified} src="/images/check.png" width={20} height={20} /> : null}
                                </div>
                                
                                <Link href={counsellor.profile_link+"?course="+courseShortName.get()+"&mode="+mode_id.get()+"&specialization="+specialization_id.get()} passHref>
                                    <a target="_blank" className='fw-bold mt-3' style={{textDecoration: "none", color: "#0074d7", fontSize: "14px"}} >View Profile</a>
                                </Link>
                            </div>
                        </Col>
                        <Col lg="4">
                            <div className={styles.counsellor_right}>
                                <Link href={counsellor.profile_link+"?course="+courseShortName.get()+"&mode="+mode_id.get()+"&specialization="+specialization_id.get()} passHref>
                                    <a target="_blank" style={{textDecoration: "none"}}>
                                        <div className={styles.counsellor_name}>{counsellor.name}</div>
                                    </a>
                                </Link>
                                <div className={styles.counsellor_specialization}>
                                    {counsellor.specializations && JSON.parse(counsellor.specializations) ? <><span>{JSON.parse(counsellor.specializations).filter(spec => spec.name === courseShortName.get())[0] && JSON.parse(counsellor.specializations).filter(spec => spec.name === courseShortName.get())[0].name} </span><span>({JSON.parse(counsellor.specializations).filter(spec => spec.name === courseShortName.get() && spec.mode_id === mode_id.get())[0] && JSON.parse(counsellor.specializations).filter(spec => spec.name === courseShortName.get() && spec.mode_id === mode_id.get())[0].mode})</span>{JSON.parse(counsellor.specializations).length > 1 ? <span className={styles.more_affiliation}> + {JSON.parse(counsellor.specializations).length - 1} more</span> : null}</> : null}
                                </div>
                                <div className={styles.counsellor_experience}>
                                    {counsellor.experience} years experience overall
                                </div>
                                <div className={styles.counsellor_experience}>
                                    {counsellor.qualification}
                                </div>
                                <div className={styles.consultation_fees}>
                                    ₹{counsellor.fees} consultation fee
                                </div>
                                <div className={styles.counsellor_reviews}>
                                <Badge bg={counsellor.reviews && JSON.parse(counsellor.reviews).rating > 3 ? "success" : "danger"}><AiFillStar /> {counsellor.reviews && JSON.parse(counsellor.reviews).rating}</Badge>
                                    <Link href={counsellor.profile_link+"?course="+courseShortName.get()+"&mode="+mode_id.get()+"&specialization="+specialization_id.get()} passHref>
                                        <a target="_blank" style={{textDecoration: "none", fontSize: "12px", color: "#0074d7", marginLeft: "10px"}}>
                                            {counsellor.reviews && JSON.parse(counsellor.reviews).count} student reviews
                                        </a>
                                    </Link>
                                </div>
                            </div>
                        </Col>
                        <Col>
                            <div className='d-flex align-items-center' style={{height: "100%", justifyContent: "center", flexDirection: "column"}}>
                                {globalStore.activeCounsellors.attach(Downgraded).get().filter(activeCounsellor => activeCounsellor.counsellor_id === counsellor.id).length > 0 ?
                                <Button className={`${styles.video_call_btn} myshine`}>Instant Video Consultation</Button>
                                :
                                null
                                }
                                <Link href={counsellor.profile_link+"?course="+courseShortName.get()+"&mode="+mode_id.get()+"&specialization="+specialization_id.get()} passHref>
                                    <a target="_blank">
                                        <Button style={{width: "212px"}} className='cv-primary-btn fw-bold'>Book appointment<div className='fw-bold' style={{fontSize: "10px", whiteSpace: "pre-line"}}>Schedule a consultation</div></Button>
                                    </a>
                                </Link>
                            </div>
                        </Col>

                    </Row>
                )
                :
                <EmptyList title="No Counsellors Found!" subtitle="Change current filter criteria to get more options" />
                }
                {pageCount > 1 ?
                <Row className='mt-5 mb-5'>
                    <Col>
                        <ReactPaginate
                            previousLabel={<FaChevronLeft />}
                            nextLabel={<FaChevronRight />}
                            pageCount={pageCount}
                            onPageChange={changePage}
                            containerClassName={"paginationBtns"}
                            previousLinkClassName={"previousBtn"}
                            nextLinkClassName={"nextBtn"}
                            disabledClassName={"paginationDisabled"}
                            activeClassName={"paginationActive"}
                        />
                    </Col>
                </Row>
                :
                null}
            </div>
        </div>
    );
}

export default CounsellorPage;