import moment from 'moment';
import Image from 'next/image';
import { Badge, Button, Col, Form, FormControl, InputGroup, Modal, Row, Spinner } from 'react-bootstrap';
import { AiFillStar } from 'react-icons/ai';
import { BsCalendarPlus } from 'react-icons/bs';
import { FiClock } from 'react-icons/fi';
import useRazorpay from 'react-razorpay';
import { generate } from 'shortid';
import { useFormik } from 'formik';
import { useRouter } from 'next/router';
import * as Yup from 'yup';
import { getUser } from '../../../utils/common';
import axiosJWT from '../../../utils/jwtAxios';
import styles from './BookSlot.module.css';
import { Downgraded, useState } from '@hookstate/core';
import { useEffect } from 'react';
import isAuthenticated from '../../../utils/auth';
import { getCounsellorCourses, getCounsellorModesForACourse, getCounsellorSpecializationsForModeCourse } from '../../../pages/api/counsellor';
import { bookAppointment } from '../../../pages/api/consultation';
import CustomModal from '../../global/customs/CustomModal';
import AuthModal from '../../global/header/modals/AuthModal';
import { FaHandsHelping } from 'react-icons/fa';

const BookSlot = (props) => {

    const booking = useState(false);
    const router = useRouter();

    const modalContent = useState(null);
    const showModal = useState(false);

    const Razorpay = useRazorpay();

    function getRatingColor(rating) {
        if (rating < 3) {
            return "#cb202d";
        }
        else if (rating < 4) {
            return "#F5CB39";
        }
        else {
            return "#01a400";
        }
    }

    const getDateTime = (dateLabel, slot) => {
        dateLabel = props.dateLabel === 'Today' ? moment().format('Do MMM YYYY') : props.dateLabel === 'Tomorrow' ? moment().add(1, 'days').format('Do MMM YYYY') : props.dateLabel;

        var day = dateLabel.split(" ")[0].substring(0, dateLabel.split(" ")[0].length - 2);
        var month = dateLabel.split(" ")[1];
        var year = dateLabel.split(" ")[2];

        var finalDate = day + " " + month + " " + year + " " + slot;

        return moment(Date.parse(finalDate)).format('YYYY-MM-DD HH:mm:ss');
    }

    function paymentSuccessful(paymentID) {
        let formData = new FormData();
        formData.append("counsellor_id", props.counsellor.id);
        formData.append("customer_id", getUser().id);
        formData.append("specialization_id", formik.values.specialization);
        formData.append("start_time", getDateTime(props.dateLabel, props.slot));
        formData.append("transaction_id", paymentID);

        bookAppointment(formData).then((response) => {

            booking.set(false);

            iziToast.success({
                title: "Consultation slot booked",
                message: `Your consulation will begin on ${props.dateLabel} at ${props.slot}`,
                timeout: 2000,
                position: "topRight",
            });

            props.closeModal();

            router.push("/user/consultations?consultation_id="+response.data.description);
        })
            .catch((error) => {

                booking.set(false);

                if (error.response.status === 422) {
                    iziToast.error({
                        title: "Consultation booking failed",
                        message: error.response.data,
                        timeout: 3000,
                        position: "topRight",
                    });
                }
                else {
                    iziToast.error({
                        title: "Consultation booking failed",
                        message: `We were unable to book this slot for you. If any money has been deducted. It will be refunded within 5-7 working days`,
                        timeout: 3000,
                        position: "topRight",
                    });
                }
            });
    }

    const handlePayment = async (params) => {

        const order = await createOrder(params);
        const options = {
            key: process.env.NEXT_PUBLIC_RAZORPAY_KEY_ID,
            amount: params.amount,
            currency: params.currency,
            name: "CV Counsellors",
            description: "Online consultation for education",
            order_id: order.id,
            handler: function (response) {
                paymentSuccessful(response.razorpay_payment_id);
            },
            prefill: {
                name: getUser().name,
                email: getUser().email,
                contact: getUser().mobile,
            },
            theme: {
                color: "#14bef1",
            },
        }

        const rzp1 = new Razorpay(options);

        rzp1.on("payment.failed", function () {

            booking.set(false);

            iziToast.error({
                title: "Consultation booking failed",
                message: "We were unable to book this slot for you. If any money has been deducted. It will be refunded within 5-7 working days",
                timeout: 2000,
                position: "topRight",
            });
        });

        rzp1.open();
    }

    async function createOrder(params) {

        var formData = new FormData();

        formData.append("amount", params.amount);
        formData.append("currency", params.currency);
        formData.append("receipt", params.receipt);

        const order = await axiosJWT.post(process.env.NEXT_PUBLIC_BACKEND_URL + "consultation/create/instant", formData)
            .then((response) => {
                return response.data;
            })
            .catch(() => {
                booking.set(false);

                iziToast.error({
                    title: "Payment failed",
                    message: "Couldn't book your slot.",
                    timeout: 2000,
                    position: "topRight",
                });
            });

        return order;
    }

    const validationSchema = Yup.object({
        course: Yup.string().required('Please select a course'),
        mode: Yup.string().required('Please select a mode'),
        specialization: Yup.string().required('Please select a specialization')
    });

    function setupModal(redirectURL) {
        modalContent.set(<AuthModal redirectURL={redirectURL} closeModal={() => showModal.set(false)} />);
        showModal.set(true);
    }

    const formik = useFormik({
        initialValues: {
            course: props.course ? props.course : "",
            mode: props.mode ? props.mode : "",
            specialization: props.specialization ? props.specialization : "",
        },
        validationSchema: validationSchema,
        enableReinitialize: true,
        onSubmit: () => {
            if (isAuthenticated()) {
                booking.set(true);
                if(props.counsellor.fees === "0") {
                    handlePayment({ amount: props.counsellor.fees * 100, currency: "INR", receipt: "receipt_" + generate() })
                }
                else {
                    paymentSuccessful(generate());
                }
            }
            else {
                setupModal(router.asPath);
            }
        }
    });

    const courses = useState([]);
    const modes = useState([]);
    const specializations = useState([]);

    const modeDescription = useState("");

    useEffect(() => {
        getCounsellorCourses(props.counsellor.id).then((response) => {
            courses.set(response.data);
        });
    }, []);

    useEffect(() => {

        modes.set([]);
        formik.setFieldValue('mode', '');

        modeDescription.set("");

        specializations.set([]);
        formik.setFieldValue('specialization', '');

        if (formik.values.course !== "") {

            getCounsellorModesForACourse(props.counsellor.id, formik.values.course).then((response) => {
                modes.set(response.data);
            })
        }
    }, [formik.values.course]);

    useEffect(() => {

        modeDescription.set("");

        specializations.set([]);
        formik.setFieldValue('specialization', '');

        if (formik.values.mode !== "") {
            if (modes.attach(Downgraded).get().length > 0) {
                modeDescription.set(modes.attach(Downgraded).get().filter(mode => mode.id === parseInt(formik.values.mode))[0].description);
            }
            getCounsellorSpecializationsForModeCourse(formik.values.course, formik.values.mode).then((response) => {
                specializations.set(response.data);
            });
        }
    }, [formik.values.mode]);

    return (
        <Modal.Body>
            <Form onSubmit={formik.handleSubmit}>
                <Row>
                    <Col>
                        <div>
                            <span className="close_modal_button" style={{ marginRight: "7px" }} onClick={() => props.closeModal()} />
                        </div>
                    </Col>
                </Row>
                <Row className='mt-3'>
                    <Col>
                        <h5 className='fw-bold'>Confirm &amp; Pay</h5>
                    </Col>
                </Row>
                <div className='mt-2 mb-3 border-top border-bottom' style={{ padding: "15px" }}>
                    <div className='d-flex align-items-center justify-content-between'>
                        <span className='d-flex align-items-center' style={{ fontSize: "14px" }}>
                            <BsCalendarPlus />
                            &nbsp;&nbsp;On <b>&nbsp;{props.dateLabel === 'Today' ? moment().format('Do MMM YYYY') : props.dateLabel === 'Tomorrow' ? moment().add(1, 'days').format('Do MMM YYYY') : props.dateLabel}</b>
                        </span>
                        <span className='d-flex align-items-center' style={{ fontSize: "14px" }}>
                            <FiClock />
                            &nbsp;&nbsp;At <b>&nbsp;{props.slot}</b>
                        </span>
                    </div>
                    <div onClick={() => props.closeModal()} className='mt-2' style={{ color: "#14bef1", fontSize: "14px", fontWeight: "bold", cursor: "pointer" }}>
                        Change Date &amp; Time
                    </div>
                </div>
                <div className='d-flex mt-2'>
                    <div className={styles.image_wrapper}>
                        {props.counsellor.image === "" ?
                            <Image src="/images/profile.png" width={100} height={100} />
                            :
                            <Image src={props.counsellor.image} width={100} height={100} />
                        }
                    </div>
                    <div className={styles.details_wrapper}>
                        <div style={{ fontSize: "18px" }} className='fw-bold'>{props.counsellor.name}</div>
                        <div style={{ fontSize: "14px" }}>{props.counsellor.designation}</div>
                        <div style={{ fontSize: "14px" }}>{props.counsellor.experience} Years Experience Overall</div>
                        <div className='d-flex align-items-center'>
                            <AiFillStar fontSize={20} color={getRatingColor(props.counsellor.avg_rating)} />
                            <span style={{ color: getRatingColor(props.counsellor.avg_rating) }}>&nbsp;{props.counsellor.avg_rating}</span>
                            <span style={{ fontSize: "12px", color: "#787887", marginTop: "2px" }}>&nbsp;({props.reviews.length} votes)</span>
                        </div>
                    </div>
                </div>
                <Row>
                    <Col>
                        <Form.Label>Course*</Form.Label>
                        <Form.Select name="course"
                            className="shadow-none cv-input-field"
                            value={formik.values.course}
                            onChange={formik.handleChange}
                            isInvalid={!!formik.errors.course}>
                            <option defaultValue={true} value="">Select an option</option>
                            {courses.attach(Downgraded).get().map(course =>
                                <option key={course.id} value={course.name}>{course.name}</option>
                            )}
                        </Form.Select>
                        <Form.Control.Feedback type="invalid">
                            {formik.errors.course}
                        </Form.Control.Feedback>
                    </Col>

                    <Col>
                        <Form.Label>Education Mode*</Form.Label>
                        <Form.Select name="mode"
                            className="shadow-none cv-input-field"
                            value={formik.values.mode}
                            onChange={formik.handleChange}
                            isInvalid={!!formik.errors.mode}>
                            <option defaultValue={true} value="">Select an option</option>
                            {modes.attach(Downgraded).get().map(mode =>
                                <option key={mode.id} value={mode.id}>{mode.name}</option>
                            )}
                        </Form.Select>
                        <Form.Control.Feedback type="invalid">
                            {formik.errors.mode}
                        </Form.Control.Feedback>
                        {/* <Form.Text>{modeDescription && modeDescription.get() !== "" ? <><Badge bg="success">Note :</Badge>&nbsp;<b>{modeDescription.get()}</b></> : null }</Form.Text> */}
                    </Col>
                </Row>
                <Row className='mt-2'>
                    <Col>
                        <Form.Label>Specialization*</Form.Label>
                        <Form.Select name="specialization"
                            className="shadow-none cv-input-field"
                            value={formik.values.specialization}
                            onChange={formik.handleChange}
                            isInvalid={!!formik.errors.specialization}>
                            <option defaultValue={true} value="">Select an option</option>
                            {specializations.attach(Downgraded).get().map(specialization =>
                                <option key={specialization.id} value={specialization.id}>{specialization.name}</option>
                            )}
                        </Form.Select>
                        <Form.Control.Feedback type="invalid">
                            {formik.errors.specialization}
                        </Form.Control.Feedback>
                    </Col>
                </Row>
                {/* <div className='mt-2 mb-2'>
                    <div style={{ fontSize: "14px", color: "#14bef1" }}>Have a coupon code?</div>
                    <InputGroup className='mt-2'>
                        <FormControl className='shadow-none cv-input-field'
                            placeholder='AJS89A0XXXX' />
                        <InputGroup.Text style={{ cursor: "pointer", backgroundColor: "#fff" }}>Apply</InputGroup.Text>
                    </InputGroup>
                </div> */}
                <div className='mt-3 mb-2'>
                    <div style={{ color: "#787887", fontSize: "14px" }}>
                        Final Fee
                    </div>
                    <div className='fw-bold' style={{ fontSize: "24px" }} >
                        ₹{props.counsellor.fees}
                    </div>
                </div>
                <div className='mt-4 mb-2'>
                    {booking.get() ?
                        <Button className='cv-primary-btn shadow-none fw-bold w-100'><Spinner animation='border' size='sm' /></Button>
                        :
                        <Button type='submit' className='cv-primary-btn shadow-none fw-bold w-100'>Proceed now&nbsp;&nbsp;<FaHandsHelping fontSize={23} /></Button>
                    }
                </div>
            </Form>
            <CustomModal content={modalContent} showModal={showModal} modalSize="md" staticBackdrop={false} />
        </Modal.Body>
    )
}

export default BookSlot;