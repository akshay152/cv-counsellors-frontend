import styles from './Feedback.module.css';
import { Button, Col, Form, FormControl, Modal, Row, Spinner } from 'react-bootstrap';
import { BiLike, BiDislike } from 'react-icons/bi';
import * as Yup from 'yup';
import { useFormik } from 'formik';
import StarRatings from 'react-star-ratings';
import { useState } from '@hookstate/core';
import { getUser } from '../../../utils/common';
import axiosJWT from '../../../utils/jwtAxios';

const Feedback = (props) => {

    const submitting = useState(false);

    function getRatingColor(rating) {
        if(rating < 3 && rating != 0) {
            return "#cb202d";
        }
        else if(rating < 4 && rating != 0) {
            return "#F5CB39";
        }
        else if(rating != 0) {
            return "#01a400";
        }
        else {
            return "#cbd3e3";
        }
    }

    function getRatingText(rating) {
        if(rating < 3 && rating != 0) {
            return "Poor";
        }
        else if(rating < 4 && rating != 0) {
            return "Average";
        }
        else if(rating != 0) {
            return "Excellent";
        }
        else {
            return "Not Rated";
        }
    }

    const validationSchema = Yup.object({
        title: Yup.string().required('Please enter a headline to your review'),
        review: Yup.string().required('Please add a written review')
    });

    const formik = useFormik({
        initialValues: {
            rating: 0,
            recommendation: "",
            title: "",
            review: "",
            improvement: ""
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            if(values.rating > 0) {
                submitting.set(true);

                var formData = new FormData();
                formData.append("counsellor_id", props.counsellor.id);
                formData.append("customer_id", getUser().id);
                formData.append("rating", values.rating);
                formData.append("recommendation", values.recommendation);
                formData.append("title", values.title);
                formData.append("review", values.review);
                formData.append("improvement", values.improvement);

                axiosJWT.post(process.env.NEXT_PUBLIC_BACKEND_URL+"counsellor/review/create", formData)
                .then(() => {
                    submitting.set(false);

                    iziToast.success({
                        title: "Success",
                        message: "Your feedback was successfully added",
                        position: "topRight",
                        timeout: 2000
                    });

                    props.updateCounsellor();
                    props.closeModal(true);
                })
                .catch(() => {
                    submitting.set(false);

                    iziToast.error({
                        title: "Error",
                        message: "Oops! Something went wrong",
                        position: "topRight",
                        timeout: 2000
                    });
                })
            }
            else {
                formik.setFieldError('rating', 'Please provide a rating');
            }
        }
    })

    return (
        <Modal.Body style={{padding: "30px 90px"}}>
            <Form onSubmit={formik.handleSubmit}>
                <Row>
                    <Col>
                        <div>
                            <span className="close_modal_button" style={{marginRight: "7px"}} onClick={() => props.closeModal()} />
                        </div>
                    </Col>
                </Row>
                <Row className='mt-3'>
                    <Col>
                        <span style={{fontSize: "24px"}}>How was your counselling experience with {props.counsellor.name}?</span>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <span style={{fontSize: "14px", color: "#787887"}}>Your experience will help over 83678+ people choose the right Mentor, daily.</span>
                    </Col>
                </Row>
                <Row className='mt-4'>
                    <Col>
                        <span>
                            <span className={styles.question_tag}>Q1.</span>
                            <span className={styles.question}>How would you rate our Mentor?*</span>
                        </span>
                    </Col>
                </Row>
                <div className='mt-2 d-flex align-items-center justify-content-between'>
                    <StarRatings
                    rating={formik.values.rating}
                    starDimension="32px"
                    starSpacing="2px"
                    isSelectable={false}
                    changeRating={(rating) => formik.setFieldValue('rating', rating)}
                    starRatedColor={getRatingColor(formik.values.rating)}
                    starHoverColor={getRatingColor(formik.values.rating)} />
                    <div style={{color: getRatingColor(formik.values.rating), fontSize: "14px"}}>
                        {getRatingText(formik.values.rating)}
                    </div>
                </div>
                <Form.Control.Feedback type="invalid">
                    {formik.errors.rating}
                </Form.Control.Feedback>
                <Row className='mt-4'>
                    <Col>
                        <span>
                            <span className={styles.question_tag}>Q2.</span>
                            <span className={styles.question}>Would you like to recommend our Mentor?</span>
                        </span>
                    </Col>
                </Row>
                <div className={styles.recommend_wrapper}>
                    <div onClick={() => formik.setFieldValue('recommendation', true)} className={`${styles.recommend_option_yes} ${formik.values.recommendation === true ? styles.active : ''}`}>
                        <BiLike />&nbsp;&nbsp;Yes
                    </div>
                    <div onClick={() => formik.setFieldValue('recommendation', false)} className={`${styles.recommend_option_no} ${formik.values.recommendation === false ? styles.active : ''}`}>
                        <BiDislike />&nbsp;&nbsp;No
                    </div>
                </div>
                <Row className='mt-4'>
                    <Col>
                        <span>
                            <span className={styles.question_tag}>Q3.</span>
                            <span className={styles.question}>How was your interaction with our Mentor?*</span>
                        </span>
                        <div className='mt-2'>
                            <FormControl name='title'
                            value={formik.values.title}
                            onChange={formik.handleChange}
                            isInvalid={!!formik.errors.title}
                            className='shadow-none cv-input-field'
                            placeholder='Add a title'
                            type='text' />
                             <Form.Control.Feedback type="invalid">
                                {formik.errors.title}
                            </Form.Control.Feedback>
                        </div>
                    </Col>
                </Row>
                <Row className='mt-4'>
                    <Col>
                        <span>
                            <span className={styles.question_tag}>Q4.</span>
                            <span className={styles.question}>Describe your experience with our Mentor*</span>
                        </span>
                        <div className='mt-2'>
                            <FormControl as="textarea" name='review'
                            value={formik.values.review}
                            onChange={formik.handleChange}
                            isInvalid={!!formik.errors.review}
                            className='shadow-none cv-input-field'
                            placeholder='Start typing here...'
                            rows={10} />
                             <Form.Control.Feedback type="invalid">
                                {formik.errors.review}
                            </Form.Control.Feedback>
                        </div>
                        <div className={styles.feedback_info}>
                            <span><b>Info: </b></span>All student stories go under strict moderation process before publishing to check abusive language, threats, superlative comments on counselling abilities and so on.
                        </div>
                    </Col>
                </Row>
                <Row className='mt-4'>
                    <Col>
                        <span>
                            <span className={styles.question_tag}>Q5.</span>
                            <span className={styles.question}>How can we improve our services? Or any feedback?</span>
                        </span>
                        <div className='mt-2'>
                            <FormControl as="textarea" name='improvement'
                            value={formik.values.improvement}
                            onChange={formik.handleChange}
                            isInvalid={!!formik.errors.improvement}
                            className='shadow-none cv-input-field'
                            placeholder='Share your thoughts here...'
                            rows={10} />
                             <Form.Control.Feedback type="invalid">
                                {formik.errors.improvement}
                            </Form.Control.Feedback>
                        </div>
                    </Col>
                </Row>
                <Row className='mt-4'>
                    <Col className='text-end'>
                        {submitting.get() ?
                        <Button className='cv-primary-btn shadow-none fw-bold w-25'><Spinner animation='border' size='sm' /></Button>
                        :
                        <Button type='submit' className='cv-primary-btn shadow-none fw-bold w-25'>Submit</Button>
                    }
                    </Col>
                </Row>
            </Form>
        </Modal.Body>
    )
}

export default Feedback;