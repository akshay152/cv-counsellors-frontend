import Image from 'next/image';
import { useCallback, useEffect, useRef } from 'react';
import { Button, Col, ProgressBar, Row , Card} from 'react-bootstrap';
import { AiFillStar } from 'react-icons/ai';
import { HiCheckCircle } from 'react-icons/hi';
import Link from 'next/link';
import { MdLocationPin, MdVerified } from 'react-icons/md';
import { Downgraded, useState } from '@hookstate/core';
import { ImForward } from 'react-icons/im';
import StarRatings from 'react-star-ratings';
import { IoVideocamOutline } from 'react-icons/io5';
import { Swiper, SwiperSlide } from 'swiper/react';
import { FaChevronLeft, FaChevronRight } from 'react-icons/fa';
import moment from "moment";
import { generate } from 'shortid';
import axios from 'axios';
import { useRouter } from 'next/router';
import styles from './CounsellorDetails.module.css';
import BookSlot from './modals/BookSlot';
import CustomModal from '../global/customs/CustomModal';
import isAuthenticated from '../../utils/auth';
import AuthModal from '../global/header/modals/AuthModal';
import Feedback from './modals/Feedback';
import { FcGraduationCap } from "react-icons/fc";

const CounsellorDetails = (props) => {

    const router = useRouter();

    const tabValue = useState(0);

    const counsellor_slug = props.slug;

    const reviewsPerLoad = useState(10);
    const maxReviews = useState(10);

    const sliderRef = useRef(null);
    const activeDayIndex = useState(0);

    const leftNavColor = useState("#0074d7");
    const rightNavColor = useState("#0074d7");

    const counsellor = useState(null);
    const basicSetup = useState(false);

    const reviews = useState([]);
    const allReviews = useState([]);

    const consultations = useState([]);
    const appointmentDates = useState([]);
    var timingGroups = useState([]);

    const reviewFilter = useState(null);

    const modalContent = useState(null);
    const showModal = useState(false);
    const modalSize = useState("md");

    const bgColors = ['rgb(232, 252, 166)', 'rgb(255, 249, 179)', 'rgb(200, 250, 200)'];

    const course_id = useState(null);
    const mode_id = useState(null);
    const specialization_id = useState(null);

    useEffect(() => {
        if(counsellor_slug.get()) {
            axios.get(process.env.NEXT_PUBLIC_BACKEND_URL+"counsellor/consultations/"+counsellor_slug.get())
            .then((response) => {
                consultations.set(response.data);
            })

            axios.get(process.env.NEXT_PUBLIC_BACKEND_URL+"counsellor/"+counsellor_slug.get())
            .then((response) => {
                counsellor.set(response.data[0]);
            })
            .catch(() => {
                router.replace("/counsellors");
            });

            axios.get(process.env.NEXT_PUBLIC_BACKEND_URL+"review/"+counsellor_slug.get())
            .then((response) => {
                reviews.set(response.data);
                allReviews.set(response.data);
            });
        }
        
    }, [counsellor_slug.get()]);

    function updateCounsellor() {
        axios.get(process.env.NEXT_PUBLIC_BACKEND_URL+"counsellor/consultations/"+counsellor_slug.get())
        .then((response) => {
            consultations.set(response.data);
        })

        axios.get(process.env.NEXT_PUBLIC_BACKEND_URL+"counsellor/"+counsellor_slug.get())
        .then((response) => {
            counsellor.set(response.data[0]);
        })
        .catch(() => {
            router.replace("/counsellors");
        });

        axios.get(process.env.NEXT_PUBLIC_BACKEND_URL+"review/"+counsellor_slug.get())
        .then((response) => {
            reviews.set(response.data);
            allReviews.set(response.data);
        });
    }

    useEffect(() => {
        if(counsellor.get() && !basicSetup.get()) {
            basicSetup.set(true);
            parseTimings();
            createAppointmentDates();
        }
    }, [counsellor.get()]);

    useEffect(() => {
        if(activeDayIndex.get() === 0) {
            leftNavColor.set("#b4b4be");
        }
        else if(activeDayIndex.get() === appointmentDates.length - 1) {
            rightNavColor.set("#b4b4be");
        }
        else {
            leftNavColor.set("#0074d7");
            rightNavColor.set("#0074d7");
        }
    }, [activeDayIndex.get()]);

    const handlePrev = useCallback(() => {
        if (!sliderRef.current) return;
        sliderRef.current.swiper.slidePrev();
    }, []);
    
    const handleNext = useCallback(() => {
        if (!sliderRef.current) return;
        sliderRef.current.swiper.slideNext();
    }, []);

    function handleAppointmentSlideChange(slideIndex) {
        activeDayIndex.set(slideIndex);
    }

    function getRatingColor(rating) {
        if(rating < 3) {
            return "#cb202d";
        }
        else if(rating < 4) {
            return "#F5CB39";
        }
        else {
            return "#01a400";
        }
    }

    function parseTimings() {

        timingGroups.set([]);
        var sameTimings = [];

        if(counsellor.attach(Downgraded).get().timings) {

            var cTimings = JSON.parse(counsellor.attach(Downgraded).get().timings);
            
            for(var i=0; i<cTimings.length; i++) {
                if(sameTimings.includes(JSON.stringify(cTimings[i].timing))) {
                    timingGroups.attach(Downgraded).get().filter(group => group.timing === JSON.stringify(cTimings[i].timing)).map(gp => gp.days.push(cTimings[i].day));
                }
                else {
                    sameTimings.push(JSON.stringify(cTimings[i].timing));
                    timingGroups.attach(Downgraded).get().push({days: [cTimings[i].day], timing: JSON.stringify(cTimings[i].timing)});
                }
            }
        }
    }

    function createAppointmentDates() {

        appointmentDates.set([]);

        let daysRequired = 6;

        for (let i = 0; i <= daysRequired; i++) {
            let day_slots_all = [];

            let label = moment().add(i, 'days').format('Do MMM YYYY');
            let label_to_show = moment().add(i, 'days').format('ddd, Do MMM');

            if(i === 0) {
                label = "Today";
                label_to_show = "Today";
            }

            if(i === 1) {
                label = "Tomorrow";
                label_to_show = "Tomorrow";
            }

            let day = moment().add(i, 'days').format('ddd').toUpperCase();

            let date_day = moment().add(i, 'days').format('l');

            let day_timings = counsellor.attach(Downgraded).get().timings && JSON.parse(counsellor.attach(Downgraded).get().timings).filter(timing => timing.day === day)[0];
            
            day_timings && day_timings.timing.forEach((timing) => {

                var c_slots = [];

                let beginTime = moment(timing.begin, 'LT');
                let endTime = moment(timing.end, 'LT');

                var currentTime = moment(beginTime);

                while (currentTime <= endTime) {

                    let dateTimeForDay = date_day +" "+ currentTime.format('LT');
                    
                    if(Date.now() < Date.parse(dateTimeForDay)) {
                        var bookedSlots = consultations.attach(Downgraded).get().filter(consultation => moment(consultation.start_time).format('YYYY-MM-DD HH:mm:ss') === moment(dateTimeForDay).format('YYYY-MM-DD HH:mm:ss'));
                        if(bookedSlots.length === 0) {
                            c_slots.push(currentTime.format('LT'));
                        }
                    }
                    currentTime.add(counsellor.attach(Downgraded).get().slot_time, 'minutes');
                }
                day_slots_all = day_slots_all.concat(c_slots);
                day_slots_all = day_slots_all.filter(function(member, index) { return index !== day_slots_all.length - 1; });
            });
            appointmentDates.attach(Downgraded).get().push({id: i, label: label, label_to_show: label_to_show, slots: day_slots_all});
        }
    }

    function setReviewFilter(rating) {
        if(!reviewFilter.get() && reviews.attach(Downgraded).get().filter(review => review.rating === rating).length > 0) {
            reviewFilter.set(rating);
        }
    }

    function resetReviewFilter() {
        reviewFilter.set(null);
        reviews.set(allReviews.attach(Downgraded).get());
    }

    useEffect(() => {
        if(reviewFilter.get()) {
            reviews.set(reviews.attach(Downgraded).get().filter(review => review.rating === reviewFilter.get()));
        }
        else {
            reviews.set(allReviews.attach(Downgraded).get());
        }
    }, [reviewFilter.get()]);

    useEffect(() => {
        if(router.query.course) {
            course_id.set(router.query.course);
        }
        if(router.query.mode) {
            mode_id.set(router.query.mode);
        }
        if(router.query.specialization) {
            specialization_id.set(router.query.specialization);
        }
    }, [router]);

    function setupModal(key, eKey="", eKeyTwo="") {
        if(key === "book-slot") {
            modalContent.set(<BookSlot course={course_id.get()} mode={mode_id.get()} specialization={specialization_id.get()} closeModal={() => showModal.set(false)} dateLabel={eKey} slot={eKeyTwo} counsellor={counsellor.attach(Downgraded).get()} reviews={allReviews.attach(Downgraded).get()} />);
            showModal.set(true);
        }
        if(key === "auth") {
            modalContent.set(<AuthModal closeModal={() => showModal.set(false)} />);
            showModal.set(true);
        }
        if(key === "feedback") {
            modalSize.set("lg");
            modalContent.set(<Feedback counsellor={eKey} closeModal={() => showModal.set(false)} updateCounsellor={updateCounsellor} />);
            showModal.set(true)
        }
    }

    return (
        <div className={styles.counsellor_profile}>
            <Row>
                <Col lg="12" className='bg-white pt-3'>
                   <div>
                   <Row>
                       <Col md={8}>
                       <Row>
                        <Col md={4}>
                            <div className={`${styles.counsellor_top_div} text-center rounded`} style={{backgroundColor:"#f0f8ff"}}>
                                <div className='mx-auto'>
                                    {counsellor.attach(Downgraded).get() && counsellor.attach(Downgraded).get().image !== "" ?
                                        <div className='mx-auto' style={{width: "100px", height: "100px"}}>
                                            <Image className='rounded-circle shadow-sm' src={counsellor.attach(Downgraded).get().image} width={100} height={100} layout="responsive" />
                                        </div>
                                        :
                                        <div className='mx-auto' style={{width: "100px", height: "100px"}}>
                                            <Image className='rounded-circle shadow-sm' src="/images/profile.png" width={100} height={100} layout="responsive" />
                                        </div>
                                    }
                                </div>
                                {counsellor.attach(Downgraded).get() && counsellor.attach(Downgraded).get().verified ?
                                    <div className='my-2'>
                                        <HiCheckCircle fontSize={20} color="#01A400" />
                                        <span style={{fontSize: "14px", color: "#414146"}}>&nbsp;Verified</span>
                                    </div>
                                    :
                                    null}
                                <div className={`${styles.c_profile_title}`}>
                                        {counsellor.attach(Downgraded).get() &&counsellor.attach(Downgraded).get().name}
                                    </div>
                                    <div className={`${styles.c_profile_designation}`}>
                                       ( {counsellor.attach(Downgraded).get() && counsellor.attach(Downgraded).get().designation})
                                    </div>
                                    <div className={`${styles.c_profile_designation}`}>
                                    <FcGraduationCap fontSize={22} /> {counsellor.attach(Downgraded).get() && counsellor.attach(Downgraded).get().qualification}
                                    </div>
                                    <div className={`${styles.c_profile_experience}`}>
                                        {counsellor.attach(Downgraded).get() && counsellor.attach(Downgraded).get().experience} Years Exp
                                    </div>
                                 
                                
                            </div>
                            
                        </Col>
                        <Col md={8}>
                        <div className='bg-white p-3'>
                                  
                                 <h4>About </h4>
                                    <div className='d-flex align-items-center mt-2 mb-2'>
                                        <AiFillStar fontSize={20} color={getRatingColor(counsellor.get() && counsellor.get().avg_rating)} />
                                        <span style={{color: getRatingColor(counsellor.get() && counsellor.get().avg_rating)}}>&nbsp;{counsellor.get() && counsellor.get().avg_rating && counsellor.get().avg_rating.toFixed(1)}</span>
                                        <span style={{fontSize: "12px", color: "#787887", marginTop: "2px"}}>&nbsp;({allReviews.attach(Downgraded).get().length} votes)</span>
                                    </div>
                                    <div style={{fontSize: "14px"}}>
                                        {counsellor.attach(Downgraded).get() && counsellor.attach(Downgraded).get().about}
                                    </div>
                                    <div className='d-flex' style={{paddingTop: "16px", justifyContent: "end"}}>
                                        <span onClick={() => isAuthenticated() ? setupModal('feedback', counsellor.attach(Downgraded).get()) : setupModal('auth')} style={{fontSize: "13px", color: "#0074d7", cursor: "pointer"}}>Share your experience</span>
                                    </div>
                                </div>
                        </Col>

                    </Row>
                       </Col>
                       <Col md={4}>
                       <div className='border p-3'>
                    <Row>
                    <div className={styles.consultation_div}>
                        <div style={{color: "#787887", fontSize: "14px"}}>
                            PICK A TIME SLOT
                        </div>
                        <div className='border-bottom' style={{marginTop: "16px", paddingBottom: "16px"}}>
                            <div className='d-flex align-items-center justify-content-between'>
                                <div className='d-flex'>
                                    <div style={{borderRadius: "50%", height: "20px", width: "20px", backgroundColor: "#0074d7"}}>
                                        <IoVideocamOutline style={{marginBottom: "6px", marginLeft: "2px", padding: "1px"}} color="#FFFFFF"/>
                                    </div>
                                    <span style={{fontSize: "14px", fontWeight: "bold"}} >&nbsp;&nbsp;VIDEO CONSULTATION</span>
                                </div>
                                <div style={{fontWeight: "bold"}}>
                                    ₹{counsellor.attach(Downgraded).get() && counsellor.attach(Downgraded).get().fees}
                                </div>
                            </div>
                            <div style={{fontSize: "12px", marginLeft: "28px", color: "#787887"}}>
                                Includes Audio, Video and Chat
                            </div>
                        </div>
                        <div className='d-flex align-items-center'>
                            <FaChevronLeft cursor="pointer" onClick={handlePrev} color={leftNavColor.get()} />
                            <Swiper 
                            centeredSlides={true}
                            ref={sliderRef}
                            style={{width: "100%", padding: "0px 10px"}}
                            loop={false}
                            onSlideChange={(slider) => handleAppointmentSlideChange(slider.activeIndex)}
                            slidesPerView={3}
                            spaceBetween={20}>
                                {appointmentDates.attach(Downgraded).get().map(appointmentDate =>
                                <SwiperSlide className='unselectable' key={appointmentDate.id} onClick={() => activeDayIndex.set(appointmentDate.id)}>
                                    <div className={`${styles.appointment_wrapper} ${activeDayIndex.get() === appointmentDate.id ? styles.active : ''}`}>
                                        <div className={activeDayIndex.get() === appointmentDate.id ? 'fw-bold' : ''}>{appointmentDate.label_to_show}</div>
                                        <div className={`${styles.appointment_slots} ${appointmentDate.slots.length === 0 ? '' : styles.active}`}>{appointmentDate.slots.length} Slots Available</div>
                                    </div>
                                </SwiperSlide>
                                )}
                            </Swiper>
                            <FaChevronRight cursor="pointer" onClick={handleNext} color={rightNavColor.get()} />
                        </div>
                        <div className={styles.app_slots_wrapper}>
                            {appointmentDates.attach(Downgraded).get().filter(apptDate => apptDate.id === activeDayIndex.get()).length > 0 && appointmentDates.attach(Downgraded).get().filter(apptDate => apptDate.id === activeDayIndex.get())[0].slots.length === 0 ?
                            <div className='d-flex justify-content-center align-items-center' style={{height: "215px", flexDirection: "column"}}>
                                <Image src="/images/no_slots.svg" height={45} width={55} />
                                <div className='mt-2' style={{color: "#787887", fontSize: "12px"}}>No slots available for this day</div>
                            </div>
                            :
                            <div>
                                <Row className='m-0'>
                                    {appointmentDates.attach(Downgraded).get().filter(apptDate => apptDate.id === activeDayIndex.get()).length > 0 && appointmentDates.attach(Downgraded).get().filter(apptDate => apptDate.id === activeDayIndex.get())[0].slots.map((slot) => 
                                    <Col onClick={() => setupModal('book-slot', appointmentDates.attach(Downgraded).get().filter(apptDate => apptDate.id === activeDayIndex.get())[0].label, slot)} className='unselectable' key={generate()} lg="3">
                                        <div className={`${styles.time_slot} d-flex align-items-center justify-content-center mt-3`}>
                                            <span style={{fontSize: "12px"}}>{slot}</span>
                                        </div>
                                    </Col>
                                    )}
                                </Row>
                            </div>
                            }
                        </div>
                    </div>
                    </Row>
                    </div>
                       </Col>
                   </Row>
                 
                   </div>
                   <div>
                   <Row className='mt-3'>
                        <Col>
                            <ul className={styles.pills}>
                                <li onClick={() => tabValue.set(0)} className={`${styles.tab} ${tabValue.get() === 0 ? styles.active : ''}`}>Info</li>
                                <li onClick={() => tabValue.set(1)} className={`${styles.tab} ${tabValue.get() === 1 ? styles.active : ''}`}>Reviews ({allReviews.attach(Downgraded).get().length})</li>
                                {/* <li onClick={() => tabValue.set(2)} className={`${styles.tab} ${tabValue.get() === 2 ? styles.active : ''}`}>Consult Q&amp;A</li>
                                <li onClick={() => tabValue.set(3)} className={`${styles.tab} ${tabValue.get() === 3 ? styles.active : ''}`}>Feed</li> */}
                            </ul>
                            <div className={styles.pill_panel}>
                                {tabValue.get() === 0 ?
                                <div>
                                    <Row>
                                    <Card className='mb-3 py-3 shadow-sm'>
                                    <div className={styles.counsellor_locality}>
                                                <MdLocationPin />&nbsp;{counsellor.attach(Downgraded).get() && counsellor.attach(Downgraded).get().locality}
                                            </div>
                                    </Card>
                                    <Card className='mb-3 py-3 shadow-sm'>
                                    {timingGroups.attach(Downgraded).get().map((group, gindex) => {
                                                if(group.timing && JSON.parse(group.timing).length === 0) {
                                                    return (
                                                        <>{group.days.timing}</>
                                                    );
                                                }
                                                else {
                                                    return ( 
                                                        <div key={generate()} className={gindex === 0 ? "" : "mt-3"}>
                                                        {
                                                        group.days.map((day, index) => {
                                                            return (
                                                                <span className='mb-3 d-inline-block text-dark  me-2 px-2 py-1 rounded' key={day} style={{fontWeight: "bold", fontSize: "12px" , backgroundColor: "#f0f8ff"}}>{day}{index < group.days.length - 1 ? "" : ""}</span>
                                                            );
                                                        })
                                                        }
                                                            <div className={styles.timeslot}>
                                                                {
                                                                group.timing && JSON.parse(group.timing).map(time =>
                                                                <div className='mb-2' key={JSON.stringify(time)}>
                                                                    {time.begin} - {time.end}<br/>
                                                                </div>
                                                                )}
                                                            </div>
                                                        </div>
                                                    )
                                                }
                                            })}
                                    </Card>
                                      
                                        
                                        
                                        <Card className='mb-3 py-3 shadow-sm'>
                                            <div className={styles.timeslot}>
                                                <h6 className='mb-2' style={{fontWeight: "bold", fontSize: "14px"}}>Consultation Fee</h6>
                                                <span className='h3' style={{color:"#0074d7"}}>
                                                ₹{counsellor.attach(Downgraded).get() && counsellor.attach(Downgraded).get().fees}
                                                </span>
                                            </div>
                                            <h6 className='mt-3 mb-3' style={{fontWeight: "bold", fontSize: "14px"}}>Specializations</h6>
                                            <ul className="list-unstyled d-flex flex-wrap">
                                                {counsellor.attach(Downgraded).get() && JSON.parse(counsellor.attach(Downgraded).get().specializations).map(specialization =>
                                                <li className={`${styles.specializations_list}`}  key={specialization.id}>
                                                    <div className={styles.timeslot}>{specialization.name} ({specialization.mode})</div>
                                                </li>
                                                )}
                                            </ul>
                                        </Card>
                                    </Row>
                                </div>
                            :
                            tabValue.get() === 1 ?
                            <div>
                                <div className='fw-bold' style={{fontSize: "18px"}}>
                                    Student Reviews for {counsellor.attach(Downgraded).get() && counsellor.attach(Downgraded).get().name}
                                </div>
                                <div style={{color: "#787887", fontSize: "14px"}}>
                                    These are student's opinions and do not necessarily reflect the counsellor's capabilities.
                                </div>
                                <div className={styles.review_filter_section}>
                                    <div className={styles.review_filter_header}>
                                        <StarRatings
                                            rating={counsellor.attach(Downgraded).get() && counsellor.attach(Downgraded).get().avg_rating ? counsellor.attach(Downgraded).get().avg_rating : 0}
                                            starDimension="24px"
                                            starSpacing="2px"
                                            isSelectable={false}
                                            starRatedColor={getRatingColor(counsellor.attach(Downgraded).get().avg_rating)} 
                                        />
                                        <span className={styles.review_filter_header_title}>
                                            {counsellor.attach(Downgraded).get() && counsellor.attach(Downgraded).get().avg_rating !== null && counsellor.attach(Downgraded).get().avg_rating.toFixed(1)} out of 5
                                        </span>
                                        {reviewFilter.get() ? 
                                            <span onClick={() => resetReviewFilter()} className={styles.review_filter_reset}>
                                                Reset
                                            </span>
                                            :
                                            null
                                        }
                                    </div>
                                    <div className={styles.review_filter_subtitle}>
                                        {allReviews.attach(Downgraded).get().length} global ratings
                                    </div>
                                    <div className={styles.review_filter_stats_container}>

                                        <div onClick={() => setReviewFilter(5)} className={styles.review_filter_stats_container_item}>
                                            <span className={styles.review_filter_stats_container_item_props}>5 star</span>
                                            <ProgressBar className={styles.review_filter_stats_container_item_bar} animated={true} variant="info" striped={true} now={reviews.attach(Downgraded).get().filter(review => review.rating === 5).length / reviews.attach(Downgraded).get().length * 100} />
                                            <span style={{textAlign: "center"}} className={styles.review_filter_stats_container_item_props}>{(reviews.attach(Downgraded).get().filter(review => review.rating === 5).length / reviews.attach(Downgraded).get().length * 100).toFixed(2)}%</span>
                                        </div>

                                        <div onClick={() => setReviewFilter(4)} className={styles.review_filter_stats_container_item}>
                                            <span className={styles.review_filter_stats_container_item_props}>4 star</span>
                                            <ProgressBar className={styles.review_filter_stats_container_item_bar} animated={true} variant="info" striped={true} now={reviews.attach(Downgraded).get().filter(review => review.rating === 4).length / reviews.attach(Downgraded).get().length * 100} />
                                            <span style={{textAlign: "center"}} className={styles.review_filter_stats_container_item_props}>{(reviews.attach(Downgraded).get().filter(review => review.rating === 4).length / reviews.attach(Downgraded).get().length * 100).toFixed(2)}%</span>
                                        </div>

                                        <div onClick={() => setReviewFilter(3)} className={styles.review_filter_stats_container_item}>
                                            <span className={styles.review_filter_stats_container_item_props}>3 star</span>
                                            <ProgressBar className={styles.review_filter_stats_container_item_bar} animated={true} variant="info" striped={true} now={reviews.attach(Downgraded).get().filter(review => review.rating === 3).length / reviews.attach(Downgraded).get().length * 100} />
                                            <span style={{textAlign: "center"}} className={styles.review_filter_stats_container_item_props}>{(reviews.attach(Downgraded).get().filter(review => review.rating === 3).length / reviews.attach(Downgraded).get().length * 100).toFixed(2)}%</span>
                                        </div>

                                        <div onClick={() => setReviewFilter(2)} className={styles.review_filter_stats_container_item}>
                                            <span className={styles.review_filter_stats_container_item_props}>2 star</span>
                                            <ProgressBar className={styles.review_filter_stats_container_item_bar} animated={true} variant="info" striped={true} now={reviews.attach(Downgraded).get().filter(review => review.rating === 2).length / reviews.attach(Downgraded).get().length * 100} />
                                            <span style={{textAlign: "center"}} className={styles.review_filter_stats_container_item_props}>{(reviews.attach(Downgraded).get().filter(review => review.rating === 2).length / reviews.attach(Downgraded).get().length * 100).toFixed(2)}%</span>
                                        </div>

                                        <div onClick={() => setReviewFilter(1)} className={styles.review_filter_stats_container_item}>
                                            <span className={styles.review_filter_stats_container_item_props}>1 star</span>
                                            <ProgressBar className={styles.review_filter_stats_container_item_bar} animated={true} variant="info" striped={true} now={reviews.attach(Downgraded).get().filter(review => review.rating === 1).length / reviews.attach(Downgraded).get().length * 100} />
                                            <span style={{textAlign: "center"}} className={styles.review_filter_stats_container_item_props}>{(reviews.attach(Downgraded).get().filter(review => review.rating === 1).length / reviews.attach(Downgraded).get().length * 100).toFixed(2)}%</span>
                                        </div>
                                    </div>
                                </div>
                                {reviews.attach(Downgraded).get().slice(0, maxReviews.get()).map(review =>
                                <div className='border-bottom' style={{paddingBottom: "32px", paddingTop: "24px"}} key={review.id}>
                                    <div className={styles.review_header}>
                                        <div className={styles.reviewer_profile}>
                                            <div className={styles.reviewer_image} style={{backgroundColor: bgColors[Math.floor(Math.random() * (2 + 1))]}}>
                                                {review.name.substring(0, 1)}
                                            </div>
                                            <div className={styles.reviewer_name}>
                                                {review.name} {review.is_verified ?<span style={{marginLeft: "3px", fontSize: "14px", color: "#000"}}><MdVerified fontSize={18} color='#0FA148' /><b> Verified</b></span> : ""}
                                            </div>
                                        </div>
                                        <div className={styles.review_time}>
                                            {moment(review.timestamp, 'YYYYMMDDhmmssa').fromNow()}
                                        </div>
                                    </div>
                                    <div className={styles.review_body}>
                                        <div className='fw-bold' style={{fontSize: "16px", paddingBottom: "8px"}}>
                                            {review.title}
                                        </div>
                                        <div style={{color: getRatingColor(review.rating)}} className={styles.rating_container}>
                                            <span style={{position: "absolute", bottom: "1.7px", left: "7px"}}>{review.rating}</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <StarRatings
                                                rating={review.rating}
                                                starDimension="16px"
                                                starSpacing="2px"
                                                isSelectable={false}
                                                starRatedColor={getRatingColor(review.rating)}
                                                />&nbsp;
                                        </div>
                                        <div className={`${styles.review} mt-3`}>
                                            {review.review}
                                        </div>
                                        {review.reply && review.reply !== "" ?
                                            <div className={`${styles.reply} mt-3`}>
                                                <div className='d-flex align-items-center'>
                                                    <ImForward color='#B4B4BE' fontSize={18} />&nbsp;&nbsp;
                                                    <span>{counsellor.attach(Downgraded).get().name} replied</span>
                                                </div>
                                                <div className={`${styles.reply_text}`}>
                                                    {review.reply}
                                                </div>
                                            </div>
                                        :
                                        null
                                        }
                                    </div>
                                </div>
                                )}
                                {maxReviews.get() >= reviews.attach(Downgraded).get().length ?
                                null
                                :
                                <div className='d-flex p-4 align-items-center justify-content-center'>
                                    <Button onClick={() => maxReviews.set(maxReviews.get() + reviewsPerLoad.get())} className='cv-primary-btn' style={{padding: "5px 30px"}}>More</Button>
                                </div>
                                }
                            </div>
                            :
                            tabValue.get() === 2 ?
                                <div style={{height: "150px"}} className='d-flex align-items-center justify-content-center'>
                                    <span style={{fontSize: "18px", fontWeight: "bold", color: "#414146"}}>Coming Soon</span>
                                </div>
                                :
                                <div style={{height: "150px"}} className='d-flex align-items-center justify-content-center'>
                                    <span style={{fontSize: "18px", fontWeight: "bold", color: "#414146"}}>Coming Soon</span>
                                </div>
                            }
                            </div>
                        </Col>
                    </Row>
                   </div>
                   
                </Col>
                {/* <Col className='unselectable' lg="4">
                    
                </Col> */}
            </Row>
            <CustomModal content={modalContent} showModal={showModal} modalSize={modalSize.get()} staticBackdrop={false} />
        </div>
    )
}

export default CounsellorDetails