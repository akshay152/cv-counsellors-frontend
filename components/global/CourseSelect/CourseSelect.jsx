import { Downgraded, useState } from '@hookstate/core';
import axios from 'axios';
import Image from 'next/image';
import { useEffect } from 'react';
import { Col, Row } from 'react-bootstrap';
import CustomModal from '../customs/CustomModal';
import styles from './CourseSelect.module.css';
import ConsultationDetailsModal from './modals/ConsultationDetailsModal';

const CourseSelect = () => {

    const currentCategory = useState(0);
    const courseCategories = useState([]);

    const courses = useState([]);

    const modalContent = useState(null);
    const showModal = useState(false);

    function setupModal(key, eKey="") {
        if(key === 'consultation-details') {
            modalContent.set(<ConsultationDetailsModal course={eKey} closeModal={() => showModal.set(false)} />);
            showModal.set(true);
        }
    }

    useEffect(() => {
        axios.get(process.env.NEXT_PUBLIC_BACKEND_URL+"program-category")
        .then((response) => {
            courseCategories.set(response.data);
            if(response.data.length > 0) {
                currentCategory.set(response.data[0].id);
            }
        });

        axios.get(process.env.NEXT_PUBLIC_BACKEND_URL+"course-consultation")
        .then((response) => {
            courses.set(response.data);
        })
    }, []);

    return (
        <Row className='mt-5'>
            <Col md="3">
                <div className={`${styles.courses_pills} flex-column`}>
                    {courseCategories.attach(Downgraded).get().map(category =>
                        <div onClick={() => currentCategory.set(category.id)} key={category.id} className={`${styles.pill_option} ${currentCategory.get() === category.id ? styles.active : ''}`} >{category.short_name}</div>
                    )}
                </div>
            </Col>
            <Col md="9">
                <div className={styles.tab_content}>
                    <div>
                        <div className={styles.tab_wrapper}>
                            {courses.attach(Downgraded).get().filter(course => course.category_id === currentCategory.get()).map(course =>
                                <div onClick={() => setupModal('consultation-details', {id: course.id, name: course.short_name})} key={course.id} className={styles.icon_card_wrap}>
                                    <div className={styles.icon_card}>
                                        <Image src={course.icon} height={22} width={20} />
                                        <p className={styles.icon_card_head}>{course.short_name}</p>
                                        <p className={styles.icon_card_sm_text}>{course.counsellors} Counsellors Available</p>
                                    </div>
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            </Col>
            <CustomModal content={modalContent} showModal={showModal} modalSize="md" staticBackdrop={false} />
        </Row>
    );
}

export default CourseSelect;