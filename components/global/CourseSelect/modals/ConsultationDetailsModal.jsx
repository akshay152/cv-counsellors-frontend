import { Downgraded, useState } from '@hookstate/core';
import { useEffect } from 'react';
import { Badge, Button, Col, Form, Modal, Row } from 'react-bootstrap';
import axios from 'axios';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useRouter } from 'next/router';
import isAuthenticated from '../../../../utils/auth';
import CustomModal from '../../customs/CustomModal';
import AuthModal from '../../header/modals/AuthModal';

const ConsultationDetailsModal = (props) => {

    const router = useRouter();

    const courseID = props.course.id;
    const courseName = props.course.name;

    const modes = useState([]);
    const specializations = useState([]);

    const modeDescription = useState("");

    const modalContent = useState(null);
    const showModal = useState(false);

    const validationSchema = Yup.object({
        mode: Yup.string().required('Please select a mode'),
        specialization: Yup.string().required('Please select a specialization')
    });

    const formik = useFormik({
        initialValues: {
            mode: '',
            specialization: ''
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {

            const course = modes.attach(Downgraded).get().filter(mode => mode.id === parseInt(values.mode))[0].course_id;
            values['course'] = course;

            if(isAuthenticated()) {
                router.push('/consultation/initiate?mode='+values.mode+'&course='+values.course+'&specialization='+values.specialization);
            }
            else {
                setupModal('/consultation/initiate?mode='+values.mode+'&course='+values.course+'&specialization='+values.specialization);
            }
        }
    });

    useEffect(() => {
        axios.get(process.env.NEXT_PUBLIC_BACKEND_URL+"mode/"+courseName)
        .then((response) => {
            modes.set(response.data);
        })
    }, []);

    useEffect(() => {
        if(formik.values.mode !== "") {
            const course_id = modes.attach(Downgraded).get().filter(mode => mode.id === parseInt(formik.values.mode))[0].course_id;

            axios.get(process.env.NEXT_PUBLIC_BACKEND_URL+"specialization?mode_id="+formik.values.mode+"&course_id="+course_id)
            .then((response) => {
                specializations.set(response.data);
                formik.setFieldValue('specialization', '');
            });

            axios.get(process.env.NEXT_PUBLIC_BACKEND_URL+"mode/description/"+formik.values.mode)
            .then((response) => {
                modeDescription.set(response.data[0].description);
            });
        }
        else {
            modeDescription.set("");
        }

    },[formik.values.mode]);

    function setupModal(redirectURL) {
        modalContent.set(<AuthModal redirectURL={redirectURL} closeModal={() => showModal.set(false)} />);
        showModal.set(true);
    }

    return (
        <Modal.Body style={{padding: "20px 30px"}}>
            <Form onSubmit={formik.handleSubmit}>
                <Row>
                    <Col>
                        <div>
                            <span className="close_modal_button" style={{marginRight: "7px"}} onClick={() => props.closeModal()} />
                        </div>
                    </Col>
                </Row>
                <Row className='mt-3'>
                    <Col>
                        <Form.Label>Course*</Form.Label>
                        <Form.Select disabled={true} className="shadow-none cv-input-field">
                            <option defaultValue={true} value={courseID}>{courseName}</option>
                        </Form.Select>
                    </Col>
                </Row>
                <Row className='mt-3'>
                    <Col>
                        <Form.Label>Education Mode*</Form.Label>
                        <Form.Select name="mode"
                        className="shadow-none cv-input-field"
                        value={formik.values.mode}
                        onChange={formik.handleChange}
                        isInvalid={!!formik.errors.mode}>
                            <option defaultValue={true} value="">Select an option</option>
                            {modes.attach(Downgraded).get().map(mode =>
                                <option key={mode.id} value={mode.id}>{mode.name}</option>
                            )}
                        </Form.Select>
                        <Form.Control.Feedback type="invalid">
                            {formik.errors.mode}
                        </Form.Control.Feedback>
                        <Form.Text>{modeDescription && modeDescription.get() !== "" ? <><Badge bg="success">Note :</Badge>&nbsp;<b>{modeDescription.get()}</b></> : null }</Form.Text>
                    </Col>
                </Row>
                <Row className='mt-3'>
                    <Col>
                        <Form.Label>Specialization*</Form.Label>
                        <Form.Select name="specialization"
                        className="shadow-none cv-input-field"
                        value={formik.values.specialization}
                        onChange={formik.handleChange}
                        isInvalid={!!formik.errors.specialization}>
                            <option defaultValue={true} value="">Select an option</option>
                            {specializations.attach(Downgraded).get().map(specialization =>
                                <option key={specialization.id} value={specialization.id}>{specialization.name}</option>
                            )}
                        </Form.Select>
                        <Form.Control.Feedback type="invalid">
                            {formik.errors.specialization}
                        </Form.Control.Feedback>
                    </Col>
                </Row>
                <Row className='mt-4'>
                    <Col>
                        <Button type="submit" className='cv-primary-btn fw-bold w-100'>Instant Video Consultation</Button>
                    </Col>
                </Row>
            </Form>
            <CustomModal content={modalContent} showModal={showModal} modalSize="md" staticBackdrop={false} />
        </Modal.Body>
    );
}

export default ConsultationDetailsModal;