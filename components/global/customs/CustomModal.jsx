import { Downgraded } from '@hookstate/core';
import React from 'react';
import { Modal } from 'react-bootstrap';
import styles from "../../../components/user/consultations/modals/CounsellorInfo.module.css";

const CustomModal = (props) => {
    return (
        <Modal dialogClassName={props.dialogClass ? styles.counselllor_info_dialog : ''} {...props.staticBackdrop ? 'backdrop="static"' : null} show={props.showModal.get()} onHide={() => props.showModal.set(false)} size={props.modalSize} centered>
            {props.content.attach(Downgraded).get()}
        </Modal>
    )
}

export default CustomModal;  