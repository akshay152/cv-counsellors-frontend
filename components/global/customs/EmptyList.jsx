import Image from "next/image";

const EmptyList = (props) => {
    return (
        <div className="d-flex" style={{height: "400px", justifyContent: "center", alignItems: "center", flexDirection: "column"}}>
            <Image src="/images/empty.png" width={400} height={300} />
            <h4 style={{marginBottom: "20px", color: "#B1B1B1"}}>{props.title}</h4>
            <h6 style={{color: "#B1B1B1"}}>{props.subtitle}</h6>
        </div>
    )
}

export default EmptyList;