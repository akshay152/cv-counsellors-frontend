import { useState } from "@hookstate/core";
import { Col, Modal, Row } from "react-bootstrap";
import styles from './HeaderModal.module.css';
import LoginContent from "./LoginContent";
import RegisterContent from "./RegisterContent";

const AuthModal = (props) => {

    const activeTab = useState(0);

    return (
        <Modal.Body style={{padding: "20px 30px"}}>
            <Row>
                <Col>
                    <div>
                        <span className="close_modal_button" style={{marginRight: "7px"}} onClick={() => props.closeModal()} />
                    </div>
                </Col>
            </Row>
            <div className="d-flex" style={{height: "30px"}}>
                <div className={`${styles.header_pill} ${activeTab.get() === 0 ? styles.active : ''}`} onClick={() => activeTab.set(0)}>
                    <span className={`${styles.header_pill_title} ${activeTab.get() === 0 ? styles.active : ''}`}>Already a user?</span>
                </div>
                <div className={`${styles.header_pill} ${activeTab.get() === 1 ? styles.active : ''}`} style={{marginLeft: "10px"}} onClick={() => activeTab.set(1)}>
                    <span className={`${styles.header_pill_title} ${activeTab.get() === 1 ? styles.active : ''}`}>New user</span>
                </div>
            </div>
            <div style={{marginTop: "10px"}}>
                {activeTab.get() === 0 ? <LoginContent redirectURL={props.redirectURL ? props.redirectURL : ""} closeModal={props.closeModal} /> : <RegisterContent redirectURL={props.redirectURL ? props.redirectURL : ""} closeModal={props.closeModal} />}
            </div>
        </Modal.Body>
    );
}

export default AuthModal;