import { Button, Form, FormControl, InputGroup, Spinner } from "react-bootstrap";
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { FaEye, FaEyeSlash } from 'react-icons/fa';
import { useState } from "@hookstate/core";
import axios from "axios";
import { setUserAuth } from "../../../../utils/common";

const LoginContent = (props) => {

    const showPass = useState(false);
    const isLogging = useState(false);

    const handlePass = () => {
        showPass.set(!showPass.get())
    }

    const validationSchema = Yup.object({
        mobile: Yup.number('Invalid number').required('Mobile number is required'),
        password: Yup.string().required('Password is required')
    });

    const formik = useFormik({
        initialValues: {
            mobile: '',
            password: ''
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {

            isLogging.set(true);

            let formData = new FormData();
            formData.append('mobile', values.mobile);
            formData.append('password', values.password);

            axios.post(process.env.NEXT_PUBLIC_BACKEND_URL+"customer/login", formData)
                .then((response) => {

                    setUserAuth(response.data.user, response.data.accessToken, response.data.refreshAccessToken);
                    isLogging.set(false);

                    props.closeModal();
                    if(props.redirectURL === "") {
                        window.location.reload();
                    }
                    else {
                        window.location.href = props.redirectURL;
                    }
                })
                .catch((error) => {
                    console.log(error);
                    iziToast.error({
                        title: "Login failed",
                        message: error.response.data,
                        timeout: "2000",
                        position: "topRight"
                    });

                    isLogging.set(false);
                });
        }
    });

    return (
        <Form onSubmit={formik.handleSubmit}>
            <Form.Label>Mobile Number</Form.Label>
            <FormControl name="mobile"
            value={formik.values.mobile}
            onChange={formik.handleChange}
            isInvalid={!!formik.errors.mobile}
            type="number"
            className="shadow-none cv-input-field" placeholder="Mobile Number" />
            <Form.Control.Feedback type="invalid">
                {formik.errors.mobile}
            </Form.Control.Feedback>
            <div style={{marginTop: "10px"}} />
            <Form.Label>Password</Form.Label>
            <InputGroup>
                <FormControl name="password"
                value={formik.values.password}
                onChange={formik.handleChange}
                isInvalid={!!formik.errors.password}
                className="shadow-none cv-input-field" placeholder="Password" type={showPass.get() ? 'text' : 'password'} />
                <InputGroup.Text onClick={handlePass} style={{cursor: "pointer"}} className="password-group">
                    {showPass.get() ?
                    <FaEyeSlash color="#7f7f7f" />
                    :
                    <FaEye color="#7f7f7f" />
                    }
                </InputGroup.Text>
            </InputGroup>
            <Form.Control.Feedback type="invalid">
                {formik.errors.password}
            </Form.Control.Feedback>
            <div style={{marginTop: "10px"}} />
            <div className="d-flex align-items-center" style={{justifyContent: "space-between"}}>
                <Form.Check className="shadow-none" type="checkbox" label="Remember me" />
                <div style={{color: "#14bef0", fontSize: "12px", marginTop: "3px", cursor: "pointer"}}>
                    Forgot Password
                </div>
            </div>
            <div style={{marginTop: "26px"}} />
            {isLogging.get() ?
            <Button className="cv-primary-btn" style={{width: "100%"}}><Spinner size="sm" animation="border" /></Button>
            :
            <Button type="submit" className="cv-primary-btn" style={{width: "100%"}}>Login</Button>
            }
        </Form>
    )
}

export default LoginContent;