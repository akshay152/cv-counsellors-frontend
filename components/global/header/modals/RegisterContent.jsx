import { Button, Form, FormControl, InputGroup, Row, Col, Spinner } from "react-bootstrap";
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { FaEye, FaEyeSlash } from 'react-icons/fa';
import { Downgraded, useState } from "@hookstate/core";
import styles from './HeaderModal.module.css';
import Link from 'next/link';
import SearchBox from "../../../home/SearchBox/SearchBox";
import { useEffect } from "react";
import axios from "axios";
import getCountry from "../../../../utils/getCountry";
import { firebase, auth } from '../../../../utils/firebase';
import { setUserAuth } from "../../../../utils/common";
import Image from 'next/image';

const RegisterContent = (props) => {

    const showPass = useState(false);
    const selectedCountryCode = useState('');
    const isPhoneLoading = useState(true);
    const otpSent = useState(false);
    const sendingOTP = useState(false);
    const validatingAndSaving = useState(false);
    const verificationID = useState('');
    const countryID = useState('');
    const timeZone = useState('');

    const countries = useState([]);

    const handlePass = () => {
        showPass.set(!showPass.get())
    }

    const validationSchema = Yup.object({
        full_name: Yup.string().required('Name is required'),
        mobile: Yup.number('Invalid number').required('Mobile number is required'),
        password: Yup.string().required('Password is required')
    });

    const formik = useFormik({
        initialValues: {
            full_name: '',
            mobile: '',
            password: '',
            otp: ''
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            if(otpSent.get()) {

                validatingAndSaving.set(true);

                validateOTP().then(() => {

                    values['country_id'] = countryID.get();
                    values['country_timezone'] = timeZone.get();

                    let formData = new FormData();
                    formData.append('name', values.full_name);
                    formData.append('mobile', values.mobile);
                    formData.append('password', values.password);
                    formData.append('timezone', values.country_timezone);
                    formData.append('country_id', values.country_id);

                    axios.post(process.env.NEXT_PUBLIC_BACKEND_URL+"customer/create", formData)
                    .then(() => {

                        formData = new FormData();
                        formData.append('mobile', values.mobile);
                        formData.append('password', values.password);

                        axios.post(process.env.NEXT_PUBLIC_BACKEND_URL+"customer/login", formData)
                        .then((response) => {

                            setUserAuth(response.data.user, response.data.accessToken, response.data.refreshAccessToken);
                            validatingAndSaving.set(false);

                            props.closeModal();
                            if(props.redirectURL === "") {
                                window.location.reload();
                            }
                            else {
                                window.location.href = props.redirectURL;
                            }
                        })
                        .catch((error) => {
                            validatingAndSaving.set(false);

                            if(error.response.status === 403) {
                                iziToast.error({
                                    title: "Login failed",
                                    message: error.response.data,
                                    timeout: "2000",
                                    position: "topRight"
                                });
                            }
                        });
                    })
                    .catch((error) => {
                        validatingAndSaving.set(false);

                        if(error.response.status === 422) {
                            otpSent.set(false);
                            formik.setErrors({mobile: error.response.data});
                        }
                        else {
                            iziToast.error({
                                title: "Oops",
                                message: "Some error occured. Please try again",
                                timeout: "2000",
                                position: "topRight"
                            });
                        }
                    });
                })
                .catch((error) => {
                    validatingAndSaving.set(false);
                    formik.setErrors({otp: error.message});
                })
            }
            else {
                sendingOTP.set(true);

                let verify = new firebase.auth.RecaptchaVerifier('recaptcha-container');
                let phonenumber = "+"+selectedCountryCode.attach(Downgraded).get().value+formik.values.mobile;

                auth.signInWithPhoneNumber(phonenumber, verify)
                .then((result) => {
                    verificationID.set(result);

                    verify.clear();
                    sendingOTP.set(false);
                    otpSent.set(true);
                })
                .catch(() => {
                    sendingOTP.set(false);
                    if(verify) {
                        verify.clear();
                    }

                    formik.setErrors({mobile: 'Invalid number'});
                })
            }
        }
    });

    const validateOTP = async () => {
        if(formik.values.otp.length !== 6) {
            formik.setErrors({otp: "OTP must be 6 digits in length"});
            return false;
        }
        return await verificationID.attach(Downgraded).get().confirm(formik.values.otp);
    }

    async function setCodeField() {
        var _country = await getCountry();

        axios.get(process.env.NEXT_PUBLIC_BACKEND_URL+"country")
        .then((response) => {
            countries.set(response.data);
            isPhoneLoading.set(false);

            var defCt = response.data.filter(country => country.modified === _country.country_code)[0];
            var _timezone = JSON.parse(defCt.country_timezones).filter(timezone => timezone.zoneName === _country.timezone)[0];
            var timeZoneStr = `(${_timezone.gmtOffsetName}) ${_timezone.zoneName}`;

            selectedCountryCode.set(defCt);
            timeZone.set(timeZoneStr);
            countryID.set(defCt.country_id);
        });
    }

    useEffect(() => {
        setCodeField();
    }, []);

    return (
        <Form onSubmit={formik.handleSubmit}>
            {otpSent.get() ?
            <>
                <div className="d-flex align-items-center" style={{justifyContent: "space-between", paddingBottom: "10px", borderBottom: "solid 1px rgba(216,216,216,0.5)"}}>
                    <span className={styles.join_tag}>Verify Mobile</span>
                </div>
                <div style={{marginTop: "10px"}} />
                <p style={{color: "rgb(47, 47, 47)", fontSize: "14px", margin: "0"}}>We have sent you an OTP on</p>
                <div style={{color: "#7f7f7f", fontSize: "20px", padding: "10px 15px 0 0"}}>
                    {"+"+selectedCountryCode.attach(Downgraded).get().value+formik.values.mobile}
                </div>
                <div style={{marginTop: "16px"}} />
                <Form.Label>OTP</Form.Label>
                <FormControl name="otp"
                value={formik.values.otp}
                onChange={formik.handleChange}
                isInvalid={!!formik.errors.otp}
                type="tel"
                className="shadow-none cv-input-field" placeholder="Please enter the 6 digit OTP here to verify" />
                <Form.Control.Feedback type="invalid">
                    {formik.errors.otp}
                </Form.Control.Feedback>
                <div style={{marginTop: "10px"}} />
                <div className="d-flex align-items-center" style={{justifyContent: "space-between"}}>
                    <span style={{fontSize: "12px"}}>Still not received OTP? <span style={{color: "#14bef0", marginTop: "3px"}}>Get via call</span></span>
                    <div style={{color: "#14bef0", fontSize: "12px", marginTop: "3px", cursor: "pointer"}}>
                        Resend OTP
                    </div>
                </div>
                <div style={{marginTop: "26px"}} />
                {validatingAndSaving.get() ?
                <Button className="cv-primary-btn" style={{width: "100%"}}><Spinner size="sm" animation="border" /></Button>
                :
                <Button type="submit" className="cv-primary-btn" style={{width: "100%"}}>Register</Button>
                }
            </>
            :
            <>
                <Form.Label>Full Name</Form.Label>
                <FormControl name="full_name"
                value={formik.values.full_name}
                onChange={formik.handleChange}
                isInvalid={!!formik.errors.full_name}
                type="text"
                className="shadow-none cv-input-field" placeholder="Full Name" />
                <Form.Control.Feedback type="invalid">
                    {formik.errors.full_name}
                </Form.Control.Feedback>
                <div style={{marginTop: "10px"}} />
                <Row>
                    <Form.Label>Mobile Number</Form.Label>
                    <Col lg="4">
                        <SearchBox onChange={(value) => selectedCountryCode.set(value)} default={selectedCountryCode.attach(Downgraded).get()} placeholder="Code" isLoading={isPhoneLoading.get()} options={countries.attach(Downgraded).get()} />
                    </Col>
                    <Col lg="8">
                        <FormControl name="mobile"
                        value={formik.values.mobile}
                        onChange={formik.handleChange}
                        isInvalid={!!formik.errors.mobile}
                        type="number"
                        className="shadow-none cv-input-field" placeholder="Mobile Number" />
                        <Form.Control.Feedback type="invalid">
                            {formik.errors.mobile}
                        </Form.Control.Feedback>
                    </Col>
                </Row>
                <div style={{marginTop: "10px"}} />
                <Form.Label>Create Password</Form.Label>
                <InputGroup>
                    <FormControl name="password"
                    value={formik.values.password}
                    onChange={formik.handleChange}
                    isInvalid={!!formik.errors.password}
                    className="shadow-none cv-input-field" placeholder="Password" type={showPass.get() ? 'text' : 'password'} />
                    <InputGroup.Text onClick={handlePass} style={{cursor: "pointer"}} className="password-group">
                        {showPass.get() ?
                        <FaEyeSlash color="#7f7f7f" />
                        :
                        <FaEye color="#7f7f7f" />
                        }
                    </InputGroup.Text>
                </InputGroup>
                <Form.Control.Feedback type="invalid">
                    {formik.errors.password}
                </Form.Control.Feedback>
                <div style={{marginTop: "10px"}} />
                <div className="d-flex align-items-center justify-content-center mt-3" style={{borderRadius: "10px", backgroundImage: "linear-gradient(to right, white , #EEF4FF)"}}>
                    <Image src="/images/stamp.png" height={30} width={45} />
                    <span className="fw-bold" style={{fontSize: "14px", color: "#6A7890"}}>Only certified CV Counselor experts will assist you</span>
                </div>
                <div style={{fontSize: "12px", paddingTop: "8px"}}>
                    <p>By signing up, you agree to our <Link href="/"><a style={{color: "#14bef0", textDecoration: "none"}}>Privacy policy,</a></Link> <Link href="/"><a style={{color: "#14bef0", textDecoration: "none"}}>Terms of Use</a></Link> &amp; <Link href="/"><a style={{color: "#14bef0", textDecoration: "none"}}>Disclaimers</a></Link></p>
                </div>
                <div style={{marginTop: "10px"}} id="recaptcha-container"></div>
                <div style={{marginTop: "26px"}} />
                {sendingOTP.get() ?
                <Button className="cv-primary-btn" style={{width: "100%"}}><Spinner size="sm" animation="border" /></Button>
                :
                <Button type="submit" className="cv-primary-btn" style={{width: "100%"}}>Send OTP</Button>
                }
            </>
            }
        </Form>
    )
}

export default RegisterContent;