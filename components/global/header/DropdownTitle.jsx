import React from 'react';
import { BsChevronDown } from 'react-icons/bs';

const DropdownTitle = (props) => {
    return (
        <React.Fragment>
            {props.title} <BsChevronDown color='#414146' fontSize={11} style={{marginTop: "-2px"}} />
        </React.Fragment>
    );
}

export default DropdownTitle;