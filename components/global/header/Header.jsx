import styles from './Header.module.css';
import Image from 'next/image';
import { Badge, Container, Nav, Navbar, NavDropdown , Dropdown } from 'react-bootstrap';
import NavItem from './NavItem';
import DropdownTitle from './DropdownTitle';
import DropdownItem from './DropdownItem';
import Link from 'next/link';
import { useState } from '@hookstate/core';
import CustomModal from '../customs/CustomModal';
import AuthModal from './modals/AuthModal';
import { getUser, removeUserAuth } from '../../../utils/common';
import { useEffect } from 'react';
import isAuthenticated from '../../../utils/auth';
import store from '../../../utils/store';
import { AiOutlineMenu } from "react-icons/ai";

const Header = () => {

    const modalContent = useState(null);
    const showModal = useState(false);

    function setupModal() {
        modalContent.set(<AuthModal closeModal={() => showModal.set(false)} />);
        showModal.set(true);
    }

    const isAuth = useState(null);
    const globalStore = useState(store);

    useEffect(() => {
        if (isAuthenticated()) {
            isAuth.set(true);
        }
    }, [isAuthenticated]);

    return (
     <>
            <Navbar className='position-fixed w-100 top-0' id="main-header" variant='light' expand='lg' style={{ borderBottom: "3px solid #f0f0f5", backgroundColor: "white",zIndex:"1111" }}>
            <Container fluid>
                <Navbar.Brand>
                    <a href="/">
                        <Image src="/images/logo.png" alt="CV Counsellors" width={110} height={59} />
                        <span className={styles.com}>com</span>
                    </a>
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <div className='d-flex justify-content-center position-absolute' style={{ top: "0px", width: "99%" }}>
                    {globalStore.isActive.get() ?
                        <Badge bg="success">Online</Badge>
                        :
                        <Badge bg="warning">Offine</Badge>
                    }
                </div>
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className='me-auto align-items-lg-center'>
                        <NavItem title="Find Mentors" href="/counsellors" />
                        {/* <NavItem title="Video Consult" href="/consult" />
                        <NavItem title="Courses" href="/courses" />
                        <NavItem title="Testimonials" href="/testimonials" /> */}
                    </Nav>
                    <Nav className='align-items-lg-center'>
                        {/* <NavDropdown className={styles.nav_dropdown} title={<DropdownTitle title="For Counsellors" />}>
                            <DropdownItem title="Become a CV counsellor" href="/counsellor/register" new={false} />
                            <DropdownItem title="Software for counsellors" href="/counsellor" new={false} />
                        </NavDropdown>
                        <NavDropdown className={styles.nav_dropdown} title={<DropdownTitle title="Security &amp; help" />}>
                            <DropdownItem title="Data security" href="/data-security" new={false} />
                            <DropdownItem title="Help" href="/help" new={false} />
                        </NavDropdown> */}
                        {isAuth.get() ?
                            <NavDropdown className={`last-dropdown ${styles.nav_dropdown}`} title={<DropdownTitle title={getUser().name.substring(0, 10) + "..."} />}>
                                <div className={`${styles.profile_item} d-flex`}>
                                    {getUser().profile_image === "" ?
                                        <Image className={styles.profile_image} width={40} height={40} src="/images/profile.png" />
                                        :
                                        <Image className={styles.profile_image} width={40} height={40} src={getUser().profile_image} />
                                    }
                                    <div className={styles.user_name}>
                                        <Link href="/user/profile">
                                            <a className={styles.u_name} >{getUser().name}</a>
                                        </Link>
                                        <div className={styles.number}>{getUser().mobile}</div>
                                    </div>
                                </div>
                                <DropdownItem title="My Consultations" href="/user/consultations" new={false} />
                                <DropdownItem title="My Feedbacks" href="/user/feedbacks" new={false} />
                                {/* <DropdownItem title="View/Update Profile" href="/user/profile" new={false} />
                            <DropdownItem title="Settings" href="/user/settings" new={false} /> */}
                                <DropdownItem title="Logout" href="/" clickHandler={(e) => removeUserAuth(e)} />
                                {/* <NavDropdown.Divider />
                            <div onClick={() => window.location.href = "https://partner.cvcounselor.com"} className={styles.switch_div}>
                                <span className={styles.switch_div_span}>
                                    <span>Switch to counsellor&nbsp;&nbsp;</span>
                                    <FaChevronRight />
                                </span>
                                <div className={styles.switch_div_desc}>
                                    Manage your consultations, answer questions, write articles and more
                                </div>
                            </div> */}
                            </NavDropdown>
                            :
                            <div onClick={() => setupModal()} className={styles.login_register_button}>
                                Login / Signup
                            </div>
                        }
                    </Nav>
                </Navbar.Collapse>
            </Container>
            <CustomModal content={modalContent} showModal={showModal} modalSize="md" staticBackdrop={false} />
        </Navbar>

{/* <div className='bg-white d-flex justify-content-between align-items-center px-3'>
    <div> <a href="/">
                        <Image src="/images/logo.png" alt="CV Counsellors" width={110} height={59} />
                        <span className={styles.com}>com</span>
                    </a></div>
    <div>
    <Dropdown>
  <Dropdown.Toggle id="dropdown-basic">
    <AiOutlineMenu/>
  </Dropdown.Toggle>

  <Dropdown.Menu>
    <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
    <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
    <Dropdown.Item href="#/action-3">
        
    </Dropdown.Item>
  </Dropdown.Menu>
</Dropdown>
    </div>
</div> */}

     </>


    );
}

export default Header;