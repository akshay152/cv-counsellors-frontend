import Link from "next/link";
import styles from './Header.module.css';
import { useRouter } from 'next/router';

const NavItem = (props) => {

    const router = useRouter();

    return (
        <Link href={props.href}>
            <a className={`${styles.nav_link} ${router.pathname === props.href ? styles.active : ''}`}>{props.title}</a>
        </Link>
    );
}

export default NavItem;