import styles from './TestimonialSlider.module.css';
import { Swiper, SwiperSlide } from 'swiper/react';
import Image from 'next/image';
import { Autoplay } from "swiper";

const TestimonialSlider = () => {

    const testimonials = [
        {
            id: 1,
            image: "/images/testimonial-1.jpg",
            name: "Sikha",
            comment: `"I had a great experience. I wasn't sure what I wanted to do with my life. Isha Sharma, on the other hand, is an exceptional individual who leads a highly competent team. College Vidya was a really beneficial experience for us. We are honoured and grateful for the time we spent with him"`
        },
        {
            id: 2,
            image: "/images/testimonial-2.jpg",
            name: "Ananya",
            comment: `"College Vidya enabled me to realise my ambition of making it great in the corporate world. They not only assisted me in shortlisting a few quality management programmes, but they also offered ongoing assistance and support in Admissions."`
        },
        {
            id: 3,
            image: "/images/testimonial-3.jpg",
            name: "Gagan",
            comment: `"I never imagined I'd be able to get a college degree online since there are so many institutions and it's so complicated. Tabassum and his team's efforts were essential in gaining admission to India's finest Online Course"`
        }
    ]

    return (
        <div className='mt-3'>
            <Swiper loop={true} autoplay={{ delay: 2500, disableOnInteraction: false }} modules={[Autoplay]}>
                {testimonials.map(testimonial => 
                    <SwiperSlide key={testimonial.id}>
                        <div className={styles.review_card}>
                            <div className="d-flex">
                                <div className={styles.review_card_left}>
                                    <div className={styles.review_card_img}>
                                        <Image src={testimonial.image} width={150} height={150} />
                                    </div>
                                    <img className={styles.dot_s} src="/images/dots.svg" width={85} height={108} />
                                </div>
                                <div className={styles.review_card_right}>
                                    <h3 className='fw-bold'>{testimonial.name}</h3>
                                    <p>{testimonial.comment}</p>
                                </div>
                            </div>
                        </div>
                    </SwiperSlide>
                )}
            </Swiper>
        </div>
    );
}

export default TestimonialSlider;