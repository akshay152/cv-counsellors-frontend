import { Downgraded, useState } from '@hookstate/core';
import moment from 'moment';
import Image from 'next/image';
import Link from 'next/link';
import { Button, Col, NavDropdown, Row, Stack, Badge } from 'react-bootstrap';
import { MdCall, MdVideocam } from 'react-icons/md';
import { BsArrowLeftShort, BsThreeDotsVertical } from 'react-icons/bs';
import React, { useEffect } from 'react';
import { FiAlertTriangle } from 'react-icons/fi';
import store from '../../../utils/store';
import styles from './ChatHeader.module.css';
import CustomModal from '../../global/customs/CustomModal';
import ReportModal from './modals/ReportModal';
import CounsellorInfo from './modals/CounsellorInfo';

const ChatHeader = (props) => {

    const globalStore = useState(store);

    const [statusText, setStatusText] = React.useState("Offline");
    const [statusColor, setStatusColor] = React.useState("gray");

    const modalContent = useState(null);
    const showModal = useState(false);

    function handleCall(type) {
        if (moment(props.startTime).valueOf() <= moment().valueOf()) {
            if (globalStore.currentCounsellorAvailability.get()) {
                if (type === "video") {
                    window.location.href = "/consultation/" + props.videoRoomID + "?type=video&counsellor_id=" + globalStore.activeConsultation.attach(Downgraded).get().counsellor_id;
                }
                else {
                    window.location.href = "/consultation/" + props.videoRoomID;
                }
            }
            else {
                iziToast.warning({
                    title: "Unavailable",
                    message: `${props.userName} is currently unavailable for a call. Send a message and they would reply asap`,
                    timeout: 2000,
                    position: "topRight",
                });
            }
        }
        else {
            iziToast.warning({
                title: "Consultation Inactive",
                message: `This consultation will become active at ${moment(props.startTime).format('lll')}`,
                timeout: 2000,
                position: "topRight",
            });
        }
    }

    function handleChatClose() {
        globalStore.activeConsultation.set({
            id: -1,
            unread: false,
            image: "",
            name: "",
            lastMessage: "",
            start_time: "",
            video_room_id: "",
            audio_room_id: "",
            lastMessageTime: "",
            counsellor_id: -1,
            finished: false
        });
        globalStore.currentChatMessages.set([]);
    }

    useEffect(() => {
        if (globalStore.socketRef.attach(Downgraded).get().current) {
            globalStore.socketRef.attach(Downgraded).get().current.on("user-status-update", (consultationID, status) => {
                if (globalStore.activeConsultation.attach(Downgraded).get().id === consultationID) {
                    setStatusColor("green");
                    if (status.includes("typing")) {
                        setStatusText("typing...");
                        globalStore.typing.set(true);
                    }
                    else {
                        setStatusText("Online");
                        globalStore.typing.set(false);
                    }
                }
            });

            globalStore.socketRef.attach(Downgraded).get().current.on("online-offline-status", (counsellorID, status) => {
                if (globalStore.activeConsultation.attach(Downgraded).get().counsellor_id === counsellorID) {
                    if (status === "ONLINE") {
                        setStatusColor("green");
                        setStatusText("Online");
                    }
                    else {
                        setStatusColor("gray");
                        setStatusText("Offline");
                    }
                }
            });
        }
    }, [globalStore.socketRef.attach(Downgraded).get()]);

    useEffect(() => {
        if (globalStore.currentCounsellorAvailability.get()) {
            setStatusText("Online");
            setStatusColor("green");
        }
        else {
            setStatusText("Offline");
            setStatusColor("gray");
        }
    }, [globalStore.currentCounsellorAvailability.get()]);

    function setupModal(key, eKey = '') {
        if (key === 'report-modal') {
            modalContent.set(<ReportModal counsellorID={eKey} closeModal={() => showModal.set(false)} />);
            showModal.set(true);
        }
        if (key === 'show-counsellor-info') {
            modalContent.set(<CounsellorInfo counsellorID={eKey} closeModal={() => showModal.set(false)} />);
            showModal.set(true);
        }
    }

    return (
        <div className={`${styles.chat_header_main} border-bottom`}>
            <Row className={`align-items-center`}>
                <Col xs="6" className='d-flex align-items-center' style={{ gap: 10 }}>
                    <Stack direction='horizontal' gap={2}>
                        <Button onClick={() => handleChatClose()} className={`${styles.header_icon} ${styles.header_icon_left} d-block d-sm-none`}>
                            <BsArrowLeftShort />
                        </Button>
                        <div style={{ borderRadius: "30px", overflow: "hidden", height: "40px", width: "40px" }}>
                            {props.userImage !== "" ?
                                <Image src={props.userImage} height={40} width={40} layout="responsive" />
                                :
                                <Image src="/images/profile.png" height={40} width={40} layout="responsive" />
                            }
                        </div>
                        <span onClick={() => setupModal('show-counsellor-info', globalStore.activeConsultation.attach(Downgraded).get().counsellor_id)} style={{ cursor: "pointer", textDecoration: "none", color: "#444444", whiteSpace: "nowrap", overflow: "hidden", textOverflow: "ellipsis" }}>
                            {props.userName}
                            <p style={{ fontSize: "12px", display: "flex", alignItems: "center" }} className='mb-0'><span style={{ height: "5px", width: "5px", borderRadius: "50%", backgroundColor: statusColor, display: "inline-block" }}></span>&nbsp;{statusText} <Badge pill bg="primary" className="ms-2" >
                                <span style={{ fontSize: "10px" }}>Know More</span>
                            </Badge></p>
                        </span>
                    </Stack>

                </Col>
                <Col xs="6">
                    {/* <span style={{borderRadius: "10px", fontSize: "14px", padding: "2px 10px", backgroundColor: "#cce7ff"}}>
                        <Link href={props.profLink} passHref>
                            <a target="_blank" style={{textDecoration: "none", color: "#0074d7"}}>
                                Know More
                            </a>
                        </Link>
                    </span> */}



                    <div>
                        {globalStore.activeConsultation.attach(Downgraded).get().finished ?
                            <Stack className='help-header' direction='horizontal' gap={2}>
                                <NavDropdown className='ms-auto' title={<span style={{ color: "#0074d7", cursor: "pointer" }}>Help</span>}>
                                    <NavDropdown.Item onClick={() => setupModal('report-modal', globalStore.activeConsultation.attach(Downgraded).get().counsellor_id)}>
                                        <span><FiAlertTriangle color='red' />&nbsp; Report</span>
                                    </NavDropdown.Item>
                                </NavDropdown>
                            </Stack>
                            :
                            <Stack className='help-header' direction='horizontal' gap={3}>
                                {/* <Button onClick={() => handleCall("audio")} className={`${styles.header_icon} ms-auto`}>
                                <MdCall />
                            </Button> */}
                                <Button onClick={() => handleCall("video")} className={`${styles.header_icon} ms-auto`}>
                                    <MdVideocam />
                                </Button>
                                {/* <NavDropdown title={<span style={{color: "#0074d7", cursor: "pointer"}}>Help</span>}>
                                <NavDropdown.Item onClick={() => setupModal('report-modal', globalStore.activeConsultation.attach(Downgraded).get().counsellor_id)}>
                                    <span><FiAlertTriangle color='red' />&nbsp; Report</span>
                                </NavDropdown.Item>
                            </NavDropdown> */}
                                <NavDropdown title={<BsThreeDotsVertical fontSize={20} className='text-dark' />} id="basic-nav-dropdown">
                                    {/* <Link href={props.profLink} passHref><a className='px-3 text-decoration-none text-dark' target="_blank" style={{ fontSize: "12px" }}>Know More</a></Link> */}
                                    <NavDropdown.Item style={{ fontSize: "12px" }} passHref onClick={() => setupModal('report-modal', globalStore.activeConsultation.attach(Downgraded).get().counsellor_id)}>Report</NavDropdown.Item>

                                </NavDropdown>
                            </Stack>
                        }
                    </div>


                </Col>
            </Row>
            <CustomModal dialogClass={true} content={modalContent} showModal={showModal} modalSize="md" staticBackdrop={false} />
        </div>
    );
}

export default ChatHeader;