import styles from './ConsultationWrapper.module.css';

const ConsultationWrapper = ({children}) => {
    return (
        <div className={`${styles.chat_main} position-relative`}>
            {children}
        </div>
    )
}

export default ConsultationWrapper;