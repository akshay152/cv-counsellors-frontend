import { Row, Col, Form, InputGroup, FormControl, ListGroup } from "react-bootstrap";
import { Downgraded, useState } from "@hookstate/core";
import { BsChatTextFill } from 'react-icons/bs';
import { Tooltip } from 'primereact/tooltip';
import Link from 'next/link';
import store from "../../../utils/store";
import styles from './ChatList.module.css';
import ChatListItem from "./ChatListItem";
import { useEffect } from "react";
import { getUserChats, readConsultation } from "../../../utils/chat";
import { getUser } from "../../../utils/common";

const ChatList = ({ children }) => {

    const globalStore = useState(store);
    const searchText = useState("");

    const fakeList = useState([]);

    useEffect(() => {
        getUserChats().then((response) => {
            globalStore.currentChatList.set(response.data.description);
            fakeList.set(response.data.description);
        })
    }, []);

    useEffect(() => {
        fakeList.set(globalStore.currentChatList.attach(Downgraded).get());
    }, [globalStore.currentChatList.attach(Downgraded).get()]);

    useEffect(() => {
        if (searchText.get() === "") {
            fakeList.set(
                globalStore.currentChatList.attach(Downgraded).get().sort((a, b) => {
                    return Date.parse(JSON.parse(b.lastMessageDetails)[0].lastMessageTime) - Date.parse(JSON.parse(a.lastMessageDetails)[0].lastMessageTime);
                }
                ))
        }
        else {
            fakeList.set(
                globalStore.currentChatList.attach(Downgraded).get().sort((a, b) => {
                    return Date.parse(JSON.parse(b.lastMessageDetails)[0].lastMessageTime) - Date.parse(JSON.parse(a.lastMessageDetails)[0].lastMessageTime);
                }).filter(chat => chat.name.toLowerCase().includes(searchText.get().toLowerCase()))
            )
        }
    }, [searchText.get()])

    return (
        <div className={styles.chat_list_main} style={{ padding: '30px 20px 0' }}>
            <Row>
                <Col>
                    <div className="mb-3 d-flex align-items-center" style={{ justifyContent: "space-between" }}>
                        <h5 className={`${styles.chat_list_heading} mb-0 fw-bold`}>Ongoing consultations</h5>
                        {/* <Tooltip target=".new-chat" />
                        <div data-pr-tooltip="Start a new consultation" className="new-chat" style={{padding: "0px 15px", cursor: "pointer"}}>
                            <Link href="/consult">
                                <a><BsChatTextFill fontSize={18} color="#14bef0" /></a>
                            </Link>
                        </div> */}
                    </div>
                    <Form>
                        <InputGroup className="mb-3">
                            <FormControl onChange={(e) => searchText.set(e.target.value)} className="shadow-none cv-input-field" placeholder="Search..." />
                        </InputGroup>
                    </Form>
                </Col>
            </Row>
            <Row>
                <Col>
                    <div className={styles.chat_list}>
                        <ListGroup className={styles.chat_list_group}>
                            {fakeList.attach(Downgraded).get().sort((a, b) => {
                                return Date.parse(JSON.parse(b.lastMessageDetails)[0].lastMessageTime) - Date.parse(JSON.parse(a.lastMessageDetails)[0].lastMessageTime);
                            }).map(consultation =>
                                <ChatListItem key={consultation.id} onClick={() => {
                                    readConsultation({ "customer_id": getUser().id, "consultation_id": consultation.id }).then(() => {
                                        getUserChats().then((response) => {
                                            globalStore.currentChatList.set(response.data.description);
                                        });
                                    });
                                    globalStore.lowerLimit.set(1);
                                    globalStore.upperLimit.set(100);
                                    globalStore.activeConsultation.set(consultation);
                                }
                                } unread={JSON.parse(consultation.lastMessageDetails).unread} consultation={consultation} />
                            )}
                        </ListGroup>
                    </div>
                </Col>
            </Row>
            {children}
        </div>
    )
}

export default ChatList;