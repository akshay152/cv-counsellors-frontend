import { Downgraded, useState } from '@hookstate/core';
import { Button, Col, Form, FormControl, Row, Spinner } from 'react-bootstrap';
import { FaArrowUp, FaMicrophone, FaRegStopCircle, FaTimes } from 'react-icons/fa';
import { ImAttachment, ImCancelCircle } from 'react-icons/im';
import { MdSend } from 'react-icons/md';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { generate } from 'shortid';
import { AiOutlinePlus } from 'react-icons/ai';
import store from '../../../utils/store';
import styles from './ChatFooter.module.css';
import { getUserChats, readConsultation, scrollToBottom, sendMessage, sendMessagePingToSocket, sendStatusPingToSocket } from '../../../utils/chat';
import { getUser } from '../../../utils/common';
import { Image } from 'primereact/image';
import { IoMdDocument } from 'react-icons/io';
import axiosJWT from '../../../utils/jwtAxios';
import axios from 'axios';
import dynamic from 'next/dynamic';
import { BsFillRecordCircleFill } from 'react-icons/bs';
import { useEffect, useRef } from 'react';

const ReactMic = dynamic(
    () => import('react-mic').then((mod) => mod.ReactMic),
    {
        ssr: false
    }
);

const ChatFooter = (props) => {

    const audioUIRef = useRef(null);

    const globalStore = useState(store);
    const sendingAttachments = useState(false);
    const sendingAudio = useState(false);
    const isAudioRecording = useState(false);
    const recordingUIShown = useState(false);

    const showSendAudioUI = useState(false);
    const audioPlayerRef = useRef(null);

    const audioBlob = useState(null);

    const validationSchema = Yup.object({
        message: Yup.string().required('Please type a message')
    });

    const formik = useFormik({
        initialValues: {
            message: "",
            caption: ""
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {

            formik.handleReset();

            let formData = new FormData();
            formData.append("customer_id", getUser().id);
            formData.append("consultation_id", globalStore.activeConsultation.attach(Downgraded).get().id);
            formData.append("category", "text");
            formData.append("sender_type", "CUSTOMER");
            formData.append("message", values.message);
            formData.append("media", "");

            sendMessage(formData).then(() => {

                let message = {
                    id: generate(),
                    category: "text",
                    sender_type: "CUSTOMER",
                    msg: values.message,
                    media: ""
                }

                sendMessagePingToSocket(globalStore.socketRef.attach(Downgraded).get(), props.counsellorID, message, globalStore.activeConsultation.attach(Downgraded).get().id);

                globalStore.currentChatMessages.set([...globalStore.currentChatMessages.attach(Downgraded).get(), message]);
                getUserChats().then((response) => {
                    globalStore.currentChatList.set(response.data.description);
                });
                scrollToBottom();
            })
                .catch(() => {
                    iziToast.error({
                        title: "Message not sent",
                        message: "Your message couldn't be sent. Please try again.",
                        timeout: 2000
                    });
                });
        }
    });

    function sendAttachments() {
        sendingAttachments.set(true);

        globalStore.currentChatAttachments.attach(Downgraded).get().forEach(async (attachment, index) => {
            const { url } = await axiosJWT.get(process.env.NEXT_PUBLIC_BACKEND_URL + "s3URL?object=chat-doc&type=" + attachment.media.name.split(".").pop()).then((res) => res.data);
            axios.put(url, attachment.media)
                .then(() => {
                    const finalMedia = url.split("?")[0];

                    const category = attachment.media.type === "image/jpg" || attachment.media.type === "image/jpeg" || attachment.media.type === "image/png" || attachment.media.type === "image/gif" ? 'image' : attachment.media.type === "video/mp4" || attachment.media.type === "video/mov" ? 'video' : 'document';

                    let formData = new FormData();
                    formData.append("customer_id", getUser().id);
                    formData.append("consultation_id", globalStore.activeConsultation.attach(Downgraded).get().id);
                    formData.append("category", category);
                    formData.append("sender_type", "CUSTOMER");
                    formData.append("message", formik.values.caption);
                    formData.append("media", finalMedia);

                    sendMessage(formData).then(() => {

                        let message = {
                            id: attachment.id,
                            category: category,
                            sender_type: "CUSTOMER",
                            msg: formik.values.caption,
                            media: finalMedia
                        }

                        sendMessagePingToSocket(globalStore.socketRef.attach(Downgraded).get(), props.counsellorID, message, globalStore.activeConsultation.attach(Downgraded).get().id);

                        globalStore.currentChatMessages.set([...globalStore.currentChatMessages.attach(Downgraded).get(), message]);
                        getUserChats().then((response) => {
                            globalStore.currentChatList.set(response.data.description);
                        });
                        scrollToBottom();

                        if (index === globalStore.currentChatAttachments.attach(Downgraded).get().length - 1) {
                            globalStore.currentChatAttachments.set([]);
                            sendingAttachments.set(false);
                        }
                    })
                        .catch(() => {
                            iziToast.error({
                                title: "Message not sent",
                                message: "Your message couldn't be sent. Please try again.",
                                timeout: 2000
                            });

                            if (index === globalStore.currentChatAttachments.attach(Downgraded).get().length - 1) {
                                globalStore.currentChatAttachments.set([]);
                                sendingAttachments.set(false);
                            }
                        });
                })
        });
    }

    async function sendAudio() {
        sendingAudio.set(true);

        const { url } = await axiosJWT.get(process.env.NEXT_PUBLIC_BACKEND_URL + "s3URL?object=chat-doc&type=mp3").then((res) => res.data);

        axios.put
            (url, audioBlob.attach(Downgraded).get().blob)
            .then(() => {
                const finalMedia = url.split("?")[0];

                const category = "audio";

                let formData = new FormData();
                formData.append("customer_id", getUser().id);
                formData.append("consultation_id", globalStore.activeConsultation.attach(Downgraded).get().id);
                formData.append("category", category);
                formData.append("sender_type", "CUSTOMER");
                formData.append("message", "");
                formData.append("media", finalMedia);

                sendMessage(formData).then(() => {

                    let message = {
                        id: generate(),
                        category: category,
                        sender_type: "CUSTOMER",
                        msg: formik.values.caption,
                        media: finalMedia
                    }

                    sendMessagePingToSocket(globalStore.socketRef.attach(Downgraded).get(), props.counsellorID, message, globalStore.activeConsultation.attach(Downgraded).get().id);

                    globalStore.currentChatMessages.set([...globalStore.currentChatMessages.attach(Downgraded).get(), message]);
                    getUserChats().then((response) => {
                        globalStore.currentChatList.set(response.data.description);
                    });
                    scrollToBottom();

                    sendingAudio.set(false);
                    showSendAudioUI.set(false);
                })
                    .catch(() => {
                        iziToast.error({
                            title: "Message not sent",
                            message: "Your message couldn't be sent. Please try again.",
                            timeout: 2000
                        });
                    });
            });
    }

    function addDocument(media) {
        globalStore.currentChatAttachments.set([...globalStore.currentChatAttachments.attach(Downgraded).get(), { id: generate(), media: media }]);
    }

    function useOutsideAlerter(ref) {
        useEffect(() => {
            function handleClickOutside(event) {
                if (ref.current && !ref.current.contains(event.target) && !isAudioRecording.get()) {
                    recordingUIShown.set(false);
                }
            }

            document.addEventListener("mousedown", handleClickOutside);
            return () => {
                document.removeEventListener("mousedown", handleClickOutside);
            };
        }, [ref]);
    }

    useOutsideAlerter(audioUIRef);

    const handleAudioBlob = (audioB) => {
        recordingUIShown.set(false);
        showSendAudioUI.set(true);
        audioPlayerRef.current.src = audioB.blobURL;

        audioBlob.set(audioB);
    }

    useEffect(() => {
        if (globalStore.activeConsultation.attach(Downgraded).get().id !== -1) {
            var messageBox = document.getElementById("message-box");
            var timeout = setTimeout(function () { }, 0);
            if (messageBox) {
                messageBox.addEventListener('keypress', function () {
                    clearTimeout(timeout);
                    sendStatusPingToSocket(globalStore.socketRef.attach(Downgraded).get(), props.counsellorID, globalStore.activeConsultation.attach(Downgraded).get().id, "typing...");
                    timeout = setTimeout(function () { sendStatusPingToSocket(globalStore.socketRef.attach(Downgraded).get(), props.counsellorID, globalStore.activeConsultation.attach(Downgraded).get().id, "other"); }, 1000)
                })
            }
        }
    }, [globalStore.activeConsultation.attach(Downgraded).get()]);

    useEffect(() => {
        document.onkeydown = function (event) {
            if (event.key === 'Enter') {
                if (globalStore.currentChatAttachments.attach(Downgraded).get().length > 0) {
                    sendAttachments();
                }
                else if (showSendAudioUI.get()) {
                    sendAudio();
                }
            }
        }
    }, []);

    function continueConsultation() {
        iziToast.question({
            timeout: false,
            close: false,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            title: 'Hey',
            message: 'Are you sure you want to reopen this consultation?',
            position: 'center',
            buttons: [
                ['<button><b>Yes</b></button>', function (instance, toast) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                    var formData = new FormData();
                    formData.append("customer_id", getUser().id);
                    formData.append("consultation_id", globalStore.activeConsultation.attach(Downgraded).get().id);

                    axiosJWT.put(process.env.NEXT_PUBLIC_BACKEND_URL + "consultation/reopen", formData)
                        .then(() => {
                            readConsultation({ "customer_id": getUser().id, "consultation_id": globalStore.activeConsultation.attach(Downgraded).get().id }).then(() => {

                                iziToast.success({
                                    title: "Opened",
                                    message: "Your consultation has been successfully reopened.",
                                    position: "topRight",
                                    timeout: 2000
                                });

                                getUserChats().then((response) => {
                                    globalStore.currentChatList.set(response.data.description);
                                    var neededChat = response.data.description.filter(chat => chat.id === globalStore.activeConsultation.attach(Downgraded).get().id)[0];
                                    globalStore.activeConsultation.set(neededChat);
                                });
                            });
                        })
                        .catch(() => {
                            iziToast.error({
                                title: "Error",
                                message: "Oops! Something went wrong",
                                position: "topRight",
                                timeout: 2000
                            });
                        });

                }, true],
                ['<button>No</button>', function (instance, toast) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }],
            ],
        });
    }

    return (
        globalStore.activeConsultation.attach(Downgraded).get().finished ?
            <Button onClick={() => continueConsultation()} className='cv-primary-btn w-100 fw-bold' style={{ padding: "1.1rem" }}>
                Continue Consultation
            </Button>
            :
            <div style={{ padding: "10px 25px" }}>
                <Row>
                    <Col xs="12">
                        <input type="file" id='attach-file' style={{ display: "none" }} onChange={(e) => {
                            formik.setErrors({ message: null });

                            const media = e.target.files[0];
                            if (media.size > 100000000) {
                                e.target.value = null;
                                formik.setErrors({ message: "Document size cannot exceed 100MB" });
                                iziToast.error({
                                    title: "Maximum size exceeded",
                                    message: "Document size cannot exceed 100MB",
                                    position: "topRight",
                                    timeout: 2000
                                });
                                return;
                            }
                            addDocument(media);
                            e.target.value = null;
                        }} />
                        {globalStore.currentChatAttachments.attach(Downgraded).get().length === 0 ?
                            showSendAudioUI.get() ?
                                <div className={styles.audioWrapper}>
                                    <Button disabled={sendingAttachments.get()} onClick={() => showSendAudioUI.set(false)} variant='danger' className='me-2'><ImCancelCircle fontSize={22} /></Button>
                                    <audio style={{ width: '-webkit-fill-available' }} ref={audioPlayerRef} controls />
                                    {!sendingAudio.get() ?
                                        <Button onClick={() => sendAudio()} className='cv-primary-btn ms-2'><FaArrowUp fontSize={22} /></Button>
                                        :
                                        <Button className='cv-primary-btn ms-2'><Spinner animation='border' size='sm' /></Button>
                                    }
                                </div>
                                :
                                <Form onSubmit={formik.handleSubmit}>
                                    <div className='position-relative'>
                                        <span ref={audioUIRef} style={{ position: "relative" }} onClick={() => recordingUIShown.set(true)}>
                                            <FaMicrophone fontSize={18} color='#0074d7' style={{ position: "absolute", top: "12px", left: "12px", cursor: "pointer" }} />
                                            {recordingUIShown.get() ?
                                                <div className={styles.audio_record}>
                                                    <div style={{ marginLeft: "8px" }}>
                                                        {isAudioRecording.get() ?
                                                            <FaRegStopCircle onClick={() => isAudioRecording.set(false)} style={{ cursor: "pointer" }} fontSize={24} color={"#000"} />
                                                            :
                                                            <BsFillRecordCircleFill onClick={() => isAudioRecording.set(true)} style={{ cursor: "pointer" }} fontSize={24} color={"#9f3bff"} />
                                                        }
                                                    </div>
                                                    <ReactMic
                                                        record={isAudioRecording.get()}
                                                        backgroundColor="#F5F6F6"
                                                        strokeColor="#0074d7"
                                                        className={styles.audioPlayer}
                                                        echoCancellation={true}
                                                        mimeType="audio/mp3"
                                                        onStop={handleAudioBlob}
                                                    />
                                                </div>
                                                :
                                                null
                                            }
                                        </span>
                                        <FormControl name='message'
                                            id="message-box"
                                            value={formik.values.message}
                                            onChange={formik.handleChange}
                                            isInvalid={!!formik.errors.message}
                                            className={`shadow-none ${styles.chatbox}`} placeholder='Message' />
                                        <div className={`${styles.icon_holder}`}>
                                            <ImAttachment onClick={() => document.getElementById('attach-file').click()} style={{ cursor: "pointer" }} fontSize={14} />
                                            <MdSend onClick={() => formik.submitForm()} className={`${styles.send_btn}`} />
                                        </div>
                                        <Form.Control.Feedback type="invalid">
                                            {formik.errors.message}
                                        </Form.Control.Feedback>
                                    </div>
                                </Form>
                            :
                            <div className={styles.attachmentWrapper}>
                                {globalStore.currentChatAttachments.attach(Downgraded).get().map(attachment => {
                                    if (attachment.media.type === "image/jpeg" || attachment.media.type === 'image/jpg' || attachment.media.type === 'image/png' || attachment.media.type === 'image/gif') {
                                        return (
                                            <div key={attachment.id} className='d-flex' style={{ position: "relative" }}>
                                                <div onClick={() => { if (!sendingAttachments.get()) globalStore.currentChatAttachments.set(globalStore.currentChatAttachments.attach(Downgraded).get().filter(x => x.id !== attachment.id)) }} className={styles.attachmentClose}>
                                                    <FaTimes fontSize={13} style={{ opacity: "0.64" }} />
                                                </div>
                                                <Image className={styles.attachmentMedia} src={URL.createObjectURL(attachment.media)} alt="" height={90} width={90} preview={true} />
                                            </div>
                                        );
                                    }
                                    else if (attachment.media.type === "video/mp4" || attachment.media.type === "video/mov") {
                                        return (
                                            <div key={attachment.id} className='d-flex' style={{ position: "relative" }}>
                                                <div onClick={() => { if (!sendingAttachments.get()) globalStore.currentChatAttachments.set(globalStore.currentChatAttachments.attach(Downgraded).get().filter(x => x.id !== attachment.id)) }} className={styles.attachmentClose}>
                                                    <FaTimes fontSize={13} style={{ opacity: "0.64" }} />
                                                </div>
                                                <video className={styles.attachmentMedia} autoPlay={true} muted={true} src={URL.createObjectURL(attachment.media)} alt="" height={90} width={90} />
                                            </div>
                                        )
                                    }
                                    else {
                                        return (
                                            <div key={attachment.id} className='d-flex' style={{ position: "relative" }}>
                                                <div onClick={() => { if (!sendingAttachments.get()) globalStore.currentChatAttachments.set(globalStore.currentChatAttachments.attach(Downgraded).get().filter(x => x.id !== attachment.id)) }} className={styles.attachmentClose}>
                                                    <FaTimes fontSize={13} style={{ opacity: "0.64" }} />
                                                </div>
                                                <div className={styles.attachmentDocument}>
                                                    <IoMdDocument color="#0074d7" fontSize={54} />
                                                    <div style={{ fontSize: "10px", position: "absolute", bottom: "20px", left: "5px" }}>{attachment.media.name.length > 9 ? attachment.media.name.substring(0, 10) + "..." : attachment.media.name}</div>
                                                    <span style={{ fontSize: "12px", position: "absolute", bottom: "2px", left: "5px" }}>{attachment.media.name.split(".").pop().toUpperCase()}</span>
                                                </div>
                                            </div>
                                        );
                                    }
                                })}
                                <div onClick={() => { if (!sendingAttachments.get()) document.getElementById('attach-file').click() }} style={{ cursor: "pointer", display: "flex", height: "90px", width: "90px", background: "#fafafa", marginTop: "15px", border: "2px dashed #aab7b8", borderRadius: "8px", justifyContent: "center", alignItems: "center" }}>
                                    <AiOutlinePlus color='#aab7b8' fontSize={40} />
                                </div>
                                <div className='d-flex w-100 mt-3 justify-content-between'>
                                    <Button disabled={sendingAttachments.get()} onClick={() => globalStore.currentChatAttachments.set([])} variant='danger' className='me-2'><ImCancelCircle fontSize={22} /></Button>
                                    <FormControl name='caption' value={formik.values.caption}
                                        onChange={formik.handleChange}
                                        isInvalid={!!formik.errors.caption}
                                        placeholder='Add a caption'
                                        className={`shadow-none`} />
                                    {!sendingAttachments.get() ?
                                        <Button onClick={() => sendAttachments()} className='cv-primary-btn ms-2'><FaArrowUp fontSize={22} /></Button>
                                        :
                                        <Button className='cv-primary-btn ms-2'><Spinner animation='border' size='sm' /></Button>
                                    }
                                </div>
                            </div>
                        }
                    </Col>
                </Row>
            </div>
    )
}

export default ChatFooter;