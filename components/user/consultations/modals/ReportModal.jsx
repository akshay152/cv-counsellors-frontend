import { Button, Col, Form, FormControl, Modal, Row, Spinner } from "react-bootstrap";
import { useFormik } from "formik";
import * as Yup from "yup";
import { FiAlertTriangle } from "react-icons/fi";
import { useState } from "@hookstate/core";
import { getUser } from "../../../../utils/common";
import { submitReport } from "../../../../pages/api/misc";

const ReportModal = (props) => {

    const reasons = [
        {
            id: 0,
            reason: "Language barrier"
        },
        {
            id: 1,
            reason: "Impolite mentor"
        },
        {
            id: 2,
            reason: "Internet connection"
        },
        {
            id: 3,
            reason: "Non-responsive mentor"
        },
        {
            id: 4,
            reason: "Improper remarks"
        },
    ];

    const submitting = useState(false);

    const validationSchema = Yup.object({
        reason: Yup.string().required("Please select a reason"),
    });

    const formik = useFormik({
        initialValues: {
            reason: "",
            description: "",
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            submitting.set(true);

            submitReport(getUser().id, props.counsellorID, values.reason, values.description)
                .then(() => {
                    submitting.set(false);

                    iziToast.success({
                        title: "Report submitted",
                        message: "Someone of our team will get back to you",
                        timeout: 2000,
                        position: "topRight"
                    });

                    props.closeModal();
                })
                .catch(() => {
                    submitting.set(false);

                    iziToast.error({
                        title: "Error",
                        message: "Oops! Some error occured",
                        timeout: 2000,
                        position: "topRight"
                    });
                })
        },
    });

    return (
        <Modal.Body style={{ padding: "20px 30px" }}>
            <Form onSubmit={formik.handleSubmit}>
                <Row>
                    <Col>
                        <div>
                            <span
                                className="close_modal_button"
                                style={{ marginRight: "7px" }}
                                onClick={() => props.closeModal()}
                            />
                        </div>
                    </Col>
                </Row>
                <Row className="mt-3">
                    <Col>
                        <h3 className="d-flex align-items-center">
                            <FiAlertTriangle color="red" />
                            &nbsp;Report an issue
                        </h3>
                        <p style={{ color: "#444444", fontSize: "14px" }}>
                            Please fill the following details and we'll get back to you.
                        </p>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form.Label>
                            What seems to be the issue?<span style={{ color: "red" }}>*</span>
                        </Form.Label>
                        <Form.Select
                            name="reason"
                            className="shadow-none"
                            value={formik.values.reason}
                            onChange={formik.handleChange}
                            isInvalid={!!formik.errors.reason}
                        >
                            <option defaultValue={true} value="">Select an option</option>
                            {reasons.map(reason =>
                                <option key={reason.id} value={reason.reason}>{reason.reason}</option>
                            )}
                        </Form.Select>
                        <Form.Control.Feedback type="invalid">
                            {formik.errors.reason}
                        </Form.Control.Feedback>
                    </Col>
                </Row>
                <Row className="mt-2">
                    <Col>
                        <Form.Label>Description</Form.Label>
                        <FormControl
                            name="description"
                            value={formik.values.description}
                            onChange={formik.handleChange}
                            as="textarea"
                            rows={6}
                            placeholder="Describe the issue"
                        />
                    </Col>
                </Row>
                <Row className="mt-4">
                    <Col className="text-end">
                        {submitting.get() ?
                            <Button className="cv-primary-btn">
                                <Spinner size="sm" animation="border" />
                            </Button>
                            :
                            <Button type="submit" className="cv-primary-btn">
                                Submit
                            </Button>
                        }
                    </Col>
                </Row>
            </Form>
        </Modal.Body>
    );
};

export default ReportModal;
