import { Downgraded, useState } from "@hookstate/core";
import Image from "next/image";
import { useEffect } from "react";
import { Col, Modal, Row, Spinner } from "react-bootstrap";
import { getCounsellorDetails } from "../../../../pages/api/counsellor";
import { FaTimes } from 'react-icons/fa';
import styles from "../../../user/consultations/modals/CounsellorInfo.module.css";
import { BsFillArrowRightSquareFill } from "react-icons/bs";
import language from "../../../../public/images/language.png";
import verified from "../../../../public/images/check.png";
import Link from 'next/link';
import { FcGraduationCap } from "react-icons/fc";


const CounsellorInfo = (props) => {
  const counsellor = useState(null);

  useEffect(() => {
    getCounsellorDetails(props.counsellorID).then((response) => {
      counsellor.set(response.data[0]);
    });
  }, []);
  
  return (
    <Modal.Body style={{ padding: "0px", position: "relative", top: "0px" }}>
      <Row style={{position: 'absolute',zIndex:"1",right:"4px"}}>
        <Col className="text-end">
          <FaTimes style={{backgroundColor:"#fff",padding:"2px",borderRadius:"2px"}} onClick={() => props.closeModal()} />
        </Col>
      </Row>
      {counsellor.attach(Downgraded).get() ?
        <Row>
          <Col>
            <div className={`${styles.modalinfo} text-center`}>
              <div className="position-relative">
                {counsellor.attach(Downgraded).get().image ?
                  <Image width={100} height={100} src={counsellor.attach(Downgraded).get().image} layout="responsive" />
                  :
                  <Image width={100} height={100} src="/images/profile.png" layout="responsive" />
                }

                {counsellor.attach(Downgraded).get().verified ?
                  <p
                    className="position-absolute bottom-0 end-0 px-2 bg-white shadow-sm rounded-start d-flex align-items-center"
                    style={{ fontSize: "12px" }}
                  >
                    <span className="me-1">Verified</span>
                    <Image width={20} height={20} src={verified} />
                  </p>
                  :
                  null
                }
              </div>
              <div className="bg-white pt-4">
                <p className="fw-bold h4">{counsellor.attach(Downgraded).get().name}</p>
                <p style={{ fontSize: "14px" }}>({counsellor.attach(Downgraded).get().designation})</p>
                <p style={{ fontSize: "12px" }}> <FcGraduationCap fontSize={22} /> {counsellor.attach(Downgraded).get().qualification}</p>
                <p style={{ fontSize: "14px" }}>

                  <span style={{ color: "#fd4705" }}>

                    <Image width={20} height={20} src={language} /> Speaks :
                  </span>
                  English , हिन्दी
                </p>
                <p
                  className="d-inline-block px-2 py-1 shadow-sm mb-4"
                  style={{
                    backgroundColor: "#f0f8ff",
                    borderRadius: "11px",
                    fontSize: "12px",
                  }}
                >
                  {counsellor.attach(Downgraded).get().experience} Years exp
                </p>
                <Link href={`/counsellor/${counsellor.attach(Downgraded).get().slug}`}
                  style={{
                    backgroundColor: "#0074d7",
                    color: "#fff",
                    padding: "5px 20px",
                    borderRadius: "0px",
                  }}
                >
                  <a target="_blank" style={{textDecoration: "none", backgroundColor: "#0074d7", color: "white"}} className="text-center py-3 px-2 d-flex justify-content-between m-0 shadow-sm">
                    Know More
                    <BsFillArrowRightSquareFill
                      className="shadow-sm"
                      fontSize={30}
                    />
                  </a>
                </Link>
              </div>
            </div>
          </Col>
        </Row>
        :
        <Row>
          <Col>
            <div className="d-flex justify-content-center align-items-center" style={{ height: "300px" }}>
              <Spinner size="lg" animation="border" />
            </div>
          </Col>
        </Row>
      }
    </Modal.Body>
  );
};

export default CounsellorInfo;
