import { Modal } from 'react-bootstrap';
import { Swiper, SwiperSlide } from "swiper/react";
import { EffectCards, Autoplay } from "swiper";

import styles from './NotifyModalContent.module.css';
import Image from 'next/image';
import { Downgraded, useState } from '@hookstate/core';
import store from '../../../../../utils/store';
import Timer from "../modals/Timer"
import { RiDoubleQuotesL,RiDoubleQuotesR } from "react-icons/ri";

const NotifyModalContent = () => {

    const globalStore = useState(store);

    return (
      
   
            <Modal.Body style={{padding: "20px 30px"}}>
            <div className='d-flex align-items-center justify-content-center'>
                <h5 className='text-center'><sup><RiDoubleQuotesL fontSize={20} /></sup> <i>Everything comes to those who wait! </i> <sub><RiDoubleQuotesR fontSize={20}/></sub></h5>
                
            </div>
            <p className='text-end' style={{fontSize:"10px"}}>- Mahatma Gandhi</p>
            <div className='d-flex justify-content-center'>
            <Timer/>
            </div>
            <p className='text-center my-4' style={{fontSize:"13px"}}>So, sit tight! Your counsellor is also trying to connect with you.</p>
           
            <div className={styles.card_wrapper}>
                <Swiper loop={true} autoplay={{ delay: 1500, disableOnInteraction: false }} effect={"cards"} grabCursor={true} modules={[EffectCards, Autoplay]} className="cardSwiper">
                    {globalStore.activeCounsellors.attach(Downgraded).get().map(activeCounsellor => {
                        return (
                            <SwiperSlide key={activeCounsellor.counsellor_id}>
                                <div className={styles.card_wrapper}>
                                    <div className={styles.image_wrapper}>
                                        <Image src={activeCounsellor.image} width={100} height={100} />
                                    </div>
                                </div>
                            </SwiperSlide>
                        );
                    })}
                </Swiper>
            </div>
            <div className='mt-5 d-flex justify-content-center mb-2'>
                <small>Please don't close or refresh your browser</small>
            </div>
           
        </Modal.Body>
   
      
    )
}

export default NotifyModalContent;