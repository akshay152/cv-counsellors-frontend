import React from "react";
import { CountdownCircleTimer } from "react-countdown-circle-timer";

const Timer = () => {
  return (
    <div>
      <CountdownCircleTimer
        isPlaying
        duration={20}
        colors={["#004777", "#F7B801", "#A30000", "#A30000"]}
        colorsTime={[7, 5, 2, 0]}
        size={70}
        strokeWidth={3}
        onComplete={() => {
          return { shouldRepeat: true, delay: 0 };
        }}
      >
        {({ remainingTime }) => remainingTime}
      </CountdownCircleTimer>
    </div>
  );
};

export default Timer;
