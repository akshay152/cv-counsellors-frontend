import { Button, Col, Modal, Row } from 'react-bootstrap';
import Lottie from 'lottie-react';

import animationData from '../../../../../public/animation/no-counsellor-online.json';

const NoCounsellorModalContent = (props) => {
    return (
        <Modal.Body style={{ padding: "20px 30px" }}>
            <Row>
                <Col>
                    <div>
                        <span className="close_modal_button" style={{ marginRight: "7px" }} onClick={() => props.closeModal()} />
                    </div>
                </Col>
            </Row>
            <Row className='mt-3'>
                <Col style={{ display: "flex", justifyContent: "center" }}>
                    <div style={{ width: "250px", height: "250px" }}>
                        <Lottie animationData={animationData} autoPlay={true} loop={true} />
                    </div>
                </Col>
            </Row>
            <Row>
                <Col className='text-center'>
                    <h5 className='fw-bold'> No Mentors are online right now...But there is good news! </h5>
                </Col>
            </Row>
            <Row className='mt-4'>
                <Col className='text-center'>
                    <Button onClick={() => props.schedule()} style={{fontSize: "20px"}} className='cv-primary-btn'>Schedule for later</Button>
                </Col>
            </Row>
        </Modal.Body>
    )
}

export default NoCounsellorModalContent