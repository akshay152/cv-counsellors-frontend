import { Button, Col, FormControl, InputGroup, Modal, Row } from 'react-bootstrap';
import { AvatarGroup } from 'primereact/avatargroup';
import { Avatar } from 'primereact/avatar';
import { HiOutlineLightBulb } from 'react-icons/hi';
import { BiVideo } from 'react-icons/bi';
import Image from 'next/image';
import useRazorpay from 'react-razorpay';
import { generate } from 'shortid';
import axiosJWT from '../../../../utils/jwtAxios';
import styles from './InitiateConsultationWrapper.module.css';
import { getUser } from '../../../../utils/common';
import { Downgraded, useState } from '@hookstate/core';
import store from '../../../../utils/store';
import NotifyModalContent from './modals/NotifyModalContent';
import { useEffect } from 'react';
import { getCustomerDetails } from '../../../../pages/api/customer';
import { useRouter } from 'next/router';
import NoCounsellorModalContent from './modals/NoCounsellorModalContent';
import offimage from "../../../../public/images/100-percent.png";
import { FaHandsHelping } from 'react-icons/fa';

const InitiateConsultationWrapper = (props) => {

    const router = useRouter();

    const Razorpay = useRazorpay();
    const globalStore = useState(store);

    const modalContent = useState(null);
    const showModal = useState(false);
    const modalSize = useState("lg");

    const customer = useState(null);

    const modeID = useState(null);
    const courseID = useState(null);
    const specializationID = useState(null);

    function NotifyCounsellorsModal(props) {
        return (
            <Modal {...props}
                backdrop="static"
                size={modalSize.get()}
                centered>
                {modalContent.attach(Downgraded).get()}
            </Modal>
        )
    }

    function setupModal(key) {
        if (key === 'notify-counsellors') {
            modalSize.set("md");
            modalContent.set(<NotifyModalContent />);
            showModal.set(true);
        }
        if (key === 'no-counsellor-online') {

            let formData = new FormData();
            formData.append('name', customer.attach(Downgraded).get().name);
            formData.append('mobile', customer.attach(Downgraded).get().mobile);
            formData.append('email', customer.attach(Downgraded).get().email);
            formData.append('cv_id', customer.attach(Downgraded).get().cv_id);
            formData.append('course_id', courseID.get());
            formData.append('specialization_id', specializationID.get());
            axiosJWT.post(process.env.NEXT_PUBLIC_BACKEND_URL + "misc/button-click-mail", formData);

            modalSize.set("md");
            modalContent.set(<NoCounsellorModalContent closeModal={() => showModal.set(false)} schedule={schedule} />);
            showModal.set(true);
        }
    }

    function schedule() {
        if (modeID.get() && courseID.get() && specializationID.get()) {
            router.push("/counsellors?mode=" + modeID.get() + "&course=" + courseID.get() + "&specialization=" + specializationID.get());
        }
        else {
            router.push("/counsellors");
        }
    }

    useEffect(() => {
        getCustomerDetails(getUser().id).then((response) => {
            customer.set(response.data[0]);
        })
    }, []);

    useEffect(() => {
        if (router) {
            modeID.set(router.query.mode);
            courseID.set(router.query.course);
            specializationID.set(router.query.specialization);
        }
    }, [router]);

    const handlePayment = async (params) => {

        const order = await createOrder(params);

        const options = {
            key: process.env.NEXT_PUBLIC_RAZORPAY_KEY_ID,
            amount: 399 * 100,
            currency: "INR",
            name: "CV Counsellors",
            description: "Online consultation for education",
            order_id: order.id,
            handler: function (response) {
                paymentSuccessful(true, response.razorpay_payment_id);
            },
            prefill: {
                name: getUser().name,
                email: getUser().email,
                contact: getUser().mobile,
            },
            theme: {
                color: "#14bef1",
            },
        }

        const rzp1 = new Razorpay(options);

        rzp1.on("payment.failed", function (response) {
            iziToast.error({
                title: "Payment failed",
                message: response,
                timeout: 2000,
                position: "topRight",
            });
        });

        rzp1.open();
    }

    const paymentSuccessful = (fromRazorpay, paymentID) => {

        let formData = new FormData();
        formData.append('name', customer.attach(Downgraded).get().name);
        formData.append('mobile', customer.attach(Downgraded).get().mobile);
        formData.append('email', customer.attach(Downgraded).get().email);
        formData.append('cv_id', customer.attach(Downgraded).get().cv_id);
        formData.append('course_id', courseID.get());
        formData.append('specialization_id', specializationID.get());
        axiosJWT.post(process.env.NEXT_PUBLIC_BACKEND_URL + "misc/button-click-mail", formData);

        globalStore.socketRef.attach(Downgraded).get().current.emit("notify-course-counsellors", globalStore.activeCounsellors.attach(Downgraded).get().map(activeCounsellor => activeCounsellor.counsellor_id), customer.attach(Downgraded).get().id, customer.attach(Downgraded).get().name, customer.attach(Downgraded).get().profile_image, props.specializationID, paymentID);

        globalStore.socketRef.attach(Downgraded).get().current.on("oppurtunity-accepted", (consultationID) => {
            showModal.set(false);
            window.location.href = "/user/consultations?consultation_id=" + consultationID;
        });

        if (fromRazorpay) {
            iziToast.success({
                title: "Payment successful",
                message: response,
                timeout: 2000,
                position: "topRight",
            });
        }

        setupModal('notify-counsellors');
    }

    async function createOrder(params) {

        var formData = new FormData();

        formData.append("amount", params.amount);
        formData.append("currency", params.currency);
        formData.append("receipt", params.receipt);

        const order = await axiosJWT.post(process.env.NEXT_PUBLIC_BACKEND_URL + "consultation/create/instant", formData)
            .then((response) => {
                return response.data;
            });

        return order;
    }

    return (
        <Row className='justify-content-center'>
            <Col lg="8">
                <div className={styles.wrapper_main}>
                    <Row>
                        <Col>
                            <h5 className='fw-bold'>Confirm &amp; Pay</h5>
                        </Col>
                    </Row>
                    <div className='d-flex justify-content-between mt-2'>
                        <div className={styles.showForMobile}>
                            <div style={{ fontSize: "14px", color: "#2d2d32", display: "flex", alignItems: "center" }}>
                                Verified Mentors online now &nbsp;&nbsp;<div className={`${styles.pulse} ${styles.online_dot}`} />
                            </div>
                            <div className='mt-2 mb-2'>
                                <AvatarGroup>
                                    {globalStore.activeCounsellors.attach(Downgraded).get().slice(0, 4).map(activeCounsellor =>
                                        <Avatar key={activeCounsellor.counsellor_id} image={activeCounsellor.image} size='large' shape='circle' />
                                    )}
                                    {globalStore.activeCounsellors.attach(Downgraded).get().length >= 4 ?
                                        <span style={{ fontSize: "14px", color: "#787887" }}>&nbsp;+more</span>
                                        :
                                        null
                                    }
                                </AvatarGroup>
                            </div>
                            <div className='mt-3 mb-2'>
                                One of them will speak to you shortly.
                            </div>
                            <div className='mt-3 mb-2'>
                                <div className={styles.consultation_property}>
                                    <HiOutlineLightBulb fontSize={26} color="#2e8930" />
                                    <span className={styles.consultation_property_tag}>&nbsp;93% of users found online consultation helpful.</span>
                                </div>
                                <div className={`${styles.consultation_property} mt-1`}>
                                    <BiVideo style={{ marginLeft: "5px" }} fontSize={20} color="#2e8930" />
                                    <span style={{ marginTop: "1px" }} className={styles.consultation_property_tag}>&nbsp;Consultation will include video, audio and chat.</span>
                                </div>
                            </div>
                            {/* <div className='mt-4 mb-2' style={{ width: "80%" }}>
                                <div style={{ fontSize: "14px", color: "#0074d7" }}>Have a coupon code?</div>
                                <InputGroup className='mt-2'>
                                    <FormControl className='shadow-none cv-input-field'
                                        placeholder='AJS89A0XXXX' />
                                    <InputGroup.Text style={{ cursor: "pointer", backgroundColor: "#fff" }}>Apply</InputGroup.Text>
                                </InputGroup>
                            </div> */}
                            <div style={{ color: "#787887", fontSize: "14px" }}>
                                    Final Fee
                                </div>
                            <div className='mt-3 mb-2 d-flex'>
                                
                                {process.env.NEXT_PUBLIC_CALL_COST > 0 ?
                                    <div className='fw-bold' style={{ fontSize: "36px", }} >
                                        ₹{process.env.NEXT_PUBLIC_CALL_COST}
                                    </div>
                                    :
                                    <>
                                        <div className='d-flex align-items-center me-1'>
                                            <div className='fw-bold' style={{ fontSize: "36px" }} >
                                                ₹0
                                            </div>&nbsp;
                                            <div style={{ fontSize: "18px", textDecorationLine: "line-through", color: "#C8C8C8" }} >
                                                ₹49
                                            </div>
                                            
                                            {/* <div style={{ fontSize: "16px", color: "#14bef0" }}>
                                                &nbsp;100% off
                                            </div> */}
                                        </div>
                                        <div className={`${styles.dealbtn} mt-2 position-relative ms-4`}>
                                            <span className='text-white fw-bold' style={{fontSize: "14px"}}>Deal of the day <span className='d-flex position-absolute top-50 start-0 translate-middle'><Image width={40} height={42} src={offimage} /></span> </span>
                                        </div>
                                    </>
                                }
                            </div>

                            <div className='mt-4 mb-2'>
                                {globalStore.activeCounsellors.attach(Downgraded).get().length > 0 ?
                                    process.env.NEXT_PUBLIC_CALL_COST > 0 ?
                                        <Button onClick={() => handlePayment({ amount: process.env.NEXT_PUBLIC_CALL_COST * 100, currency: "INR", receipt: "receipt_" + generate() })} className='cv-primary-btn shadow-none fw-bold'>Proceed now&nbsp;&nbsp;<FaHandsHelping fontSize={23} /></Button>
                                        :
                                        <Button className='cv-primary-btn shadow-none fw-bold' onClick={() => paymentSuccessful(false, generate())}>Proceed now&nbsp;&nbsp;<FaHandsHelping fontSize={23} /></Button>
                                    :
                                    <Button onClick={() => {
                                        setupModal('no-counsellor-online');
                                    }} className='cv-primary-btn shadow-none fw-bold'>Proceed now&nbsp;&nbsp;<FaHandsHelping fontSize={23} /></Button>
                                }
                            </div>
                        </div>
                        <div className={`align-items-center justify-content-center flex-column ${styles.hideForMobile}`}>
                            <Image src="/images/new-img.png" height={200} width={200} />
                            <h5>#Irreversible Decision</h5>
                            <p style={{ fontSize: "14px", color: "#787887", textAlign: "center", width: "300px" }}>Connect with Senior Mentor and select best online university according to your needs!</p>
                        </div>
                    </div>
                </div>
                <div className='d-flex align-items-center' style={{ flexDirection: "column" }}>
                    <p style={{ fontSize: "12px", color: "#787887" }}>CV Counsellors Guarantee: 100% Money back if no response</p>
                    <p className='w-75' style={{ fontSize: "12px", color: "#787887", textAlign: "center" }}>The contents of your consultations are private and confidential. CV Counsellor's team of analysts may
                        carry out routine anonymised audits to improve service quality. T&amp;C apply</p>
                </div>
            </Col>
            <NotifyCounsellorsModal show={showModal.get()} onHide={() => showModal.set(false)} />
        </Row>
    )
}

export default InitiateConsultationWrapper;