import { Downgraded, useState } from "@hookstate/core";
import store from "../../../utils/store";

const ChatWrapper = ({ children }) => {

    const globalStore = useState(store);

    return (
        <div className={`consultations_chat_box ${globalStore.activeConsultation.attach(Downgraded).get().id !== -1 ? 'consultations_active' : ''}`} style={{height: "100%", background: "white"}}>
            {children}
        </div>
    )
}

export default ChatWrapper;