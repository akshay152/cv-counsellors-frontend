import React from 'react'
import { Button, Col, Form, FormControl, Modal, Row, Spinner } from 'react-bootstrap';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useState } from '@hookstate/core';
import { getUser } from '../../../../utils/common';
import axiosJWT from '../../../../utils/jwtAxios';

const UpdateEmailModal = (props) => {

    const saving = useState(false);

    const validationSchema = Yup.object({
        email: Yup.string().email('Invalid email').required('Email address is required'),
    });

    const formik = useFormik({
        initialValues: {
            email: props.email
        },
        enableReinitialize: true,
        validationSchema: validationSchema,
        onSubmit: (values) => {
            saving.set(true);

            let formData = new FormData();
            formData.append('user_id', getUser().id);
            formData.append('name', props.name);
            formData.append('email_address', values.email);

            axiosJWT.post(process.env.NEXT_PUBLIC_BACKEND_URL+"customer/update-email", formData)
            .then(() => {
                saving.set(false);
                iziToast.success({
                    title: "Verify your email address",
                    message: "We have sent you an email with a link to verify your email address",
                    timeout: 2000,
                    position: "topRight"
                });
                props.closeModal();
            })
            .catch((error) => {
                saving.set(false);
                if(error.response.status === 422) {
                    formik.setErrors({email: "An account with this email address already exists"});
                    return;
                }
                iziToast.error({
                    title: "Oops",
                    message: "Something went wrong. Please try again",
                    timeout: 2000,
                    position: "topRight"
                });
            })
        }
    })

    return (
        <Modal.Body style={{padding: "20px 30px"}}>
            <Form onSubmit={formik.handleSubmit}>
                <Row>
                    <Col>
                        <div>
                            <span className="close_modal_button" style={{marginRight: "7px"}} onClick={() => props.closeModal()} />
                        </div>
                    </Col>
                </Row>
                <Row className='mt-4'>
                    <Col className='text-center'>
                        <h5 style={{color: "#444"}}>Update your Account Email</h5>
                    </Col>
                </Row>
                <Row className='mt-3'>
                    <Col>
                        <span style={{color: "#444", fontSize: "14px"}}>This will allow you to login into CV Counsellors using the Email Address. You will need to verify this email address, by clicking on the link sent to you.</span>
                    </Col>
                </Row>
                <Row className='mt-3'>
                    <Col>
                        <FormControl name="email"
                        value={formik.values.email}
                        onChange={formik.handleChange}
                        isInvalid={!!formik.errors.email}
                        type="email"
                        className="shadow-none cv-input-field"
                        placeholder="Email Address" />
                        <Form.Control.Feedback type="invalid">
                            {formik.errors.email}
                        </Form.Control.Feedback>
                    </Col>
                </Row>
                <Row className='mt-4'>
                    <Col>
                        {saving.get() ?
                        <Button className="cv-primary-btn btn-md w-100 "><Spinner size="sm" animation='border' /></Button>
                        :
                        <Button type="submit" className="cv-primary-btn btn-md w-100">Update email</Button>
                        }
                    </Col>
                </Row>
            </Form>
        </Modal.Body>
    )
}

export default UpdateEmailModal;