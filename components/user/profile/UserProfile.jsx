import { Button, Col, Form, FormControl, Row, Spinner } from 'react-bootstrap';
import styles from './UserProfile.module.css';
import Image from 'next/image';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { Downgraded, useState } from '@hookstate/core';
import { useEffect } from 'react';
import axios from 'axios';
import { getUser } from '../../../utils/common';
import moment from 'moment';
import axiosJWT from '../../../utils/jwtAxios';
import CustomModal from '../../global/customs/CustomModal';
import UpdateEmailModal from './modals/UpdateEmailModal';

const UserProfile = () => {

    const allowedMediaTypes = ["image/jpeg", "image/jpg", "image/png", "image/gif"];

    const genders = ['Male', 'Female', 'Other'];
    const languages = ['English', 'Hindi'];
    const user = useState(null);

    const countries = useState([]);
    const states = useState([]);
    const selectedCountry = useState(null);

    const saving = useState(false);

    const modalContent = useState(null);
    const showModal = useState(false);

    function setupModal(key) {
        if(key === "update-email") {
            modalContent.set(<UpdateEmailModal closeModal={() => showModal.set(false)} email={user.attach(Downgraded).get() ? user.attach(Downgraded).get().email : ""} name={user.attach(Downgraded).get() ? user.attach(Downgraded).get().name : ""}/>);
            showModal.set(true);
        }
    }

    const validationSchema = Yup.object({
        full_name: Yup.string().required('Name is required'),
        country: Yup.string().required('Country is mandatory')
    });

    const formik = useFormik({
        initialValues: {
            full_name: user.attach(Downgraded).get() ? user.attach(Downgraded).get().name : "",
            mobile: user.attach(Downgraded).get() ? user.attach(Downgraded).get().mobile : "",
            email: user.attach(Downgraded).get() ? user.attach(Downgraded).get().email : "",
            media: user.attach(Downgraded).get() ? user.attach(Downgraded).get().profile_image : "",
            gender: user.attach(Downgraded).get() ? user.attach(Downgraded).get().gender : "",
            dob: user.attach(Downgraded).get() ? moment(user.attach(Downgraded).get().dob).format(process.env.NEXT_PUBLIC_DATE_FORMAT) : "",
            timezone: user.attach(Downgraded).get() ? user.attach(Downgraded).get().timezone : "",
            house_number: user.attach(Downgraded).get() ? user.attach(Downgraded).get().house_number : "",
            colony_locality: user.attach(Downgraded).get() ? user.attach(Downgraded).get().colony_locality : "",
            city: user.attach(Downgraded).get() ? user.attach(Downgraded).get().city : "",
            state: user.attach(Downgraded).get() ? user.attach(Downgraded).get().state : "",
            country: user.attach(Downgraded).get() ? user.attach(Downgraded).get().country_id : "",
            pincode: user.attach(Downgraded).get() ? user.attach(Downgraded).get().pincode : "",
            mobile_2: user.attach(Downgraded).get() ? user.attach(Downgraded).get().secondary_mobile : "",
            language: user.attach(Downgraded).get() ? user.attach(Downgraded).get().language : ""
        },
        validationSchema: validationSchema,
        enableReinitialize: true,
        onSubmit: (values) => {
            saving.set(true);
            
            let formData = new FormData();
            formData.append("name", values.full_name);
            formData.append("secondary_mobile", values.mobile_2);
            formData.append("gender", values.gender);
            formData.append("dob", values.dob);
            formData.append("timezone", values.timezone);
            formData.append("house_number", values.house_number);
            formData.append("colony_locality", values.colony_locality);
            formData.append("city", values.city);
            formData.append("state", values.state);
            formData.append("country_id", parseInt(values.country));
            formData.append("pincode", values.pincode);
            formData.append("language", values.language);

            axiosJWT.put(process.env.NEXT_PUBLIC_BACKEND_URL+"customer/update/"+getUser().id, formData)
            .then(() => {
                iziToast.success({
                    title: "Details updated",
                    message: "Your profile has been successfully updated",
                    timeout: 2000,
                    position: "topRight"
                });
            })
            .catch(() => {
                iziToast.error({
                    title: "Updation failed",
                    message: "Your profile was not updated",
                    timeout: 2000,
                    position: "topRight"
                });
            })

            saving.set(false);
        }
    });

    useEffect(() => {
        if(formik.values.country !== '') {
            var _country = countries.attach(Downgraded).get().filter(country => country.country_id === parseInt(formik.values.country))[0];
            selectedCountry.set(_country);

            axios.get(process.env.NEXT_PUBLIC_BACKEND_URL+"state?countryCode="+_country.modified)
            .then((response) => {
                states.set(response.data);
            });
        }
    },[formik.values.country]);

    useEffect(() => {
        axios.get(process.env.NEXT_PUBLIC_BACKEND_URL+"country")
        .then((response) => {
            countries.set(response.data);
        });

        refreshUserData();
    }, []);

    function refreshUserData() {
        axios.get(process.env.NEXT_PUBLIC_BACKEND_URL+"customer/details/"+getUser().id)
        .then((response) => {
            user.set(response.data[0]);
        });
    }

    function pickMedia() {
        document.getElementById('input_files').click();
    }

    function removeMedia() {
        axiosJWT.put(process.env.NEXT_PUBLIC_BACKEND_URL+"customer/profile-image/delete/"+getUser().id)
        .then(() => {
            refreshUserData();

            iziToast.success({
                title: "Image removed",
                message: "Your profile image has been successfully removed",
                timeout: 2000,
                position: "topRight"
            });
        })
        .catch(() => {
            iziToast.error({
                title: "Image removal failed",
                message: "Your profile image could not be removed",
                timeout: 2000,
                position: "topRight"
            });
        })
    }

    async function uploadProfilePhoto(media) {

        const { url } = await axiosJWT.get(process.env.NEXT_PUBLIC_BACKEND_URL+"s3URL?object=image").then((res) => res.data);
        axios.put(url, media)
        .then(() => {
            const finalURL = url.split('?')[0];

            let formData = new FormData();
            formData.append("profile_image", finalURL);

            axiosJWT.put(process.env.NEXT_PUBLIC_BACKEND_URL+"customer/profile-image/"+getUser().id, formData)
            .then(() => {
                
                refreshUserData();

                iziToast.success({
                    title: "Image updated",
                    message: "Your profile image has been successfully updated",
                    timeout: 2000,
                    position: "topRight"
                });
            })
            .catch(() => {
                iziToast.error({
                    title: "Image updation failed",
                    message: "Your profile image was not updated",
                    timeout: 2000,
                    position: "topRight"
                });
            })
        })
        .catch(() => {
            iziToast.error({
                title: "Image updation failed",
                message: "Your profile image was not updated",
                timeout: 2000,
                position: "topRight"
            });
        });
    }

    return (
        <Form onSubmit={formik.handleSubmit}>
            <input type="file" id='input_files' accept='.jpg, .png, .jpeg, .gif' style={{display: "none"}} onChange={(e) => {
                formik.setErrors({media: null});

                const media = e.target.files[0];
                if(allowedMediaTypes.indexOf(media.type) > -1) {
                    if(media.size > 2000000) {
                        e.target.value = null;
                        formik.setErrors({media: "File size cannot exceed 2MB"});
                        return;
                    }
                    uploadProfilePhoto(media);

                    e.target.value = null;
                }
                else {
                    e.target.value = null;
                    formik.setErrors({media: "Unsupported file type"});
                }
            }} />
            <div className={styles.user_profile}>
                <div className={styles.user_profile_header}>
                    <span style={{fontSize: "14px"}}>Account</span>
                    {saving.get() ?
                    <Button className="cv-primary-btn btn-md"><Spinner size="sm" animation='border' /></Button>
                    :
                    <Button type="submit" className="cv-primary-btn btn-md">Save changes</Button>
                    }
                </div>
                <div className={styles.user_profile_content}>
                    <div className={styles.user_profile_content_section}>
                        <Row>
                            <Col lg="3">
                                <Form.Label>Profile Photo</Form.Label>
                                <div className="d-flex align-items-center">
                                    <div style={{borderRadius: "50%", overflow: "hidden"}}>
                                        {user.attach(Downgraded).get() && user.attach(Downgraded).get().profile_image !== ""
                                        ?
                                            <Image src={user.attach(Downgraded).get().profile_image} height={100} width={100} />
                                        :
                                            <Image src="/images/profile.png" height={100} width={100} />
                                        }
                                    </div>
                                    <div style={{paddingLeft: "10px"}}>
                                        <span style={{display: "block", color: "#B4B4BE", fontSize: "12px"}}>Pick a photo from your computer</span>
                                        {user.attach(Downgraded).get() && user.attach(Downgraded).get().profile_image !== "" ?
                                            <div>
                                                <span onClick={() => pickMedia()} style={{marginRight: "10px", cursor: "pointer", color: "#14BEF0", fontSize: "12px", fontWeight: "bold"}}>Edit</span>
                                                <span onClick={() => removeMedia()} style={{cursor: "pointer", color: "#14BEF0", fontSize: "12px", fontWeight: "bold"}}>Remove</span>
                                            </div>
                                        :
                                            <span onClick={() => pickMedia()} style={{cursor: "pointer", color: "#14BEF0", fontSize: "12px", fontWeight: "bold"}}>Add Photo</span>
                                        }
                                    </div>
                                </div>
                                <FormControl type="hidden" name="media"
                                value={formik.values.media}
                                onChange={formik.handleChange}
                                isInvalid={!!formik.errors.media} />
                                <Form.Control.Feedback type="invalid">
                                    {formik.errors.media}
                                </Form.Control.Feedback>
                            </Col>
                            <Col lg="4">
                                <Form.Label>Name*</Form.Label>
                                <FormControl name="full_name"
                                value={formik.values.full_name}
                                onChange={formik.handleChange}
                                isInvalid={!!formik.errors.full_name}
                                type="text"
                                className="shadow-none cv-input-field" />
                                <Form.Control.Feedback type="invalid">
                                    {formik.errors.full_name}
                                </Form.Control.Feedback>
                            </Col>
                        </Row>
                    </div>
                    <div className={styles.user_profile_content_section}>
                        <Row>
                            <Col lg="4">
                            <div style={{justifyContent: "space-between"}} className="d-flex"><Form.Label>Phone number</Form.Label><span style={{cursor: "pointer", color: "#14BEF0", fontSize: "14px", fontWeight: "bold"}}>Edit</span></div>
                                <FormControl name="mobile"
                                value={formik.values.mobile}
                                onChange={formik.handleChange}
                                isInvalid={!!formik.errors.mobile}
                                type="text"
                                disabled={true}
                                className="shadow-none cv-input-field" />
                            </Col>
                            <Col lg="4">
                                <div style={{justifyContent: "space-between"}} className="d-flex"><Form.Label>Email Address</Form.Label><span onClick={() => setupModal('update-email')} style={{cursor: "pointer", color: "#14BEF0", fontSize: "14px", fontWeight: "bold"}}>{user.attach(Downgraded).get() && user.attach(Downgraded).get().email !== "" ? "Edit" : "Add"}</span></div>
                                <FormControl name="email"
                                value={formik.values.email}
                                onChange={formik.handleChange}
                                isInvalid={!!formik.errors.email}
                                disabled={true}
                                type="email"
                                className="shadow-none cv-input-field" />
                            </Col>
                            <Col lg="4">
                            <div style={{justifyContent: "space-between"}} className="d-flex"><Form.Label>Gender</Form.Label></div>
                                <Form.Select name="gender"
                                value={formik.values.gender}
                                onChange={formik.handleChange}
                                isInvalid={!!formik.errors.gender}
                                className="shadow-none cv-input-field">
                                    <option value="" defaultValue={true} disabled={true}>Select an option</option>
                                    {genders.map(gender =>
                                        <option key={gender} value={gender}>{gender}</option>
                                    )}
                                </Form.Select>
                            </Col>
                        </Row>
                        <Row className='mt-4'>
                            <Col lg="4">
                                <Form.Label>Date of birth</Form.Label>
                                <FormControl name="dob"
                                value={formik.values.dob}
                                onChange={formik.handleChange}
                                isInvalid={!!formik.errors.dob}
                                type="date"
                                className="shadow-none cv-input-field" />
                            </Col>
                            <Col lg="4">
                                <Form.Label>Timezone</Form.Label>
                                <Form.Select name="timezone"
                                value={formik.values.timezone}
                                onChange={formik.handleChange}
                                isInvalid={!!formik.errors.timezone}
                                className="shadow-none cv-input-field">
                                    <option value="" defaultValue={true} disabled={true}>Select an option</option>
                                    {selectedCountry.attach(Downgraded).get() && JSON.parse(selectedCountry.attach(Downgraded).get().country_timezones).map(timezone =>
                                        <option key={timezone.zoneName} value={`(${timezone.gmtOffsetName}) ${timezone.zoneName}`}>{`(${timezone.gmtOffsetName}) ${timezone.zoneName}`}</option>
                                    )}
                                </Form.Select>
                            </Col>
                        </Row>
                    </div>
                    <div className={styles.user_profile_content_section}>
                        <div className={styles.user_profile_content_section_header}>
                            Address
                        </div>
                        <Row>
                            <Col lg="4">
                                <Form.Label>House No./Street Name/Area</Form.Label>
                                <FormControl name="house_number"
                                value={formik.values.house_number}
                                onChange={formik.handleChange}
                                isInvalid={!!formik.errors.house_number}
                                type="text"
                                className="shadow-none cv-input-field" />
                            </Col>
                            <Col lg="4">
                                <Form.Label>Colony/Street/Locality</Form.Label>
                                <FormControl name="colony_locality"
                                value={formik.values.colony_locality}
                                onChange={formik.handleChange}
                                isInvalid={!!formik.errors.colony_locality}
                                type="text"
                                className="shadow-none cv-input-field" />
                            </Col>
                            <Col lg="4">
                                <Form.Label>City</Form.Label>
                                <FormControl name="city"
                                value={formik.values.city}
                                onChange={formik.handleChange}
                                isInvalid={!!formik.errors.city}
                                type="text"
                                className="shadow-none cv-input-field" />
                            </Col>
                        </Row>
                        <Row className='mt-4'>
                            <Col lg="4">
                                <Form.Label>State</Form.Label>
                                <Form.Select name="state"
                                value={formik.values.state}
                                onChange={formik.handleChange}
                                isInvalid={!!formik.errors.state}
                                type="text"
                                className="shadow-none cv-input-field">
                                    <option value="" defaultValue={true} disabled={true}>Select an option</option>
                                    {states.attach(Downgraded).get().map(state =>
                                        <option key={state.value} value={state.label}>{state.label}</option>
                                    )}
                                </Form.Select>
                            </Col>
                            <Col lg="4">
                                <Form.Label>Country*</Form.Label>
                                <Form.Select name="country"
                                value={formik.values.country}
                                onChange={formik.handleChange}
                                isInvalid={!!formik.errors.country}
                                className="shadow-none cv-input-field">
                                    <option value="" defaultValue={true} disabled={true}>Select an option</option>
                                    {countries.attach(Downgraded).get().map(country =>
                                        <option key={country.country_id} value={country.country_id}>{country.country_name}</option>
                                    )}
                                </Form.Select>
                                <Form.Control.Feedback type="invalid">
                                    {formik.errors.country}
                                </Form.Control.Feedback>
                            </Col>
                            <Col lg="4">
                                <Form.Label>Pincode</Form.Label>
                                <FormControl name="pincode"
                                value={formik.values.pincode}
                                onChange={formik.handleChange}
                                isInvalid={!!formik.errors.pincode}
                                type="text"
                                className="shadow-none cv-input-field" />
                            </Col>
                        </Row>
                    </div>
                    <div className={styles.user_profile_content_section}>
                        <div className={styles.user_profile_content_section_header}>
                            Other Information
                        </div>
                        <Row>
                            <Col lg="4">
                                <Form.Label>Extra phone number</Form.Label>
                                <FormControl name="mobile_2"
                                value={formik.values.mobile_2}
                                onChange={formik.handleChange}
                                isInvalid={!!formik.errors.mobile_2}
                                type="number"
                                className="shadow-none cv-input-field" />
                            </Col>
                            <Col lg="4">
                                <Form.Label>Language</Form.Label>
                                <Form.Select name="language"
                                value={formik.values.language}
                                onChange={formik.handleChange}
                                isInvalid={!!formik.errors.language}
                                type="text"
                                className="shadow-none cv-input-field">
                                    <option value="" defaultValue={true} disabled={true}>Select an option</option>
                                    {languages.map(language =>
                                        <option key={language} value={language}>{language}</option>
                                    )}
                                </Form.Select>
                            </Col>
                        </Row>
                    </div>
                </div>
            </div>
            <CustomModal content={modalContent} showModal={showModal} modalSize="md" staticBackdrop={false} />
        </Form>
    );
}

export default UserProfile;