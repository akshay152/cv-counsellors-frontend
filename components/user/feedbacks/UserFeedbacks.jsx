import { Downgraded, useState } from '@hookstate/core';
import Link from 'next/link';
import moment from 'moment';
import { Menu } from 'primereact/menu';
import StarRatings from 'react-star-ratings';
import { useEffect, useRef } from 'react';
import styles from './UserFeedbacks.module.css';
import { ImForward } from 'react-icons/im';
import { HiDotsVertical } from 'react-icons/hi';
import { Button } from 'react-bootstrap';
import EmptyList from '../../global/customs/EmptyList';
import { getCustomerFeedbacks } from '../../../pages/api/feedbacks';
import { getUser } from '../../../utils/common';
import EditFeedbackModalContent from '../../chat/VideoComponents/modals/EditFeedbackModalContent';
import CustomModal from '../../global/customs/CustomModal';
import axiosJWT from '../../../utils/jwtAxios';

const UserFeedbacks = () => {

    const feedbacks = useState([]);

    const feedbacksPerLoad = useState(10);
    const maxFeedbacks = useState(10);

    const modalContent = useState(null);
    const showModal = useState(false);

    let years = [];

    const feedbackMenuRef = useRef(null);

    function setupModal(key, eKey = '') {
        if (key === 'edit-feedback') {
            modalContent.set(<EditFeedbackModalContent feedback={eKey} closeModal={() => showModal.set(false)} getFeedbacks={getFeedbacks} />);
            showModal.set(true);
        }
    }

    const feedbackMenuItems = [
        {
            label: 'Options',
            items: [
                {
                    label: 'Edit',
                    icon: 'pi pi-pencil',
                    command: () => {
                        setupModal('edit-feedback', feedbackMenuRef.current.props.id);
                    }
                },
                {
                    label: 'Delete',
                    icon: 'pi pi-times',
                    command: () => {
                        iziToast.question({
                            timeout: false,
                            close: false,
                            overlay: true,
                            displayMode: 'once',
                            id: 'question',
                            zindex: 999,
                            title: 'Hey',
                            message: 'Are you sure you want to delete this review?',
                            position: 'center',
                            buttons: [
                                ['<button><b>Yes</b></button>', function (instance, toast) {
                                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                                    axiosJWT.delete(process.env.NEXT_PUBLIC_BACKEND_URL + "counsellor/review/delete?customer_id="+getUser().id+"&feedback_id="+feedbackMenuRef.current.props.id.id)
                                        .then(() => {

                                            iziToast.success({
                                                title: "Deleted",
                                                message: "Your review has been successfully deleted.",
                                                position: "topRight",
                                                timeout: 2000
                                            });

                                            getFeedbacks();
                                        })
                                        .catch(() => {
                                            iziToast.error({
                                                title: "Error",
                                                message: "Oops! Something went wrong",
                                                position: "topRight",
                                                timeout: 2000
                                            });
                                        });

                                }, true],
                                ['<button>No</button>', function (instance, toast) {
                                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                                }],
                            ],
                        })
                    }
                }
            ]
        }
    ];

    function getRatingColor(rating) {
        if (rating < 3) {
            return "#cb202d";
        }
        else if (rating < 4) {
            return "#F5CB39";
        }
        else {
            return "#01a400";
        }
    }

    function getFeedbacks() {
        getCustomerFeedbacks(getUser().id).then((response) => {
            feedbacks.set(response.data.description);
        });
    }

    useEffect(() => {
        getFeedbacks();
    }, []);

    return (
        <div className={styles.feedback_wrapper}>
            <div className={styles.feedback_header}>
                <span style={{ fontSize: "14px" }}>Your Feedbacks</span>
            </div>
            {feedbacks.attach(Downgraded).get().length > 0 ?
                <div className={styles.feedback_content}>
                    {feedbacks.attach(Downgraded).get().slice(0, maxFeedbacks.get()).map(feedback => {
                        let reviewYear = moment(feedback.timestamp).format('Y');
                        if (years[reviewYear] !== undefined) {
                            years[reviewYear] = years[reviewYear].concat([feedback]);
                        }
                        else {
                            years[reviewYear] = [feedback];
                        }
                    })}
                    {years.slice(0).reverse().map((feedbacks, year) =>
                        <div className={styles.feedback_year_wrapper} key={year}>
                            <div className={styles.feedback_year}>
                                {moment(feedbacks[0].timestamp).format('Y')}
                            </div>
                            {feedbacks.map(feedback =>
                                <div key={feedback.id} className={styles.feedback_content_wrapper}>
                                    <div className='d-flex align-items-center'>
                                        <div className={styles.date_holder}>
                                            <div className={styles.feedback_date}>
                                                {moment(feedback.timestamp).format('DD')}
                                            </div>
                                            <div className={styles.feedback_month_name}>
                                                {moment(feedback.timestamp).format('MMM')}
                                            </div>
                                        </div>
                                        <div className={styles.feedback_details}>
                                            <div>
                                                <div className={styles.counsellor_name}>
                                                    Feedback for&nbsp;
                                                    <span>
                                                        <Link href={`/counsellor/${feedback.counsellor_slug}`}>
                                                            <a style={{ textDecoration: "none", color: "#414146" }}>
                                                                {feedback.counsellor_name}
                                                            </a>
                                                        </Link>
                                                    </span>
                                                </div>
                                                <div className={styles.feedback_status}>
                                                    PUBLISHED
                                                </div>
                                            </div>
                                            <div>
                                                <Menu model={feedbackMenuItems} popup ref={feedbackMenuRef} id={feedback} />
                                                <div onClick={(event) => feedbackMenuRef.current.toggle(event)} style={{ cursor: "pointer" }} aria-controls="feedback_menu" aria-haspopup>
                                                    <HiDotsVertical fontSize={20} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className={styles.feedback_item}>
                                        <div className={styles.feedback_body}>
                                            <div className={styles.feedback_ratings}>
                                                <StarRatings
                                                    rating={feedback.rating}
                                                    starDimension="20px"
                                                    starSpacing="2px"
                                                    isSelectable={false}
                                                    starRatedColor={getRatingColor(feedback.rating)}
                                                />
                                            </div>
                                            <div className={styles.feedback_title}>
                                                {feedback.title}
                                            </div>
                                            <div className={styles.feedback_review}>
                                                {feedback.review}
                                            </div>
                                            {feedback.reply && feedback.reply !== "" ?
                                                <div className={`${styles.feedback_reply} mt-2`}>
                                                    <div className='d-flex align-items-center'>
                                                        <ImForward color='#B4B4BE' fontSize={18} />&nbsp;&nbsp;
                                                        <span style={{ fontSize: "14px" }}>{feedback.counsellor_name} replied</span>
                                                    </div>
                                                    <div className={`${styles.feedback_reply_text}`}>
                                                        {feedback.reply}
                                                    </div>
                                                </div>
                                                :
                                                null
                                            }
                                        </div>
                                    </div>
                                </div>
                            )}
                        </div>
                    )}
                    {maxFeedbacks.get() >= feedbacks.attach(Downgraded).get().length ?
                        null
                        :
                        <div className='d-flex p-4 align-items-center justify-content-center'>
                            <Button onClick={() => maxFeedbacks.set(maxFeedbacks.get() + feedbacksPerLoad.get())} className='cv-primary-btn' style={{ padding: "5px 30px" }}>More</Button>
                        </div>
                    }
                </div>
                :
                <EmptyList title="Oops!" subtitle="You haven't given any feedbacks yet" />
            }
            <CustomModal content={modalContent} showModal={showModal} modalSize="md" staticBackdrop={false} />
        </div>
    )
}

export default UserFeedbacks;