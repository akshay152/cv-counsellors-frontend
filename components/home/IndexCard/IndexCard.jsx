import Image from 'next/image';
import Link from 'next/link';
import { Col } from 'react-bootstrap';
import styles from './IndexCard.module.css';

const IndexCard = (props) => {
    return (
      <Col className={`d-flex ${styles.static_card}`} style={{justifyContent: "center"}}>
        <div className={`${styles.card_offerings}`}>
          <Link href={props.card.href}>
            <a className={`${styles.card_link}`}/>
          </Link>
          <div style={{background: props.card.background}} className={`${styles.card_image_wrapper}`}>
            <Link href={props.card.href}>
              <a>
                <Image src={props.card.image} height={158} width={130} />  
              </a>
            </Link>
          </div>
          <div className={`${styles.card_info}`}>
            <div className={styles.card_title}>{props.card.title}</div>
            <div className={styles.card_subtitle}>{props.card.subtitle}</div>
          </div>
        </div>
      </Col>
    )
}

export default IndexCard;