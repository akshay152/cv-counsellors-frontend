import styles from './SearchBox.module.css';
import Select from 'react-select';

const SearchBox = (props) => {

    return (
        <Select value={props.default ? props.options.filter(option => option.modified === props.default.modified)[0] : props.options[0]} style={{fontSize: "12px"}} onChange={props.onChange} className={`${styles.f_25} f-14`} options={props.options} isLoading={props.isLoading} isSearchable={true} placeholder={props.placeholder} />
    );
}

export default SearchBox;