import Image from 'next/image';
import Link from 'next/link';
import { Badge } from 'react-bootstrap';
import { Navigation } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import { BsFillHandThumbsUpFill } from 'react-icons/bs';
import styles from './TopCounsellors.module.css';

const TopCounsellors = () => {

    const topCounsellors = [
        {
            id: 0,
            image: "/images/dr-1.jpg",
            profile_link: "/counsellor/dr-1",
            verified: true,
            name: "Sumanth Shetty",
            specializations: "BBA (Abroad)",
            experience: "4",
            fees: "300",
            upvotes_percent: "91",
            reviews: "89"
        },
        {
            id: 1,
            image: "/images/dr-2.jpg",
            profile_link: "/counsellor/dr-2",
            verified: false,
            name: "Ram Prasad",
            specializations: "MCA (Distance)",
            experience: "2",
            fees: "150",
            upvotes_percent: "59",
            reviews: "22"
        },
        {
            id: 2,
            image: "/images/dr-3.jpg",
            profile_link: "/counsellor/dr-3",
            verified: true,
            name: "John Doe",
            specializations: "MBA (Online)",
            experience: "3",
            fees: "99",
            upvotes_percent: "67",
            reviews: "12"
        },
    ];

    const breakpoints = {
        '1400': {
            slidesPerView: 3
        }
    }

    return (
        <div className='mt-5'>
            <Swiper loop={true} breakpoints={breakpoints} slidesPerView={1} spaceBetween={30} navigation={true} centeredSlides={true} modules={[Navigation]}>
                {topCounsellors.map(counsellor =>
                    <SwiperSlide key={counsellor.id}>
                        <div className={styles.top_counsellor_card}>
                            <div className="d-flex">
                                <div className={styles.top_counsellor_card_left}>
                                    <div className={styles.top_counsellor_card_img}>
                                        <Image src={counsellor.image} width={100} height={100} />
                                    </div>
                                    {counsellor.verified ? <img className={styles.verified} src="/images/verified.svg" width={20} height={20} /> : null}
                                </div>
                                <div className={styles.top_counsellor_card_right}>
                                    <Link href={counsellor.profile_link}>
                                        <a style={{textDecoration: "none"}}>
                                            <div className={styles.counsellor_name}>{counsellor.name}</div>
                                        </a>
                                    </Link>
                                    <div className={styles.counsellor_specialization}>
                                        {counsellor.specializations}
                                    </div>
                                    <div className={styles.counsellor_experience}>
                                        {counsellor.experience} years experience overall
                                    </div>
                                    <div className={styles.consultation_fees}>
                                        ₹{counsellor.fees} consultation fee
                                    </div>
                                    <div className={styles.counsellor_reviews}>
                                        <Badge bg={counsellor.upvotes_percent > 60 ? "success" : "danger"}><BsFillHandThumbsUpFill /> {counsellor.upvotes_percent}%</Badge>
                                        <Link href={counsellor.profile_link}>
                                            <a style={{textDecoration: "none", fontSize: "12px", color: "#14bef1"}}>
                                                {counsellor.reviews} student reviews
                                            </a>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </SwiperSlide>
                )}
            </Swiper>
        </div>
    );
}

export default TopCounsellors;